package tn.com.guru.project.controller.administration;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import tn.com.guru.project.administration.model.LaborHistogram;
import tn.com.guru.project.administration.model.RiskRegister;
import tn.com.guru.project.administration.service.ILaborHistogramService;
import tn.com.guru.project.common.service.ICommonService;
import tn.com.guru.project.common.service.ISendWsService;
import tn.com.guru.project.tools.ConstanteWs;
import tn.com.guru.project.tools.model.SearchObject;

@RestController
@RequestMapping("/laborHistogram")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class LaborHistogramController {

	private static final Logger logger = LogManager.getLogger(LaborHistogramController.class);

	@Autowired
	private ILaborHistogramService laborHistogramService;

	@Autowired
	private ISendWsService sendWsService;

	@Autowired
	private ICommonService commonService;
	
	
	@Operation(summary = "Insert liste des objet")
	@ApiResponses(value = { @ApiResponse(responseCode = ConstanteWs._CODE_WS_SUCCESS, description = "Success"),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR_ALIAS_PARAM, description = "Un ou plus param�tre(s) est null", content = @Content),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR_IN_METHOD, description = "Erreur du methode", content = @Content),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR, description = "Erreur du service", content = @Content) })
	@PostMapping(value = "/List", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> pushListofLaborHistogram(HttpServletRequest request, @RequestBody List<LaborHistogram>  ListOfentity) {
		try {
			return sendWsService.sendResult(request, laborHistogramService.saveOrUpdateListOFLaborHistogramws(ListOfentity));
		} catch (Exception argEx) {
			logger.error("Error RiskRegisterController in method push saveOrUpdateListOFLaborHistogramws :: " + argEx.toString());
			return sendWsService.sendResultException(request);
		}
	}

	@Operation(summary = "Recuperation liste de donnees sans filtre de recherche")
	@ApiResponses(value = { @ApiResponse(responseCode = ConstanteWs._CODE_WS_SUCCESS, description = "Success"),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR_ALIAS_PARAM, description = "Un ou plus param�tre(s) est null", content = @Content),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR_IN_METHOD, description = "Erreur du methode", content = @Content),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR, description = "Erreur du service", content = @Content) })
	@GetMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getListLaborHistogramWs(HttpServletRequest request) {
		try {
			return sendWsService.sendResult(request, laborHistogramService.getListLaborHistogramWs());
		} catch (Exception argEx) {
			logger.error("Error LaborHistogramController in method getListLaborHistogramWs :: " + argEx.toString());
			return sendWsService.sendResultException(request);
		}
	}

	@Operation(summary = "Recuperation un objet d'apres son id")
	@ApiResponses(value = { @ApiResponse(responseCode = ConstanteWs._CODE_WS_SUCCESS, description = "Success"),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR_ALIAS_PARAM, description = "Un ou plus param�tre(s) est null", content = @Content),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR_IN_METHOD, description = "Erreur du methode", content = @Content),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR, description = "Erreur du service", content = @Content) })
	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getLaborHistogramByIdWs(HttpServletRequest request, @PathVariable(name = "id") Long id) {
		try {
			return sendWsService.sendResult(request, laborHistogramService.findLaborHistogramByIdWs(id));
		} catch (Exception argEx) {
			logger.error("Error LaborHistogramController in method getLaborHistogramByIdWs :: " + argEx.toString());
			return sendWsService.sendResultException(request);
		}
	}

	@Operation(summary = "Insert un nouveau objet")
	@ApiResponses(value = { @ApiResponse(responseCode = ConstanteWs._CODE_WS_SUCCESS, description = "Success"),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR_ALIAS_PARAM, description = "Un ou plus param�tre(s) est null", content = @Content),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR_IN_METHOD, description = "Erreur du methode", content = @Content),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR, description = "Erreur du service", content = @Content) })
	@PostMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> pushLaborHistogram(HttpServletRequest request, @RequestBody LaborHistogram entity) {
		try {
			return sendWsService.sendResult(request, laborHistogramService.saveOrUpdateLaborHistogramWs(entity));
		} catch (Exception argEx) {
			logger.error("Error LaborHistogramController in method pushLaborHistogramWs :: " + argEx.toString());
			return sendWsService.sendResultException(request);
		}
	}

	@Operation(summary = "Mise a jour l'objet")
	@ApiResponses(value = { @ApiResponse(responseCode = ConstanteWs._CODE_WS_SUCCESS, description = "Success"),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR_ALIAS_PARAM, description = "Un ou plus param�tre(s) est null", content = @Content),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR_IN_METHOD, description = "Erreur du methode", content = @Content),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR, description = "Erreur du service", content = @Content) })
	@PutMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> putLaborHistogramWs(HttpServletRequest request, @RequestBody LaborHistogram entity) {
		try {
			return sendWsService.sendResult(request, laborHistogramService.saveOrUpdateLaborHistogramWs(entity));
		} catch (Exception argEx) {
			logger.error("Error LaborHistogramController in method putLaborHistogramWs :: " + argEx.toString());
			return sendWsService.sendResultException(request);
		}
	}

	@Operation(summary = "Supprimer l'objet ")
	@ApiResponses(value = { @ApiResponse(responseCode = ConstanteWs._CODE_WS_SUCCESS, description = "Success"),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR_ALIAS_PARAM, description = "Un ou plus param�tre(s) est null", content = @Content),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR_DELETE_ROW, description = "Impossible de supprimer l'objet car il est reference ", content = @Content),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR_IN_METHOD, description = "Erreur du methode", content = @Content),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR, description = "Erreur du service", content = @Content) })
	@DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> deleteLaborHistogramByIdWs(HttpServletRequest request, @PathVariable("id") Long id) {
		try {
			return sendWsService.sendResult(request, laborHistogramService.deleteLaborHistogramByIdWs(id));
		} catch (Exception argEx) {
			logger.error("Error LaborHistogramController in method deleteLaborHistogramByIdWs :: " + argEx.toString());
			return sendWsService.sendResultException(request);
		}
	}

	@Operation(summary = "Recuperation liste de donnees avec filtre de recherche")
	@ApiResponses(value = { @ApiResponse(responseCode = ConstanteWs._CODE_WS_SUCCESS, description = "Success"),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR_ALIAS_PARAM, description = "Un ou plus param�tre(s) est null", content = @Content),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR_IN_METHOD, description = "Erreur du methode", content = @Content),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR, description = "Erreur du service", content = @Content) })
	@PostMapping(value = "/data", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getDataLaborHistogramWs(HttpServletRequest request, @RequestBody SearchObject obj) {
		try {
			return sendWsService.sendResult(request, commonService.getListPaginator(obj, new LaborHistogram(), null));
		} catch (Exception argEx) {
			logger.error("Error LaborHistogramController in method getDataLaborHistogramWs :: " + argEx.toString());
			return sendWsService.sendResultException(request);
		}
	}

}
