package tn.com.guru.project.controller.administration;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import tn.com.guru.project.administration.model.AdmUtilisateur;
import tn.com.guru.project.administration.service.IAdmUtilisateurService;
import tn.com.guru.project.common.service.ICommonService;
import tn.com.guru.project.common.service.ISendWsService;
import tn.com.guru.project.tools.ConstanteWs;
import tn.com.guru.project.tools.model.SearchObject;

@RestController
@RequestMapping("/admUtilisateur")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class AdmUtilisateurController {

	private static final Logger logger = LogManager.getLogger(AdmUtilisateurController.class);

	@Autowired
	private IAdmUtilisateurService admUtilisateurService;

	@Autowired
	private ISendWsService sendWsService;

	@Autowired
	private ICommonService commonService;

	@Operation(summary = "Recuperation liste de donnees sans filtre de recherche")
	@ApiResponses(value = { @ApiResponse(responseCode = ConstanteWs._CODE_WS_SUCCESS, description = "Success"),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR_ALIAS_PARAM, description = "Un ou plus param�tre(s) est null", content = @Content),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR_IN_METHOD, description = "Erreur du methode", content = @Content),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR, description = "Erreur du service", content = @Content) })
	@GetMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getListAdmUtilisateurWs(HttpServletRequest request) {
		try {
			return sendWsService.sendResult(request, admUtilisateurService.getListAdmUtilisateurWs());
		} catch (Exception argEx) {
			logger.error("Error AdmUtilisateurController in method getListAdmUtilisateurWs :: " + argEx.toString());
			return sendWsService.sendResultException(request);
		}
	}

	@Operation(summary = "Recuperation un objet d'apres son id")
	@ApiResponses(value = { @ApiResponse(responseCode = ConstanteWs._CODE_WS_SUCCESS, description = "Success"),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR_ALIAS_PARAM, description = "Un ou plus param�tre(s) est null", content = @Content),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR_IN_METHOD, description = "Erreur du methode", content = @Content),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR, description = "Erreur du service", content = @Content) })
	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getAdmUtilisateurByIdWs(HttpServletRequest request, @PathVariable(name = "id") Long id) {
		try {
			return sendWsService.sendResult(request, admUtilisateurService.findAdmUtilisateurByIdWs(id));
		} catch (Exception argEx) {
			logger.error("Error AdmUtilisateurController in method getAdmUtilisateurByIdWs :: " + argEx.toString());
			return sendWsService.sendResultException(request);
		}
	}

	@Operation(summary = "Insert un nouveau objet")
	@ApiResponses(value = { @ApiResponse(responseCode = ConstanteWs._CODE_WS_SUCCESS, description = "Success"),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR_ALIAS_PARAM, description = "Un ou plus param�tre(s) est null", content = @Content),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR_IN_METHOD, description = "Erreur du methode", content = @Content),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR, description = "Erreur du service", content = @Content) })
	@PostMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> pushAdmUtilisateur(HttpServletRequest request, @RequestBody AdmUtilisateur entity) {
		try {
			return sendWsService.sendResult(request, admUtilisateurService.saveOrUpdateAdmUtilisateurWs(entity));
		} catch (Exception argEx) {
			logger.error("Error AdmUtilisateurController in method pushAdmUtilisateurWs :: " + argEx.toString());
			return sendWsService.sendResultException(request);
		}
	}

	@Operation(summary = "Mise a jour l'objet")
	@ApiResponses(value = { @ApiResponse(responseCode = ConstanteWs._CODE_WS_SUCCESS, description = "Success"),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR_ALIAS_PARAM, description = "Un ou plus param�tre(s) est null", content = @Content),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR_IN_METHOD, description = "Erreur du methode", content = @Content),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR, description = "Erreur du service", content = @Content) })
	@PutMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> putAdmUtilisateurWs(HttpServletRequest request, @RequestBody AdmUtilisateur entity) {
		try {
			return sendWsService.sendResult(request, admUtilisateurService.saveOrUpdateAdmUtilisateurWs(entity));
		} catch (Exception argEx) {
			logger.error("Error AdmUtilisateurController in method putAdmUtilisateurWs :: " + argEx.toString());
			return sendWsService.sendResultException(request);
		}
	}

	@Operation(summary = "Supprimer l'objet ")
	@ApiResponses(value = { @ApiResponse(responseCode = ConstanteWs._CODE_WS_SUCCESS, description = "Success"),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR_ALIAS_PARAM, description = "Un ou plus param�tre(s) est null", content = @Content),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR_DELETE_ROW, description = "Impossible de supprimer l'objet car il est reference ", content = @Content),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR_IN_METHOD, description = "Erreur du methode", content = @Content),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR, description = "Erreur du service", content = @Content) })
	@DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> deleteAdmUtilisateurByIdWs(HttpServletRequest request, @PathVariable("id") Long id) {
		try {
			return sendWsService.sendResult(request, admUtilisateurService.deleteAdmUtilisateurByIdWs(id));
		} catch (Exception argEx) {
			logger.error("Error AdmUtilisateurController in method deleteAdmUtilisateurByIdWs :: " + argEx.toString());
			return sendWsService.sendResultException(request);
		}
	}

	@Operation(summary = "Recuperation liste de donnees avec filtre de recherche")
	@ApiResponses(value = { @ApiResponse(responseCode = ConstanteWs._CODE_WS_SUCCESS, description = "Success"),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR_ALIAS_PARAM, description = "Un ou plus param�tre(s) est null", content = @Content),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR_IN_METHOD, description = "Erreur du methode", content = @Content),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR, description = "Erreur du service", content = @Content) })
	@PostMapping(value = "/data", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getDataAdmUtilisateurWs(HttpServletRequest request, @RequestBody SearchObject obj) {
		try {
			return sendWsService.sendResult(request, commonService.getListPaginator(obj, new AdmUtilisateur(), null));
		} catch (Exception argEx) {
			logger.error("Error AdmUtilisateurController in method getDataAdmUtilisateurWs :: " + argEx.toString());
			return sendWsService.sendResultException(request);
		}
	}

	@Operation(summary = "User Register")
	@ApiResponses(value = { @ApiResponse(responseCode = ConstanteWs._CODE_WS_SUCCESS, description = "Success"),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR_ALIAS_PARAM, description = "Un ou plus param�tre(s) est null", content = @Content),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR_IN_METHOD, description = "Erreur du methode", content = @Content),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR, description = "Erreur du service", content = @Content) })
	@PostMapping(value = "/user", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> saveUser(HttpServletRequest request, @RequestBody AdmUtilisateur user) throws Exception {
		try {
			return sendWsService.sendResult(request,
					admUtilisateurService.registerWs(user));
		} catch (Exception argEx) {
			logger.error("Error AdministrationController in method saveUser :: " + argEx.toString());
			return sendWsService.sendResultException(request);
		}
	}

	@Operation(summary = "User Update")
	@ApiResponses(value = { @ApiResponse(responseCode = ConstanteWs._CODE_WS_SUCCESS, description = "Success"),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR_ALIAS_PARAM, description = "Un ou plus param�tre(s) est null", content = @Content),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR_IN_METHOD, description = "Erreur du methode", content = @Content),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR, description = "Erreur du service", content = @Content) })
	@PutMapping(value = "/user", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> updateUser(HttpServletRequest request, @RequestBody AdmUtilisateur user) throws Exception {
		try {
			return sendWsService.sendResult(request, admUtilisateurService.updateUserWs(user));
		} catch (Exception argEx) {
			logger.error("Error AdministrationController in method updateUser :: " + argEx.toString());
			return sendWsService.sendResultException(request);
		}
	}
}
