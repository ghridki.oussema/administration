package tn.com.guru.project.controller.administration;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import tn.com.guru.project.administration.model.MaterialSubmittalStatus;
import tn.com.guru.project.administration.service.IMaterialSubmittalStatusService;
import tn.com.guru.project.common.service.ICommonService;
import tn.com.guru.project.common.service.ISendWsService;
import tn.com.guru.project.tools.ConstanteWs;
import tn.com.guru.project.tools.model.SearchObject;

@RestController
@RequestMapping("/materialSubmittalStatus")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class MaterialSubmittalStatusController {

	private static final Logger logger = LogManager.getLogger(MaterialSubmittalStatusController.class);

	@Autowired
	private IMaterialSubmittalStatusService materialSubmittalStatusService;

	@Autowired
	private ISendWsService sendWsService;

	@Autowired
	private ICommonService commonService;

	@Operation(summary = "Recuperation liste de donnees sans filtre de recherche")
	@ApiResponses(value = { @ApiResponse(responseCode = ConstanteWs._CODE_WS_SUCCESS, description = "Success"),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR_ALIAS_PARAM, description = "Un ou plus param�tre(s) est null", content = @Content),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR_IN_METHOD, description = "Erreur du methode", content = @Content),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR, description = "Erreur du service", content = @Content) })
	@GetMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getListMaterialSubmittalStatusWs(HttpServletRequest request) {
		try {
			return sendWsService.sendResult(request, materialSubmittalStatusService.getListMaterialSubmittalStatusWs());
		} catch (Exception argEx) {
			logger.error("Error MaterialSubmittalStatusController in method getListMaterialSubmittalStatusWs :: "
					+ argEx.toString());
			return sendWsService.sendResultException(request);
		}
	}

	@Operation(summary = "Recuperation un objet d'apres son id")
	@ApiResponses(value = { @ApiResponse(responseCode = ConstanteWs._CODE_WS_SUCCESS, description = "Success"),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR_ALIAS_PARAM, description = "Un ou plus param�tre(s) est null", content = @Content),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR_IN_METHOD, description = "Erreur du methode", content = @Content),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR, description = "Erreur du service", content = @Content) })
	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getMaterialSubmittalStatusByIdWs(HttpServletRequest request,
			@PathVariable(name = "id") Long id) {
		try {
			return sendWsService.sendResult(request,
					materialSubmittalStatusService.findMaterialSubmittalStatusByIdWs(id));
		} catch (Exception argEx) {
			logger.error("Error MaterialSubmittalStatusController in method getMaterialSubmittalStatusByIdWs :: "
					+ argEx.toString());
			return sendWsService.sendResultException(request);
		}
	}

	@Operation(summary = "Insert un nouveau objet")
	@ApiResponses(value = { @ApiResponse(responseCode = ConstanteWs._CODE_WS_SUCCESS, description = "Success"),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR_ALIAS_PARAM, description = "Un ou plus param�tre(s) est null", content = @Content),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR_IN_METHOD, description = "Erreur du methode", content = @Content),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR, description = "Erreur du service", content = @Content) })
	@PostMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> MaterialSubmittalStatus(HttpServletRequest request,
			@RequestBody List<MaterialSubmittalStatus> entity, @RequestParam(name = "idProject") Long idProject) {
		try {
			return sendWsService.sendResult(request,
					materialSubmittalStatusService.saveOrUpdateMaterialSubmittalStatusWs(entity, idProject));
		} catch (Exception argEx) {
			logger.error("Error MaterialSubmittalStatusController in method pushMaterialSubmittalStatusWs :: "
					+ argEx.toString());
			return sendWsService.sendResultException(request);
		}
	}

	@Operation(summary = "Mise a jour l'objet")
	@ApiResponses(value = { @ApiResponse(responseCode = ConstanteWs._CODE_WS_SUCCESS, description = "Success"),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR_ALIAS_PARAM, description = "Un ou plus param�tre(s) est null", content = @Content),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR_IN_METHOD, description = "Erreur du methode", content = @Content),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR, description = "Erreur du service", content = @Content) })
	@PutMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> putMaterialSubmittalStatusWs(HttpServletRequest request,
			@RequestBody List<MaterialSubmittalStatus> entity, @RequestParam(name = "idProject") Long idProject) {
		try {
			return sendWsService.sendResult(request,
					materialSubmittalStatusService.saveOrUpdateMaterialSubmittalStatusWs(entity, idProject));
		} catch (Exception argEx) {
			logger.error("Error MaterialSubmittalStatusController in method putMaterialSubmittalStatusWs :: "
					+ argEx.toString());
			return sendWsService.sendResultException(request);
		}
	}

	@Operation(summary = "Supprimer l'objet ")
	@ApiResponses(value = { @ApiResponse(responseCode = ConstanteWs._CODE_WS_SUCCESS, description = "Success"),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR_ALIAS_PARAM, description = "Un ou plus param�tre(s) est null", content = @Content),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR_DELETE_ROW, description = "Impossible de supprimer l'objet car il est reference ", content = @Content),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR_IN_METHOD, description = "Erreur du methode", content = @Content),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR, description = "Erreur du service", content = @Content) })
	@DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> deleteMaterialSubmittalStatusByIdWs(HttpServletRequest request,
			@PathVariable("id") Long id) {
		try {
			return sendWsService.sendResult(request,
					materialSubmittalStatusService.deleteMaterialSubmittalStatusByIdWs(id));
		} catch (Exception argEx) {
			logger.error("Error MaterialSubmittalStatusController in method deleteMaterialSubmittalStatusByIdWs :: "
					+ argEx.toString());
			return sendWsService.sendResultException(request);
		}
	}

	@Operation(summary = "Recuperation liste de donnees avec filtre de recherche")
	@ApiResponses(value = { @ApiResponse(responseCode = ConstanteWs._CODE_WS_SUCCESS, description = "Success"),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR_ALIAS_PARAM, description = "Un ou plus param�tre(s) est null", content = @Content),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR_IN_METHOD, description = "Erreur du methode", content = @Content),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR, description = "Erreur du service", content = @Content) })
	@PostMapping(value = "/data", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getDataMaterialSubmittalStatusWs(HttpServletRequest request,
			@RequestBody SearchObject obj) {
		try {
			return sendWsService.sendResult(request,
					commonService.getListPaginator(obj, new MaterialSubmittalStatus(), null));
		} catch (Exception argEx) {
			logger.error("Error MaterialSubmittalStatusController in method getDataMaterialSubmittalStatusWs :: "
					+ argEx.toString());
			return sendWsService.sendResultException(request);
		}
	}

	@Operation(summary = "Charts")
	@ApiResponses(value = { @ApiResponse(responseCode = ConstanteWs._CODE_WS_SUCCESS, description = "Success"),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR_ALIAS_PARAM, description = "Un ou plus param�tre(s) est null", content = @Content),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR_IN_METHOD, description = "Erreur du methode", content = @Content),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR, description = "Erreur du service", content = @Content) })
	@GetMapping(value = "/charts", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getListMaterialSubmittalStatusChart(HttpServletRequest request,
			@RequestParam(name = "idProject") Long idProject) {
		try {
			return sendWsService.sendResult(request,
					materialSubmittalStatusService.getMaterialSubmittalStatusByIdWs(idProject));
		} catch (Exception argEx) {
			logger.error("Error MaterialSubmittalStatusController in method getListMaterialSubmittalStatusChart :: "
					+ argEx.toString());
			return sendWsService.sendResultException(request);
		}
	}

}
