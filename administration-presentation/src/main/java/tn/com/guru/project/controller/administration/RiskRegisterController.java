package tn.com.guru.project.controller.administration;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import tn.com.guru.project.administration.model.RiskRegister;
import tn.com.guru.project.administration.service.IRiskRegisterService;
import tn.com.guru.project.common.service.ICommonService;
import tn.com.guru.project.common.service.ISendWsService;
import tn.com.guru.project.tools.ConstanteWs;
import tn.com.guru.project.tools.model.SearchObject;

@RestController
@RequestMapping("/riskRegister")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class RiskRegisterController {

	private static final Logger logger = LogManager.getLogger(RiskRegisterController.class);

	@Autowired
	private IRiskRegisterService riskRegisterService;

	@Autowired
	private ISendWsService sendWsService;

	@Autowired
	private ICommonService commonService;

	@Operation(summary = "Recuperation liste de donnees sans filtre de recherche")
	@ApiResponses(value = { @ApiResponse(responseCode = ConstanteWs._CODE_WS_SUCCESS, description = "Success"),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR_ALIAS_PARAM, description = "Un ou plus param�tre(s) est null", content = @Content),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR_IN_METHOD, description = "Erreur du methode", content = @Content),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR, description = "Erreur du service", content = @Content) })
	@GetMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getListRiskRegisterWs(HttpServletRequest request) {
		try {
			return sendWsService.sendResult(request, riskRegisterService.getListRiskRegisterWs());
		} catch (Exception argEx) {
			logger.error("Error RiskRegisterController in method getListRiskRegisterWs :: " + argEx.toString());
			return sendWsService.sendResultException(request);
		}
	}

	@Operation(summary = "Recuperation un objet d'apres son id")
	@ApiResponses(value = { @ApiResponse(responseCode = ConstanteWs._CODE_WS_SUCCESS, description = "Success"),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR_ALIAS_PARAM, description = "Un ou plus param�tre(s) est null", content = @Content),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR_IN_METHOD, description = "Erreur du methode", content = @Content),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR, description = "Erreur du service", content = @Content) })
	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getRiskRegisterByIdWs(HttpServletRequest request, @PathVariable(name = "id") Long id) {
		try {
			return sendWsService.sendResult(request, riskRegisterService.findRiskRegisterByIdWs(id));
		} catch (Exception argEx) {
			logger.error("Error RiskRegisterController in method getRiskRegisterByIdWs :: " + argEx.toString());
			return sendWsService.sendResultException(request);
		}
	}

	@Operation(summary = "Insert un nouveau objet")
	@ApiResponses(value = { @ApiResponse(responseCode = ConstanteWs._CODE_WS_SUCCESS, description = "Success"),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR_ALIAS_PARAM, description = "Un ou plus param�tre(s) est null", content = @Content),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR_IN_METHOD, description = "Erreur du methode", content = @Content),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR, description = "Erreur du service", content = @Content) })
	@PostMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> pushRiskRegister(HttpServletRequest request, @RequestBody List<RiskRegister> entity,
			@RequestParam(name = "idProject") Long idProject) {
		try {
			return sendWsService.sendResult(request, riskRegisterService.saveOrUpdateRiskRegisterWs(entity, idProject));
		} catch (Exception argEx) {
			logger.error("Error RiskRegisterController in method pushRiskRegisterWs :: " + argEx.toString());
			return sendWsService.sendResultException(request);
		}
	}

	@Operation(summary = "Mise a jour l'objet")
	@ApiResponses(value = { @ApiResponse(responseCode = ConstanteWs._CODE_WS_SUCCESS, description = "Success"),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR_ALIAS_PARAM, description = "Un ou plus param�tre(s) est null", content = @Content),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR_IN_METHOD, description = "Erreur du methode", content = @Content),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR, description = "Erreur du service", content = @Content) })
	@PutMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> putRiskRegisterWs(HttpServletRequest request, @RequestBody List<RiskRegister> entity,
			@RequestParam(name = "idProject") Long idProject) {
		try {
			return sendWsService.sendResult(request, riskRegisterService.saveOrUpdateRiskRegisterWs(entity, idProject));
		} catch (Exception argEx) {
			logger.error("Error RiskRegisterController in method putRiskRegisterWs :: " + argEx.toString());
			return sendWsService.sendResultException(request);
		}
	}

	@Operation(summary = "Supprimer l'objet ")
	@ApiResponses(value = { @ApiResponse(responseCode = ConstanteWs._CODE_WS_SUCCESS, description = "Success"),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR_ALIAS_PARAM, description = "Un ou plus param�tre(s) est null", content = @Content),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR_DELETE_ROW, description = "Impossible de supprimer l'objet car il est reference ", content = @Content),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR_IN_METHOD, description = "Erreur du methode", content = @Content),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR, description = "Erreur du service", content = @Content) })
	@DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> deleteRiskRegisterByIdWs(HttpServletRequest request, @PathVariable("id") Long id) {
		try {
			return sendWsService.sendResult(request, riskRegisterService.deleteRiskRegisterByIdWs(id));
		} catch (Exception argEx) {
			logger.error("Error RiskRegisterController in method deleteRiskRegisterByIdWs :: " + argEx.toString());
			return sendWsService.sendResultException(request);
		}
	}

	@Operation(summary = "Recuperation liste de donnees avec filtre de recherche")
	@ApiResponses(value = { @ApiResponse(responseCode = ConstanteWs._CODE_WS_SUCCESS, description = "Success"),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR_ALIAS_PARAM, description = "Un ou plus param�tre(s) est null", content = @Content),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR_IN_METHOD, description = "Erreur du methode", content = @Content),
			@ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR, description = "Erreur du service", content = @Content) })
	@PostMapping(value = "/data", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getDataRiskRegisterWs(HttpServletRequest request, @RequestBody SearchObject obj) {
		try {
			return sendWsService.sendResult(request, commonService.getListPaginator(obj, new RiskRegister(), null));
		} catch (Exception argEx) {
			logger.error("Error RiskRegisterController in method getDataRiskRegisterWs :: " + argEx.toString());
			return sendWsService.sendResultException(request);
		}
	}

}
