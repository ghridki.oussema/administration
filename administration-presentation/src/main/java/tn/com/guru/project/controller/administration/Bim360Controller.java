package tn.com.guru.project.controller.administration;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import tn.com.guru.project.administration.model.Camera;
import tn.com.guru.project.administration.service.IBim360Service;
import tn.com.guru.project.administration.service.ICameraService;
import tn.com.guru.project.common.service.ICommonService;
import tn.com.guru.project.common.service.ISendWsService;
import tn.com.guru.project.tools.ConstanteWs;
import tn.com.guru.project.tools.model.SearchObject;

@RestController
@RequestMapping("/bim360")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class Bim360Controller {

    private static final Logger logger = LogManager.getLogger(CameraController.class);

    private final IBim360Service bim360Service;

    private final ISendWsService sendWsService;

    public Bim360Controller(IBim360Service bim360Service, ISendWsService sendWsService) {
        this.bim360Service = bim360Service;
        this.sendWsService = sendWsService;
    }

    @Operation(summary = "Recuperation liste de donnees sans filtre de recherche")
    @ApiResponses(value = {@ApiResponse(responseCode = ConstanteWs._CODE_WS_SUCCESS, description = "Success"),
            @ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR_ALIAS_PARAM, description = "Un ou plus param�tre(s) est null", content = @Content),
            @ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR_IN_METHOD, description = "Erreur du methode", content = @Content),
            @ApiResponse(responseCode = ConstanteWs._CODE_WS_ERROR, description = "Erreur du service", content = @Content)})
    @PostMapping(value = "/documentManagement", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getBim360DocumentManagementWs(HttpServletRequest request,
                                                           @RequestParam(value = "clientId") String clientId,
                                                            @RequestParam(value = "clientSecret") String clientSecret,
                                                           @RequestParam(value = "projectId", required = false) String projectId) {
        try {
            return sendWsService.sendResult(request, bim360Service.getBim360DocumentManagementWs(clientId, clientSecret, projectId));
        } catch (Exception argEx) {
            logger.error("Error Bim360Controller in method getBim360DocumentManagementWs :: " + argEx.toString());
            return sendWsService.sendResultException(request);
        }
    }
}