package tn.com.guru.mailing;

import tn.com.guru.project.tools.model.EmailModal;
import tn.com.guru.project.tools.model.SendObject;

public interface IMailingService {
	
	public SendObject sendEmailWs(EmailModal email);

}
