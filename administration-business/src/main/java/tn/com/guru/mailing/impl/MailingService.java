package tn.com.guru.mailing.impl;

import java.nio.charset.StandardCharsets;

import javax.mail.internet.MimeMessage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;

import tn.com.guru.mailing.IMailingService;
import tn.com.guru.project.tools.ConstanteService;
import tn.com.guru.project.tools.ConstanteWs;
import tn.com.guru.project.tools.UtilsWs;
import tn.com.guru.project.tools.model.EmailModal;
import tn.com.guru.project.tools.model.SendObject;

@Service
public class MailingService implements IMailingService {
	
	private static final Logger logger = LogManager.getLogger(MailingService.class);
	
	@Autowired
	private UtilsWs utilsWs;
	
	private TemplateEngine templateEngine;

	@Autowired
	private JavaMailSender javaMailSender;

	public String regex = "^(.+)@(.+)$";

	@Autowired
	public MailingService(TemplateEngine templateEngine) {
		this.templateEngine = templateEngine;
	}

	@Override
	public SendObject sendEmailWs(EmailModal email) {
		try {
			MimeMessage mimeMessage = javaMailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED, StandardCharsets.UTF_8.name());
			SendObject so = this.getContextEmail(email);
			helper.setSubject(email.getTitle());
			helper.setTo(email.getEmailSend());
			helper.setFrom(email.getEmailFrom());
			helper.setText(email.getBody(), true);
			javaMailSender.send(mimeMessage);
			return utilsWs.resultWs(ConstanteWs._CODE_WS_SUCCESS, new JSONObject());
		} catch (Exception e) {
			logger.error("Error MailingService in method sendEmailWs :: " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONArray());
		}
	}

	public SendObject getContextEmail(EmailModal email) {
		try {
			switch (email.getTemplate()) {
//			case Constante.CODE_TEMPLATE_DEM_ECL:
//				EmailObject emailObject = this.getEpEmailSupplier(entity, template);
//				so.setPayload(emailObject);
//				return so;
//			default:
//				so.setCode(ConstanteService._CODE_SERVICE_ERROR_IN_METHOD);
//				return so;
			}
			return new SendObject(ConstanteService._CODE_SERVICE_SUCCESS);
		} catch (Exception e) {
			return new SendObject(ConstanteService._CODE_SERVICE_ERROR_IN_METHOD);
		}
	}

}
