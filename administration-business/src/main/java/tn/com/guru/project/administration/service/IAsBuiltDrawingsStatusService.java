package tn.com.guru.project.administration.service;

import java.util.List;

import tn.com.guru.project.administration.model.AsBuiltDrawingsStatus;
import tn.com.guru.project.tools.model.SendObject;

public interface IAsBuiltDrawingsStatusService {

	public List<AsBuiltDrawingsStatus> getList();

	public AsBuiltDrawingsStatus findById(Long id);

	public AsBuiltDrawingsStatus saveOrUpdate(AsBuiltDrawingsStatus entity);

	public Boolean deleteById(Long id);

	public SendObject findAsBuiltDrawingsStatusByIdWs(Long id);

	public SendObject getListAsBuiltDrawingsStatusWs();

	public SendObject saveOrUpdateAsBuiltDrawingsStatusWs(List<AsBuiltDrawingsStatus> entity,Long idProject);

	public SendObject deleteAsBuiltDrawingsStatusByIdWs(Long id);
	
	public SendObject getListAsBuiltDrawingsStatusByIdWs(Long idProject);


}
