package tn.com.guru.project.administration.service;

import java.util.List;

import tn.com.guru.project.administration.model.AdmLogData;
import tn.com.guru.project.tools.model.SendObject;

public interface IAdmLogDataService {

	public List<AdmLogData> getList();

	public AdmLogData findById(Long id);

	public AdmLogData saveOrUpdate(AdmLogData entity);

	public Boolean deleteById(Long id);

	public SendObject findAdmLogDataByIdWs(Long id);

	public SendObject getListAdmLogDataWs();

	public SendObject saveOrUpdateAdmLogDataWs(AdmLogData entity);

	public SendObject deleteAdmLogDataByIdWs(Long id);

}
