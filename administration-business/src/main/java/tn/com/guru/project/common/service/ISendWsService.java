package tn.com.guru.project.common.service;

import javax.servlet.http.HttpServletRequest;

import org.apache.maven.artifact.repository.Authentication;
import org.springframework.http.ResponseEntity;

import tn.com.guru.project.tools.model.SendObject;
import tn.com.guru.project.view.model.VAdmUser;

public interface ISendWsService {

	public ResponseEntity<?> sendResult(HttpServletRequest request, SendObject so);

	public ResponseEntity<?> sendResultException(HttpServletRequest request);

	public ResponseEntity<?> sendResultPublic(HttpServletRequest request, SendObject so);
	
	public Long getIdCurrentUser(HttpServletRequest request);

    public VAdmUser getCurrentUser(HttpServletRequest request);

	public ResponseEntity<?> sendResultException(Authentication authentication, String string, String codeGet);



}
