package tn.com.guru.project.administration.service.impl;

import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tn.com.guru.project.administration.model.AsBuiltDrawingsStatus;
import tn.com.guru.project.administration.model.HseReport;
import tn.com.guru.project.administration.repository.IHseReportRepository;
import tn.com.guru.project.administration.service.IHseReportService;
import tn.com.guru.project.common.service.ICommonService;
import tn.com.guru.project.tools.ConstanteService;
import tn.com.guru.project.tools.ConstanteWs;
import tn.com.guru.project.tools.UtilsWs;
import tn.com.guru.project.tools.model.SendObject;

@Service
public class HseReportService implements IHseReportService {

	private static final Logger logger = LogManager.getLogger(HseReportService.class);

	@Autowired
	private IHseReportRepository hseReportRepository;

	@Autowired
	private ICommonService commonService;

	@Autowired
	private UtilsWs utilsWs;

	@Override
	public List<HseReport> getList() {
		try {
			return hseReportRepository.findAll();
		} catch (Exception e) {
			logger.error("Error HseReportService in method getList :: " + e.toString());
			return null;
		}
	}

	@Override
	public HseReport findById(Long id) {
		try {
			SendObject sendObject = commonService.getObjectById(new HseReport(), id.toString(), false);
			if (sendObject.getCode().equals(ConstanteService._CODE_SERVICE_SUCCESS))
				return (HseReport) sendObject.getPayload();
			return null;
		} catch (Exception e) {
			logger.error("Error HseReportService in method findById :: " + e.toString());
			return null;
		}
	}

	@Override
	public HseReport saveOrUpdate(HseReport entity) {
		try {
			return hseReportRepository.save(entity);
		} catch (Exception e) {
			logger.error("Error HseReportService in method saveOrUpdate :: " + e.toString());
			return null;
		}
	}

	@Override
	public Boolean deleteById(Long id) {
		try {
			if (id == null)
				return false;
			hseReportRepository.delete(this.findById(id));
			return true;
		} catch (Exception e) {
			logger.error("Error HseReportService in method deleteById :: " + e.toString());
			return false;
		}
	}

	@Override
	public SendObject findHseReportByIdWs(Long id) {
		try {
			if (id == null)
				return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_ALIAS_PARAM, new JSONObject());
			HseReport entity = this.findById(id);
			if (entity == null)
				return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_NOT_EXISTS_ROW_DATA_BASE, new JSONObject());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_SUCCESS, new JSONObject(entity));
		} catch (Exception e) {
			logger.error("Error HseReportService in method findHseReportByIdWs :: " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	@Override
	public SendObject getListHseReportWs() {
		try {
			return utilsWs.resultWs(ConstanteWs._CODE_WS_SUCCESS, new JSONArray(this.getList()));
		} catch (Exception e) {
			logger.error("Error HseReportService in method getListHseReportWs() :: " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	@Override
	public SendObject saveOrUpdateHseReportWs(List<HseReport> entityList, Long idProject) {
		try {
			if (!entityList.isEmpty())
				hseReportRepository.deleteByIdProjet(idProject);

			for (HseReport entity : entityList) {
				entity.setCreatedAt(new Date());
				entity.setIdProjet(idProject);
				entity = this.saveOrUpdate(entity);
				if (entity == null)
					return utilsWs.resultWs(ConstanteService._CODE_SERVICE_ERROR_SAVE_OR_UPDATE, new JSONObject());
			}

			return utilsWs.resultWs(ConstanteService._CODE_SERVICE_SUCCESS, new JSONArray(entityList));
		} catch (Exception e) {
			logger.error("Error HseReportService in method saveOrUpdateHseReportWs :: " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	@Override
	public SendObject deleteHseReportByIdWs(Long id) {
		try {
			Boolean resultDelete = this.deleteById(id);
			if (resultDelete == false)
				return utilsWs.resultWs(ConstanteService._CODE_SERVICE_ERROR_DELETE_ROW, new JSONObject());
			return utilsWs.resultWs(ConstanteService._CODE_SERVICE_SUCCESS, new JSONObject());
		} catch (Exception e) {
			logger.error("Error HseReportService in method getListHseReport " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	@Override
	public SendObject getListHseReporByIdtWs(Long idProject) {
		try {

			List<HseReport> list = hseReportRepository.getListByIdProjet(idProject);

			JSONObject jo;
			JSONObject joo;
			JSONArray ja = new JSONArray();
			JSONArray jaa = new JSONArray();
			for (HseReport e : list) {
				jo = new JSONObject();
				joo = new JSONObject();
				jaa = new JSONArray();
				jo.put("name", e.getDiscription());
				joo.put("name", "Contractor this week");
				joo.put("value", e.getContractorthisweek());
				jaa.put(joo);
				joo = new JSONObject();
				joo.put("name", "Subcontractor this week");
				joo.put("value", e.getSubcontractorthisweek());
				jaa.put(joo);
				jo.put("series", jaa);
				ja.put(jo);

			}

			return utilsWs.resultWs(ConstanteWs._CODE_WS_SUCCESS, ja);
		} catch (Exception e) {
			logger.error("Error HseReportService in method getListHseReporByIdtWs() :: " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

}
