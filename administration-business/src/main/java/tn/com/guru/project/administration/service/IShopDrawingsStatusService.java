package tn.com.guru.project.administration.service;

import java.util.List;

import tn.com.guru.project.administration.model.ShopDrawingsStatus;
import tn.com.guru.project.tools.model.SendObject;

public interface IShopDrawingsStatusService {

	public List<ShopDrawingsStatus> getList();

	public ShopDrawingsStatus findById(Long id);

	public ShopDrawingsStatus saveOrUpdate(ShopDrawingsStatus entity);

	public Boolean deleteById(Long id);

	public SendObject findShopDrawingsStatusByIdWs(Long id);

	public SendObject getListShopDrawingsStatusWs();

	public SendObject saveOrUpdateShopDrawingsStatusWs(List<ShopDrawingsStatus> entity,Long idProject);

	public SendObject deleteShopDrawingsStatusByIdWs(Long id);
	
	public SendObject getListShopDrawingsStatusByIdWs(Long idProject);

}
