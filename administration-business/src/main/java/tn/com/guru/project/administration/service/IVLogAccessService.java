package tn.com.guru.project.administration.service;

import java.util.List;

import tn.com.guru.project.tools.model.SendObject;
import tn.com.guru.project.view.model.VLogAccess;

public interface IVLogAccessService {

	public List<VLogAccess> getList();

	public VLogAccess findById(Long id);

	public VLogAccess saveOrUpdate(VLogAccess entity);

	public Boolean deleteById(Long id);

	public SendObject findVLogAccessByIdWs(Long id);

	public SendObject getListVLogAccessWs();

	public SendObject saveOrUpdateVLogAccessWs(VLogAccess entity);

	public SendObject deleteVLogAccessByIdWs(Long id);

}
