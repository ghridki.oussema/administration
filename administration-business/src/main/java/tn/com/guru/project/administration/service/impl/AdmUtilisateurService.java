package tn.com.guru.project.administration.service.impl;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import tn.com.guru.project.administration.model.AdmPersonnel;
import tn.com.guru.project.administration.model.AdmUtilisateur;
import tn.com.guru.project.administration.model.AdmUtilisateurProfil;
import tn.com.guru.project.administration.repository.IAdmUtilisateurRepository;
import tn.com.guru.project.administration.service.IAdmPersonnelService;
import tn.com.guru.project.administration.service.IAdmProfilService;
import tn.com.guru.project.administration.service.IAdmUtilisateurProfilService;
import tn.com.guru.project.administration.service.IAdmUtilisateurService;
import tn.com.guru.project.common.service.ICommonService;
import tn.com.guru.project.tools.ConstanteService;
import tn.com.guru.project.tools.ConstanteWs;
import tn.com.guru.project.tools.UtilsWs;
import tn.com.guru.project.tools.model.SendObject;

@Service
public class AdmUtilisateurService implements IAdmUtilisateurService {

	private static final Logger logger = LogManager.getLogger(AdmUtilisateurService.class);

	@Autowired
	private IAdmUtilisateurRepository admUtilisateurRepository;

	@Autowired
	private ICommonService commonService;

	@Autowired
	private UtilsWs utilsWs;

	@Autowired
	private PasswordEncoder bcryptEncoder;

	@Autowired
	private IAdmPersonnelService admPersonnelService;

	@Autowired
	private IAdmUtilisateurProfilService admUserProfilService;

	@Autowired
	private IAdmProfilService admProfilService;

	@Override
	public List<AdmUtilisateur> getList() {
		try {
			return admUtilisateurRepository.findAll();
		} catch (Exception e) {
			logger.error("Error AdmUtilisateurService in method getList :: " + e.toString());
			return null;
		}
	}

	@Override
	public AdmUtilisateur findById(Long id) {
		try {
			SendObject sendObject = commonService.getObjectById(new AdmUtilisateur(), id.toString(), false);
			if (sendObject.getCode().equals(ConstanteService._CODE_SERVICE_SUCCESS))
				return (AdmUtilisateur) sendObject.getPayload();
			return null;
		} catch (Exception e) {
			logger.error("Error AdmUtilisateurService in method findById :: " + e.toString());
			return null;
		}
	}

	@Override
	public AdmUtilisateur saveOrUpdate(AdmUtilisateur entity) {
		try {
			return admUtilisateurRepository.save(entity);
		} catch (Exception e) {
			logger.error("Error AdmUtilisateurService in method saveOrUpdate :: " + e.toString());
			return null;
		}
	}

	@Override
	public Boolean deleteById(Long id) {
		try {
			if (id == null)
				return false;
			admUtilisateurRepository.delete(this.findById(id));
			return true;
		} catch (Exception e) {
			logger.error("Error AdmUtilisateurService in method deleteById :: " + e.toString());
			return false;
		}
	}

	@Override
	public SendObject findAdmUtilisateurByIdWs(Long id) {
		try {
			if (id == null)
				return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_ALIAS_PARAM, new JSONObject());
			AdmUtilisateur entity = this.findById(id);
			if (entity == null)
				return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_NOT_EXISTS_ROW_DATA_BASE, new JSONObject());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_SUCCESS, new JSONObject(entity));
		} catch (Exception e) {
			logger.error("Error AdmUtilisateurService in method findAdmUtilisateurByIdWs :: " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	@Override
	public SendObject getListAdmUtilisateurWs() {
		try {
			return utilsWs.resultWs(ConstanteWs._CODE_WS_SUCCESS, new JSONArray(this.getList()));
		} catch (Exception e) {
			logger.error("Error AdmUtilisateurService in method getListAdmUtilisateurWs() :: " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	@Override
	public SendObject saveOrUpdateAdmUtilisateurWs(AdmUtilisateur entity) {
		try {
			entity = this.saveOrUpdate(entity);
			if (entity == null)
				return utilsWs.resultWs(ConstanteService._CODE_SERVICE_ERROR_SAVE_OR_UPDATE, new JSONObject());
			return utilsWs.resultWs(ConstanteService._CODE_SERVICE_SUCCESS, new JSONObject(entity));
		} catch (Exception e) {
			logger.error("Error AdmUtilisateurService in method saveOrUpdateAdmUtilisateurWs :: " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	@Override
	public SendObject deleteAdmUtilisateurByIdWs(Long id) {
		try {
			Boolean resultDelete = this.deleteById(id);
			if (resultDelete == false)
				return utilsWs.resultWs(ConstanteService._CODE_SERVICE_ERROR_DELETE_ROW, new JSONObject());
			return utilsWs.resultWs(ConstanteService._CODE_SERVICE_SUCCESS, new JSONObject());
		} catch (Exception e) {
			logger.error("Error AdmUtilisateurService in method getListAdmUtilisateur " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	@Override
	public SendObject registerWs(AdmUtilisateur user) {
		try {
			AdmPersonnel admPersonnel = admPersonnelService.saveOrUpdate(user.getAdmPersonnel());
			if (admPersonnel.getId() == null)
				return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_ALIAS_PARAM, new JSONObject());
			if (admUtilisateurRepository.uniqueAdmUserByLogin(user.getLogin()))
				return utilsWs.resultWs(ConstanteWs._CODE_WS_LOGIN_EXISTS, new JSONObject());
			if (!user.getPassword().equals(user.getConfirmPassword()))
				return utilsWs.resultWs(ConstanteWs._CODE_WS_PRB_IN_CONFIRM_PASSWORD, new JSONObject());
			user.setIdAdmPersonnel(admPersonnel.getId());
			user.setPassword(bcryptEncoder.encode(user.getPassword()));
			user.setActif(true);
			if (user.getDtAjout() == null)
				user.setDtAjout(new Timestamp(new Date().getTime()));
			user.setDtMaj(new Timestamp(new Date().getTime()));
			user = this.saveOrUpdate(user);
			if (user.getId() != null && user.getListProfiles() != null && !user.getListProfiles().isEmpty())
				for (AdmUtilisateurProfil profile : user.getListProfiles()) {
					AdmUtilisateurProfil userProfil = new AdmUtilisateurProfil();
					userProfil.setIdAdmProfil(profile.getIdAdmProfil());
					userProfil.setIdAdmUtilisateur(user.getId());
					userProfil.setDtMaj(new Date());
					admUserProfilService.saveOrUpdate(userProfil);
				}
			return utilsWs.resultWs(ConstanteWs._CODE_WS_SUCCESS, new JSONObject(user));
		} catch (Exception argEx) {
			logger.error("Error AdmUserService in method registerWs :: " + argEx.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	@Override
	public SendObject updateUserWs(AdmUtilisateur user) {
		try {
			AdmPersonnel admPersonnel = admPersonnelService.saveOrUpdate(user.getAdmPersonnel());
			AdmUtilisateur admUser = this.findById(user.getId());
			if (admPersonnel.getId() == null)
				return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_ALIAS_PARAM, new JSONObject());
			if (user.getPassword() != null && !user.getPassword().equals("")
					&& !user.getPassword().equals(user.getConfirmPassword()))
				return utilsWs.resultWs(ConstanteWs._CODE_WS_PRB_IN_CONFIRM_PASSWORD, new JSONObject());
			else if (user.getPassword() != null && !user.getPassword().equals("")
					&& user.getPassword().equals(user.getConfirmPassword()))
				user.setPassword(bcryptEncoder.encode(user.getPassword()));
			else if (user.getPassword() == null)
				user.setPassword(admUser.getPassword());
			user.setIdAdmPersonnel(admPersonnel.getId());
			user.setDtMaj(new Date());
			user.setDtAjout(admUser.getDtAjout());
			this.saveOrUpdate(user);
			List<AdmUtilisateurProfil> listUserProfil = admUserProfilService.getListUserProfilesByIdUser(user.getId());
			if (!listUserProfil.isEmpty())
				for (AdmUtilisateurProfil admUtilisateurProfil : listUserProfil) {
					admUserProfilService.deleteAdmUtilisateurProfilByIdWs(admUtilisateurProfil.getId());
				}
			if (!user.getListProfiles().isEmpty())
				for (AdmUtilisateurProfil admUtilisateurProfil : user.getListProfiles()) {
					admUserProfilService.saveOrUpdate(admUtilisateurProfil);
				}
			return utilsWs.resultWs(ConstanteWs._CODE_WS_SUCCESS, new JSONObject(user));
		} catch (Exception argEx) {
			logger.error("Error AdmUserService in method updateUserWs :: " + argEx.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

}
