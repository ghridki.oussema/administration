package tn.com.guru.project.administration.service.impl;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tn.com.guru.project.administration.model.AdmFonc;
import tn.com.guru.project.administration.model.AdmFoncProfil;
import tn.com.guru.project.administration.model.AdmProfil;
import tn.com.guru.project.administration.repository.IAdmProfilRepository;
import tn.com.guru.project.administration.service.IAdmFoncProfilService;
import tn.com.guru.project.administration.service.IAdmFoncService;
import tn.com.guru.project.administration.service.IAdmProfilService;
import tn.com.guru.project.common.service.ICommonService;
import tn.com.guru.project.tools.ConstanteService;
import tn.com.guru.project.tools.ConstanteWs;
import tn.com.guru.project.tools.UtilsWs;
import tn.com.guru.project.tools.model.SendObject;

@Service
public class AdmProfilService implements IAdmProfilService {

	private static final Logger logger = LogManager.getLogger(AdmProfilService.class);

	@Autowired
	private IAdmProfilRepository admProfilRepository;

	@Autowired
	private ICommonService commonService;

	@Autowired
	private UtilsWs utilsWs;

	@Autowired
	private IAdmFoncProfilService admFoncProfilService;

	@Autowired
	private IAdmFoncService admFoncService;

	@Override
	public List<AdmProfil> getList() {
		try {
			return admProfilRepository.findAll();
		} catch (Exception e) {
			logger.error("Error AdmProfilService in method getList :: " + e.toString());
			return null;
		}
	}

	@Override
	public AdmProfil findById(Long id) {
		try {
			SendObject sendObject = commonService.getObjectById(new AdmProfil(), id.toString(), false);
			if (sendObject.getCode().equals(ConstanteService._CODE_SERVICE_SUCCESS))
				return (AdmProfil) sendObject.getPayload();
			return null;
		} catch (Exception e) {
			logger.error("Error AdmProfilService in method findById :: " + e.toString());
			return null;
		}
	}

	@Override
	public AdmProfil saveOrUpdate(AdmProfil entity) {
		try {

			if (entity.getDtAjout() == null)
				entity.setDtAjout(new Timestamp(new Date().getTime()));
			if (entity.getDtMaj() == null)
				entity.setDtMaj(new Timestamp(new Date().getTime()));
			return admProfilRepository.save(entity);
		} catch (Exception e) {
			logger.error("Error AdmProfilService in method saveOrUpdate :: " + e.toString());
			return null;
		}
	}

	@Override
	public Boolean deleteById(Long id) {
		try {
			if (id == null)
				return false;
			admProfilRepository.delete(this.findById(id));
			return true;
		} catch (Exception e) {
			logger.error("Error AdmProfilService in method deleteById :: " + e.toString());
			return false;
		}
	}

	@Override
	public SendObject findAdmProfilByIdWs(Long id) {
		try {
			if (id == null)
				return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_ALIAS_PARAM, new JSONObject());
			AdmProfil admProfil = this.findById(id);
			if (admProfil == null)
				return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_NOT_EXISTS_ROW_DATA_BASE, new JSONObject());

			JSONObject js = new JSONObject();
			List<AdmFonc> listAdmFonc = admFoncService.getList();
			List<AdmFonc> listAdmFoncChecked = admFoncService.getMenuByIdProfil(admProfil.getId());
			List<AdmFonc> listParent = listAdmFonc.stream().filter(x -> x.getIdAdmFonc() == null)
					.collect(Collectors.toList());
			if (!listAdmFoncChecked.isEmpty())
				for (AdmFonc admFoncChecked : listAdmFoncChecked) {
					for (AdmFonc admFonc : listParent) {
						List<AdmFonc> listChilds = admFoncService.getChildMenu(admFonc.getMenuId());
						if (admFoncChecked.equals(admFonc))
							admFonc.setIsChecked(true);
						for (AdmFonc childs : listChilds) {
							if (admFoncChecked.equals(childs))
								childs.setIsChecked(true);
						}
						admFonc.setSubmenus(listChilds);
					}
				}
			else
				for (AdmFonc admFonc : listParent) {
					List<AdmFonc> listChilds = admFoncService.getChildMenu(admFonc.getMenuId());

					admFonc.setSubmenus(listChilds);
				}

			js.put("admProfil", new JSONObject(admProfil));
			js.put("listParent", listParent);

			return utilsWs.resultWs(ConstanteWs._CODE_WS_SUCCESS, js);
		} catch (Exception e) {
			logger.error("Error AdmProfilService in method findAdmProfilByIdWs :: " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	@Override
	public SendObject getListAdmProfilWs() {
		try {
			return utilsWs.resultWs(ConstanteWs._CODE_WS_SUCCESS, new JSONArray(this.getList()));
		} catch (Exception e) {
			logger.error("Error AdmProfilService in method getListAdmProfilWs() :: " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	@Override
	public SendObject saveOrUpdateAdmProfilWs(AdmProfil entity) {
		try {

			this.saveOrUpdate(entity);
			for (AdmFonc e : entity.getListAdmFonc()) {
				AdmFoncProfil admFoncProm = new AdmFoncProfil();
				admFoncProm.setDtMaj(new Date());
				admFoncProm.setIdAdmFonc(e.getMenuId());
				admFoncProm.setIdAdmProfil(entity.getId());
				admFoncProfilService.saveOrUpdate(admFoncProm);
				if (e.getSubmenus() != null && !e.getSubmenus().isEmpty())
					for (AdmFonc e1 : e.getSubmenus()) {
						AdmFoncProfil admFoncProm1 = new AdmFoncProfil();
						admFoncProm1.setDtMaj(new Date());
						admFoncProm1.setIdAdmFonc(e1.getMenuId());
						admFoncProm1.setIdAdmProfil(entity.getId());
						admFoncProfilService.saveOrUpdate(admFoncProm1);
					}
			}

			return utilsWs.resultWs(ConstanteService._CODE_SERVICE_SUCCESS, new JSONObject(entity));
		} catch (Exception e) {
			logger.error("Error AdmProfilService in method saveOrUpdateAdmProfilWs :: " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	@Override
	public SendObject deleteAdmProfilByIdWs(Long id) {
		try {
			Boolean resultDelete = this.deleteById(id);
			if (resultDelete == false)
				return utilsWs.resultWs(ConstanteService._CODE_SERVICE_ERROR_DELETE_ROW, new JSONObject());
			List<AdmFoncProfil> listAdmFoncProfil = admFoncProfilService.getListAdmFoncProfilByIdProfil(id);
			if (listAdmFoncProfil != null)
				for (AdmFoncProfil admFoncProfil : listAdmFoncProfil) {
					admFoncProfilService.deleteById(admFoncProfil.getId());

				}

			return utilsWs.resultWs(ConstanteService._CODE_SERVICE_SUCCESS, new JSONObject());
		} catch (Exception e) {
			logger.error("Error AdmProfilService in method getListAdmProfil " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	@Override
	public List<AdmProfil> getListProfilesByIdUser(Long idUser) {
		// TODO Auto-generated method stub
		return admProfilRepository.getListProfilesByIdUser(idUser);
	}

	@Override
	public SendObject updateAdmProfilWs(AdmProfil entity) {
		try {

			this.saveOrUpdate(entity);
			List<AdmFoncProfil> listAdmFoncProfil = admFoncProfilService.getListAdmFoncProfilByIdProfil(entity.getId());
			if (listAdmFoncProfil != null)
				for (AdmFoncProfil admFoncProfil : listAdmFoncProfil) {
					admFoncProfilService.deleteById(admFoncProfil.getId());
				}

			for (AdmFonc e : entity.getListAdmFonc()) {
				AdmFoncProfil admFoncProm = new AdmFoncProfil();
				admFoncProm.setDtMaj(new Date());
				admFoncProm.setIdAdmFonc(e.getMenuId());
				admFoncProm.setIdAdmProfil(entity.getId());
				admFoncProfilService.saveOrUpdate(admFoncProm);
				if (e.getSubmenus() != null && !e.getSubmenus().isEmpty())
					for (AdmFonc e1 : e.getSubmenus()) {
						AdmFoncProfil admFoncProm1 = new AdmFoncProfil();
						admFoncProm1.setDtMaj(new Date());
						admFoncProm1.setIdAdmFonc(e1.getMenuId());
						admFoncProm1.setIdAdmProfil(entity.getId());
						admFoncProfilService.saveOrUpdate(admFoncProm1);
					}

			}

			return utilsWs.resultWs(ConstanteService._CODE_SERVICE_SUCCESS, new JSONObject(entity));
		} catch (Exception e) {
			logger.error("Error AdmProfilService in method updateAdmProfilWs :: " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}


	@Override
	public SendObject getAdmProfilPaginated(Long lim, Long page) {

		try {
			Long off = page;

			List<AdmProfil> listparent = admProfilRepository.getAdmProfilPaginatedrepo(lim, off);
			Long count = admProfilRepository.getCountP();

			return utilsWs.resultWs(ConstanteService._CODE_SERVICE_SUCCESS, new JSONArray(listparent));
		} catch (Exception e) {
			logger.error("Error AdmProfilService in method getAdmProfilPaginated " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}

	}

}
