package tn.com.guru.project.administration.service;

import tn.com.guru.project.administration.model.Document;
import tn.com.guru.project.tools.model.SendObject;

import java.util.List;

public interface IDocumentService {

	public List<Document> getList();

	public Document findById(Long id);

	public Document saveOrUpdate(Document entity);

	public Boolean deleteById(Long id);

	public SendObject findDocumentByIdWs(Long id);

	public SendObject getListDocumentWs();

	public SendObject saveOrUpdateDocumentWs(Document entity);

	public SendObject deleteDocumentByIdWs(Long id);
}
