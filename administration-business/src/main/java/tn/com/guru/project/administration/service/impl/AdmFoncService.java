package tn.com.guru.project.administration.service.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tn.com.guru.project.administration.model.AdmFonc;
import tn.com.guru.project.administration.repository.IAdmFoncRepository;
import tn.com.guru.project.administration.service.IAdmFoncService;
import tn.com.guru.project.common.service.ICommonService;
import tn.com.guru.project.tools.ConstanteService;
import tn.com.guru.project.tools.ConstanteWs;
import tn.com.guru.project.tools.UtilsWs;
import tn.com.guru.project.tools.model.SendObject;

@Service
public class AdmFoncService implements IAdmFoncService {

	private static final Logger logger = LogManager.getLogger(AdmFoncService.class);

	@Autowired
	private IAdmFoncRepository admFoncRepository;

	@Autowired
	private ICommonService commonService;

	@Autowired
	private UtilsWs utilsWs;

	@Override
	public List<AdmFonc> getList() {
		try {
			return admFoncRepository.getMenuParentOrderByCde();
		} catch (Exception e) {
			logger.error("Error AdmFoncService in method getList :: " + e.toString());
			return null;
		}
	}

	@Override
	public AdmFonc findById(Long id) {
		try {
			SendObject sendObject = commonService.getObjectById(new AdmFonc(), id.toString(), false);
			if (sendObject.getCode().equals(ConstanteService._CODE_SERVICE_SUCCESS))
				return (AdmFonc) sendObject.getPayload();
			return null;
		} catch (Exception e) {
			logger.error("Error AdmFoncService in method findById :: " + e.toString());
			return null;
		}
	}

	@Override
	public AdmFonc saveOrUpdate(AdmFonc entity) {
		try {
			if (entity.getDtMaj() == null)
				entity.setDtMaj(new Timestamp(new Date().getTime()));
			return admFoncRepository.save(entity);
		} catch (Exception e) {
			logger.error("Error AdmFoncService in method saveOrUpdate :: " + e.toString());
			return null;
		}
	}

	@Override
	public Boolean deleteById(Long id) {
		try {
			if (id == null)
				return false;
			admFoncRepository.delete(this.findById(id));
			return true;
		} catch (Exception e) {
			logger.error("Error AdmFoncService in method deleteById :: " + e.toString());
			return false;
		}
	}

	@Override
	public SendObject findAdmFoncByIdWs(Long id) {
		try {
			if (id == null)
				return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_ALIAS_PARAM, new JSONObject());
			AdmFonc entity = this.findById(id);
			if (entity == null)
				return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_NOT_EXISTS_ROW_DATA_BASE, new JSONObject());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_SUCCESS, new JSONObject(entity));
		} catch (Exception e) {
			logger.error("Error AdmFoncService in method findAdmFoncByIdWs :: " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	@Override
	public SendObject getListAdmFoncWs() {
		try {
			return utilsWs.resultWs(ConstanteWs._CODE_WS_SUCCESS, new JSONArray(this.getList()));
		} catch (Exception e) {
			logger.error("Error AdmFoncService in method getListAdmFoncWs() :: " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	@Override
	public SendObject saveOrUpdateAdmFoncWs(AdmFonc entity) {
		try {
			entity = this.saveOrUpdate(entity);
			if (entity == null)
				return utilsWs.resultWs(ConstanteService._CODE_SERVICE_ERROR_SAVE_OR_UPDATE, new JSONObject());
			return utilsWs.resultWs(ConstanteService._CODE_SERVICE_SUCCESS, new JSONObject(entity));
		} catch (Exception e) {
			logger.error("Error AdmFoncService in method saveOrUpdateAdmFoncWs :: " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	@Override
	public SendObject deleteAdmFoncByIdWs(Long id) {
		try {
			Boolean resultDelete = this.deleteById(id);
			if (resultDelete == false)
				return utilsWs.resultWs(ConstanteService._CODE_SERVICE_ERROR_DELETE_ROW, new JSONObject());
			return utilsWs.resultWs(ConstanteService._CODE_SERVICE_SUCCESS, new JSONObject());
		} catch (Exception e) {
			logger.error("Error AdmFoncService in method getListAdmFonc " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	public List<AdmFonc> getChildMenuByRole(Long idParent, List<Long> idRoles) {
		try {
			return admFoncRepository.getChildMenuByRole(idParent, idRoles);
		} catch (Exception e) {
			logger.error("Error AdmFoncService in method getChildMenu :: " + e.toString());
			return null;
		}
	}

	@Override
	public SendObject getMenuByIdUser(Long idUser, String lang) {
		try {
			if (idUser == null)
				return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_ALIAS_PARAM, new JSONObject());

			List<Long> idRoles = admFoncRepository.getRolesByIduser(idUser);
			List<AdmFonc> menu = new ArrayList<>();
			if (idRoles != null)
				menu = admFoncRepository.getMenuByRoles(idRoles);

			if (menu != null && !menu.isEmpty())

				for (AdmFonc admFonc : menu) {
					switch (lang) {
					case "ar":
						admFonc.setTitle(admFonc.getLibelleAr());
						break;
					case "fr":
						admFonc.setTitle(admFonc.getLibelleFr());
						break;
					case "en":
						admFonc.setTitle(admFonc.getLibelleEn());
						break;
					}
					admFonc.setTooltip(admFonc.getTitle());

					if (admFoncRepository.getChildMenuByRole(admFonc.getMenuId(), idRoles) != null) {
						List<AdmFonc> subMenus = admFoncRepository.getChildMenuByRole(admFonc.getMenuId(), idRoles);
						for (AdmFonc subMenu : subMenus) {
							switch (lang) {
							case "ar":
								subMenu.setTitle(subMenu.getLibelleAr());
								break;
							case "fr":
								subMenu.setTitle(subMenu.getLibelleFr());
								break;
							case "en":
								subMenu.setTitle(subMenu.getLibelleEn());
								break;
							}
							subMenu.setTooltip(subMenu.getTitle());
						}
						admFonc.setSubmenus(subMenus);
					}

				}

			return utilsWs.resultWs(ConstanteWs._CODE_WS_SUCCESS, new JSONArray(menu));
		} catch (Exception e) {
			logger.error("Error AdmFoncService in method findAdmFoncByIdWs :: " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	@Override
	public List<AdmFonc> getMenuByIdProfil(Long id) {
		try {
			return admFoncRepository.getMenuByIdProfil(id);
		} catch (Exception e) {
			logger.error("Error AdmFoncService in method getMenuByIdProfil :: " + e.toString());
			return null;
		}
	}

	@Override
	public List<AdmFonc> getChildMenu(Long idParent) {
		try {
			return admFoncRepository.getChildMenu(idParent);
		} catch (Exception e) {
			logger.error("Error AdmFoncService in method getChildMenu :: " + e.toString());
			return null;
		}
	}

	@Override
	public SendObject getMenu() {
		try {

			List<AdmFonc> listAdmFonc = this.getList();

			List<AdmFonc> listParent = listAdmFonc.stream().filter(x -> x.getIdAdmFonc() == null)
					.collect(Collectors.toList());

			for (AdmFonc admFonc : listParent) {
				List<AdmFonc> listChilds = this.getChildMenu(admFonc.getMenuId());
				admFonc.setSubmenus(listChilds);
			}

			return utilsWs.resultWs(ConstanteWs._CODE_WS_SUCCESS, new JSONArray(listParent));
		} catch (Exception e) {
			logger.error("Error AdmFoncService in method getMenu :: " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

}
