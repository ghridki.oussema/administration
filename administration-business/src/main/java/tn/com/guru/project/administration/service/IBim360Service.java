package tn.com.guru.project.administration.service;

import tn.com.guru.project.tools.model.SendObject;

public interface IBim360Service {

	SendObject getBim360DocumentManagementWs(String clientId, String clientSecret, String projectId);

}
