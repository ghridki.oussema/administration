package tn.com.guru.project.administration.service.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tn.com.guru.project.administration.model.AdmLogAccess;
import tn.com.guru.project.administration.repository.IAdmLogAccessRepository;
import tn.com.guru.project.administration.service.IAdmLogAccessService;
import tn.com.guru.project.common.service.ICommonService;
import tn.com.guru.project.tools.ConstanteService;
import tn.com.guru.project.tools.ConstanteWs;
import tn.com.guru.project.tools.UtilsWs;
import tn.com.guru.project.tools.model.SendObject;

@Service
public class AdmLogAccessService implements IAdmLogAccessService {

	private static final Logger logger = LogManager.getLogger(AdmLogAccessService.class);

	@Autowired
	private IAdmLogAccessRepository admLogAccessRepository;

	@Autowired
	private ICommonService commonService;

	@Autowired
	private UtilsWs utilsWs;

	@Override
	public List<AdmLogAccess> getList() {
		try {
			return admLogAccessRepository.findAll();
		} catch (Exception e) {
			logger.error("Error AdmLogAccessService in method getList :: " + e.toString());
			return null;
		}
	}

	@Override
	public AdmLogAccess findById(Long id) {
		try {
			SendObject sendObject = commonService.getObjectById(new AdmLogAccess(), id.toString(), false);
			if (sendObject.getCode().equals(ConstanteService._CODE_SERVICE_SUCCESS))
				return (AdmLogAccess) sendObject.getPayload();
			return null;
		} catch (Exception e) {
			logger.error("Error AdmLogAccessService in method findById :: " + e.toString());
			return null;
		}
	}

	@Override
	public AdmLogAccess saveOrUpdate(AdmLogAccess entity) {
		try {
			return admLogAccessRepository.save(entity);
		} catch (Exception e) {
			logger.error("Error AdmLogAccessService in method saveOrUpdate :: " + e.toString());
			return null;
		}
	}

	@Override
	public Boolean deleteById(Long id) {
		try {
			if (id == null)
				return false;
			admLogAccessRepository.delete(this.findById(id));
			return true;
		} catch (Exception e) {
			logger.error("Error AdmLogAccessService in method deleteById :: " + e.toString());
			return false;
		}
	}

	@Override
	public SendObject findAdmLogAccessByIdWs(Long id) {
		try {
			if (id == null)
				return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_ALIAS_PARAM, new JSONObject());
			AdmLogAccess entity = this.findById(id);
			if (entity == null)
				return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_NOT_EXISTS_ROW_DATA_BASE, new JSONObject());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_SUCCESS, new JSONObject(entity));
		} catch (Exception e) {
			logger.error("Error AdmLogAccessService in method findAdmLogAccessByIdWs :: " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	@Override
	public SendObject getListAdmLogAccessWs() {
		try {
			return utilsWs.resultWs(ConstanteWs._CODE_WS_SUCCESS, new JSONArray(this.getList()));
		} catch (Exception e) {
			logger.error("Error AdmLogAccessService in method getListAdmLogAccessWs() :: " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	@Override
	public SendObject saveOrUpdateAdmLogAccessWs(AdmLogAccess entity) {
		try {
			entity = this.saveOrUpdate(entity);
			if (entity == null)
				return utilsWs.resultWs(ConstanteService._CODE_SERVICE_ERROR_SAVE_OR_UPDATE, new JSONObject());
			return utilsWs.resultWs(ConstanteService._CODE_SERVICE_SUCCESS, new JSONObject(entity));
		} catch (Exception e) {
			logger.error("Error AdmLogAccessService in method saveOrUpdateAdmLogAccessWs :: " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	@Override
	public SendObject deleteAdmLogAccessByIdWs(Long id) {
		try {
			Boolean resultDelete = this.deleteById(id);
			if (resultDelete == false)
				return utilsWs.resultWs(ConstanteService._CODE_SERVICE_ERROR_DELETE_ROW, new JSONObject());
			return utilsWs.resultWs(ConstanteService._CODE_SERVICE_SUCCESS, new JSONObject());
		} catch (Exception e) {
			logger.error("Error AdmLogAccessService in method getListAdmLogAccess " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

}
