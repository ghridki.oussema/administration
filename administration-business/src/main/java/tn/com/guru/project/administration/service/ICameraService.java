package tn.com.guru.project.administration.service;

import java.util.List;

import tn.com.guru.project.administration.model.Camera;
import tn.com.guru.project.tools.model.SendObject;

public interface ICameraService {

	public List<Camera> getList();

	public Camera findById(Long id);

	public Camera saveOrUpdate(Camera entity);

	public Boolean deleteById(Long id);

	public SendObject findCameraByIdWs(Long id);

	public SendObject getListCameraWs();

	public SendObject saveOrUpdateCameraWs(Camera entity);

	public SendObject deleteCameraByIdWs(Long id);
}
