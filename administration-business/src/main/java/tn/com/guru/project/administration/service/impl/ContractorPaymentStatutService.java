package tn.com.guru.project.administration.service.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tn.com.guru.project.administration.model.ContractorPaymentStatut;
import tn.com.guru.project.administration.repository.IContractorPaymentStatutRepository;
import tn.com.guru.project.administration.service.IContractorPaymentStatutService;
import tn.com.guru.project.common.service.ICommonService;
import tn.com.guru.project.tools.ConstanteService;
import tn.com.guru.project.tools.ConstanteWs;
import tn.com.guru.project.tools.UtilsWs;
import tn.com.guru.project.tools.model.SendObject;

@Service
public class ContractorPaymentStatutService implements IContractorPaymentStatutService {

	private static final Logger logger = LogManager.getLogger(ContractorPaymentStatutService.class);

	@Autowired
	private IContractorPaymentStatutRepository contractorPaymentStatutRepository;

	@Autowired
	private ICommonService commonService;

	@Autowired
	private UtilsWs utilsWs;

	@Override
	public List<ContractorPaymentStatut> getList() {
		try {
			return contractorPaymentStatutRepository.findAll();
		} catch (Exception e) {
			logger.error("Error ContractorPaymentStatutService in method getList :: " + e.toString());
			return null;
		}
	}

	@Override
	public ContractorPaymentStatut findById(Long id) {
		try {
			SendObject sendObject = commonService.getObjectById(new ContractorPaymentStatut(), id.toString(), false);
			if (sendObject.getCode().equals(ConstanteService._CODE_SERVICE_SUCCESS))
				return (ContractorPaymentStatut) sendObject.getPayload();
			return null;
		} catch (Exception e) {
			logger.error("Error ContractorPaymentStatutService in method findById :: " + e.toString());
			return null;
		}
	}

	@Override
	public ContractorPaymentStatut saveOrUpdate(ContractorPaymentStatut entity) {
		try {
			return contractorPaymentStatutRepository.save(entity);
		} catch (Exception e) {
			logger.error("Error ContractorPaymentStatutService in method saveOrUpdate :: " + e.toString());
			return null;
		}
	}

	@Override
	public Boolean deleteById(Long id) {
		try {
			if (id == null)
				return false;
			contractorPaymentStatutRepository.delete(this.findById(id));
			return true;
		} catch (Exception e) {
			logger.error("Error ContractorPaymentStatutService in method deleteById :: " + e.toString());
			return false;
		}
	}

	@Override
	public SendObject findContractorPaymentStatutByIdWs(Long idProject) {
		try {
			List<ContractorPaymentStatut> list = contractorPaymentStatutRepository.getListByIdProjet(idProject);
			JSONObject jo;
			JSONObject joo;
			JSONArray ja = new JSONArray();

			return utilsWs.resultWs(ConstanteWs._CODE_WS_SUCCESS, ja);
		} catch (Exception e) {
			logger.error("Error ContractorPaymentStatutService in method findContractorPaymentStatutByIdWs :: "
					+ e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	@Override
	public SendObject getListContractorPaymentStatutWs() {
		try {
			return utilsWs.resultWs(ConstanteWs._CODE_WS_SUCCESS, new JSONArray(this.getList()));
		} catch (Exception e) {
			logger.error("Error ContractorPaymentStatutService in method getListContractorPaymentStatutWs() :: "
					+ e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	@Override
	public SendObject saveOrUpdateContractorPaymentStatutWs(List<ContractorPaymentStatut> entityList, Long idProject) {
		try {
			if (!entityList.isEmpty())
				contractorPaymentStatutRepository.deleteByIdProjet(idProject, entityList.get(0).getCode());

			for (ContractorPaymentStatut entity : entityList) {
				entity.setIdProject(idProject);
				entity = this.saveOrUpdate(entity);
				if (entity == null)
					return utilsWs.resultWs(ConstanteService._CODE_SERVICE_ERROR_SAVE_OR_UPDATE, new JSONObject());
			}

			return utilsWs.resultWs(ConstanteService._CODE_SERVICE_SUCCESS, new JSONArray(entityList));
		} catch (Exception e) {
			logger.error("Error ContractorPaymentStatutService in method saveOrUpdateContractorPaymentStatutWs :: "
					+ e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	@Override
	public SendObject deleteContractorPaymentStatutByIdWs(Long id) {
		try {
			Boolean resultDelete = this.deleteById(id);
			if (resultDelete == false)
				return utilsWs.resultWs(ConstanteService._CODE_SERVICE_ERROR_DELETE_ROW, new JSONObject());
			return utilsWs.resultWs(ConstanteService._CODE_SERVICE_SUCCESS, new JSONObject());
		} catch (Exception e) {
			logger.error(
					"Error ContractorPaymentStatutService in method getListContractorPaymentStatut " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

}
