package tn.com.guru.project.administration.service;

import java.util.List;

import tn.com.guru.project.administration.model.AdmUtilisateur;
import tn.com.guru.project.tools.model.SendObject;

public interface IAdmUtilisateurService {

	public List<AdmUtilisateur> getList();

	public AdmUtilisateur findById(Long id);

	public AdmUtilisateur saveOrUpdate(AdmUtilisateur entity);

	public Boolean deleteById(Long id);

	public SendObject findAdmUtilisateurByIdWs(Long id);

	public SendObject getListAdmUtilisateurWs();

	public SendObject saveOrUpdateAdmUtilisateurWs(AdmUtilisateur entity);

	public SendObject deleteAdmUtilisateurByIdWs(Long id);
	
	public SendObject registerWs(AdmUtilisateur user) ;
	
	public SendObject updateUserWs(AdmUtilisateur user) ;

}
