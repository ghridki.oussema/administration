package tn.com.guru.project.administration.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import tn.com.guru.project.administration.model.RiskRegister;
import tn.com.guru.project.tools.model.SendObject;

public interface IRiskRegisterService {

	public List<RiskRegister> getList();

	public RiskRegister findById(Long id);

	public RiskRegister saveOrUpdate(RiskRegister entity);

	public Boolean deleteById(Long id);

	public SendObject findRiskRegisterByIdWs(Long id);

	public SendObject getListRiskRegisterWs();

	public SendObject saveOrUpdateRiskRegisterWs(List<RiskRegister> entity,Long idProject);

	public SendObject deleteRiskRegisterByIdWs(Long id);

	SendObject uploadFileWs(MultipartFile uploadedFile, Long idProject);

	SendObject saveOrUpdateListOFRiskRegisterWs(List<RiskRegister> entity);

}
