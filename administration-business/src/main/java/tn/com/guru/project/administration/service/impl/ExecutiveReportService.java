package tn.com.guru.project.administration.service.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import tn.com.guru.project.administration.model.ExecutiveReport;
import tn.com.guru.project.administration.model.MultipartInputStreamFileResource;
import tn.com.guru.project.administration.repository.IExecutiveReportRepository;
import tn.com.guru.project.administration.service.IExecutiveReportService;
import tn.com.guru.project.common.service.ICommonService;
import tn.com.guru.project.tools.ConstanteService;
import tn.com.guru.project.tools.ConstanteWs;
import tn.com.guru.project.tools.UtilsWs;
import tn.com.guru.project.tools.model.SendObject;

@Service
public class ExecutiveReportService implements IExecutiveReportService {

	private static final Logger logger = LogManager.getLogger(ExecutiveReportService.class);

	@Autowired
	private IExecutiveReportRepository executiveReportRepository;

	@Autowired
	private ICommonService commonService;

	@Autowired
	private UtilsWs utilsWs;

	@Autowired
	private RestTemplate restTemplate;

	@Override
	public List<ExecutiveReport> getList() {
		// TODO Auto-generated method stub
		return executiveReportRepository.findAll();
	}

	@Override
	public ExecutiveReport findById(Long id) {
		try {
			SendObject sendObject = commonService.getObjectById(new ExecutiveReport(), id.toString(), false);
			if (sendObject.getCode().equals(ConstanteService._CODE_SERVICE_SUCCESS))
				return (ExecutiveReport) sendObject.getPayload();
			return null;
		} catch (Exception e) {
			logger.error("Error ExecutiveReportService in method findById :: " + e.toString());
			return null;
		}
	}

	@Override
	public ExecutiveReport saveOrUpdate(ExecutiveReport entity) {
		try {

			return executiveReportRepository.save(entity);
		} catch (Exception e) {
			logger.error("Error ExecutiveReportService in method saveOrUpdate :: " + e.toString());
			return null;
		}
	}

	@Override
	public Boolean deleteById(Long id) {
		try {
			if (id == null)
				return false;
			executiveReportRepository.delete(this.findById(id));
			return true;
		} catch (Exception e) {
			logger.error("Error ExecutiveReportService in method deleteById :: " + e.toString());
			return false;
		}
	}

	@Override
	public SendObject findExecutiveReportByIdWs(Long id) {
		try {
			if (id == null)
				return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_ALIAS_PARAM, new JSONObject());

			ExecutiveReport entity = this.findById(id);
			if (entity == null)
				return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_NOT_EXISTS_ROW_DATA_BASE, new JSONObject());

			if (entity.getNodeRef() != null) {
				String mediaData = restTemplate.getForObject("lb://ged-service/intern/document/media?nodeRef={nodeRef}",
						String.class, entity.getNodeRef());
				JSONArray jsonArray = new JSONArray(mediaData);
				entity.setDataMedia(jsonArray);
			}

			return utilsWs.resultWs(ConstanteWs._CODE_WS_SUCCESS, new JSONObject(entity));
		} catch (Exception e) {
			logger.error("Error ExecutiveReportService in method findExecutiveReportByIdWs :: " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	@Override
	public SendObject getListExecutiveReportWs() {
		try {
			return utilsWs.resultWs(ConstanteWs._CODE_WS_SUCCESS, new JSONArray(this.getList()));
		} catch (Exception e) {
			logger.error("Error ExecutiveReportService in method getListExecutiveReportWs() :: " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	@Override
	public SendObject saveOrUpdateExecutiveReportWs(ExecutiveReport entity, List<MultipartFile> filedata) {
		try {

			if (filedata != null) {

				LinkedMultiValueMap<String, Object> map = new LinkedMultiValueMap<>();

				for (MultipartFile file : filedata) {
					if (!file.isEmpty()) {
						map.add("filedata", new MultipartInputStreamFileResource(file.getInputStream(),
								file.getOriginalFilename()));
					}
				}
				HttpHeaders headers = new HttpHeaders();
				headers.setContentType(MediaType.MULTIPART_FORM_DATA);
				String url = "http://ged-service/intern/document/saveMedia";

				HttpEntity<LinkedMultiValueMap<String, Object>> requestEntity = new HttpEntity<>(map, headers);
				String response = restTemplate.postForObject(url, requestEntity, String.class);
				JSONObject j = new JSONObject(response);
				if (j.get("code").toString().equals("200"))
					entity.setNodeRef(j.get("payload").toString());
			}

			entity = this.saveOrUpdate(entity);
			if (entity == null)
				return utilsWs.resultWs(ConstanteService._CODE_SERVICE_ERROR_SAVE_OR_UPDATE, new JSONObject());
			return utilsWs.resultWs(ConstanteService._CODE_SERVICE_SUCCESS, new JSONObject(entity));
		} catch (Exception e) {
			logger.error("Error ContactService in method saveOrUpdateContactWs :: " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	@Override
	public SendObject deleteExecutiveReportByIdWs(Long id) {
		try {
			Boolean resultDelete = this.deleteById(id);
			if (resultDelete == false)
				return utilsWs.resultWs(ConstanteService._CODE_SERVICE_ERROR_DELETE_ROW, new JSONObject());
			return utilsWs.resultWs(ConstanteService._CODE_SERVICE_SUCCESS, new JSONObject());
		} catch (Exception e) {
			logger.error("Error ExecutiveReportService in method getListExecutiveReport " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	@Override
	public SendObject getListExecutiveReporListtWs(Long id) {
		try {
			if (id == null)
				return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_ALIAS_PARAM, new JSONObject());

			List<ExecutiveReport> listExecutiveReport = executiveReportRepository.getExecutiveReportByIdProject(id);
			
			for (ExecutiveReport entity : listExecutiveReport) {

				if (entity.getNodeRef() != null) {
					String mediaData = restTemplate.getForObject(
							"lb://ged-service/intern/document/media?nodeRef={nodeRef}", String.class,
							entity.getNodeRef());
					JSONArray jsonArray = new JSONArray(mediaData);
					entity.setDataMedia(jsonArray);
				}
			}
			return utilsWs.resultWs(ConstanteWs._CODE_WS_SUCCESS, new JSONArray(listExecutiveReport));
		} catch (Exception e) {
			logger.error("Error ExecutiveReportService in method getListExecutiveReporListtWs :: " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}
}
