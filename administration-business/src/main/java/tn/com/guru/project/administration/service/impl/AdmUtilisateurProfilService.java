package tn.com.guru.project.administration.service.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tn.com.guru.project.administration.model.AdmUtilisateurProfil;
import tn.com.guru.project.administration.repository.IAdmUtilisateurProfilRepository;
import tn.com.guru.project.administration.service.IAdmUtilisateurProfilService;
import tn.com.guru.project.common.service.ICommonService;
import tn.com.guru.project.tools.ConstanteService;
import tn.com.guru.project.tools.ConstanteWs;
import tn.com.guru.project.tools.UtilsWs;
import tn.com.guru.project.tools.model.SendObject;

@Service
public class AdmUtilisateurProfilService implements IAdmUtilisateurProfilService {

	private static final Logger logger = LogManager.getLogger(AdmUtilisateurProfilService.class);

	@Autowired
	private IAdmUtilisateurProfilRepository admUtilisateurProfilRepository;

	@Autowired
	private ICommonService commonService;

	@Autowired
	private UtilsWs utilsWs;

	@Override
	public List<AdmUtilisateurProfil> getList() {
		try {
			return admUtilisateurProfilRepository.findAll();
		} catch (Exception e) {
			logger.error("Error AdmUtilisateurProfilService in method getList :: " + e.toString());
			return null;
		}
	}

	@Override
	public AdmUtilisateurProfil findById(Long id) {
		try {
			SendObject sendObject = commonService.getObjectById(new AdmUtilisateurProfil(), id.toString(), false);
			if (sendObject.getCode().equals(ConstanteService._CODE_SERVICE_SUCCESS))
				return (AdmUtilisateurProfil) sendObject.getPayload();
			return null;
		} catch (Exception e) {
			logger.error("Error AdmUtilisateurProfilService in method findById :: " + e.toString());
			return null;
		}
	}

	@Override
	public AdmUtilisateurProfil saveOrUpdate(AdmUtilisateurProfil entity) {
		try {
			return admUtilisateurProfilRepository.save(entity);
		} catch (Exception e) {
			logger.error("Error AdmUtilisateurProfilService in method saveOrUpdate :: " + e.toString());
			return null;
		}
	}

	@Override
	public Boolean deleteById(Long id) {
		try {
			if (id == null)
				return false;
			admUtilisateurProfilRepository.delete(this.findById(id));
			return true;
		} catch (Exception e) {
			logger.error("Error AdmUtilisateurProfilService in method deleteById :: " + e.toString());
			return false;
		}
	}

	@Override
	public SendObject findAdmUtilisateurProfilByIdWs(Long id) {
		try {
			if (id == null)
				return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_ALIAS_PARAM, new JSONObject());
			AdmUtilisateurProfil entity = this.findById(id);
			if (entity == null)
				return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_NOT_EXISTS_ROW_DATA_BASE, new JSONObject());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_SUCCESS, new JSONObject(entity));
		} catch (Exception e) {
			logger.error(
					"Error AdmUtilisateurProfilService in method findAdmUtilisateurProfilByIdWs :: " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	@Override
	public SendObject getListAdmUtilisateurProfilWs() {
		try {
			return utilsWs.resultWs(ConstanteWs._CODE_WS_SUCCESS, new JSONArray(this.getList()));
		} catch (Exception e) {
			logger.error(
					"Error AdmUtilisateurProfilService in method getListAdmUtilisateurProfilWs() :: " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	@Override
	public SendObject saveOrUpdateAdmUtilisateurProfilWs(AdmUtilisateurProfil entity) {
		try {
			entity = this.saveOrUpdate(entity);
			if (entity == null)
				return utilsWs.resultWs(ConstanteService._CODE_SERVICE_ERROR_SAVE_OR_UPDATE, new JSONObject());
			return utilsWs.resultWs(ConstanteService._CODE_SERVICE_SUCCESS, new JSONObject(entity));
		} catch (Exception e) {
			logger.error("Error AdmUtilisateurProfilService in method saveOrUpdateAdmUtilisateurProfilWs :: "
					+ e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	@Override
	public SendObject deleteAdmUtilisateurProfilByIdWs(Long id) {
		try {
			Boolean resultDelete = this.deleteById(id);
			if (resultDelete == false)
				return utilsWs.resultWs(ConstanteService._CODE_SERVICE_ERROR_DELETE_ROW, new JSONObject());
			return utilsWs.resultWs(ConstanteService._CODE_SERVICE_SUCCESS, new JSONObject());
		} catch (Exception e) {
			logger.error("Error AdmUtilisateurProfilService in method getListAdmUtilisateurProfil " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	@Override
	public List<AdmUtilisateurProfil> getListUserProfilesByIdUser(Long iduser) {
		try {
			return admUtilisateurProfilRepository.getListUserProfilesByIdUser(iduser);
		} catch (Exception e) {
			logger.error("Error AdmUtilisateurProfilService in method getListUserProfilesByIdUser :: " + e.toString());
			return null;
		}
	}


}
