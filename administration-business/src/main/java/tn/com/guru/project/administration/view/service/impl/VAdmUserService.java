package tn.com.guru.project.administration.view.service.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tn.com.guru.project.administration.model.AdmProfil;
import tn.com.guru.project.administration.model.AdmUtilisateurProfil;
import tn.com.guru.project.administration.service.IAdmProfilService;
import tn.com.guru.project.administration.service.IAdmUtilisateurProfilService;
import tn.com.guru.project.administration.view.repository.IVAdmUserRepository;
import tn.com.guru.project.administration.view.service.IVAdmUserService;
import tn.com.guru.project.tools.ConstanteWs;
import tn.com.guru.project.tools.UtilsWs;
import tn.com.guru.project.tools.model.SendObject;
import tn.com.guru.project.view.model.VAdmUser;

@Service
public class VAdmUserService implements IVAdmUserService {

	private static final Logger logger = LogManager.getLogger(VAdmUserService.class);

	@Autowired
	private IVAdmUserRepository vAdmUserRepository;

	@Autowired
	private UtilsWs utilsWs;

	@Autowired
	private IAdmUtilisateurProfilService admUserProfilService;

	@Autowired
	private IAdmProfilService admProfilService;

	@Override
	public SendObject getVAdmUserByIdWs(Long id) {
		try {
			if (id == null)
				return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_ALIAS_PARAM, new JSONObject());

			List<AdmUtilisateurProfil> listUserProfil = admUserProfilService.getListUserProfilesByIdUser(id);
			List<AdmProfil> listProfile = admProfilService.getList();

			for (AdmUtilisateurProfil admUserProfil : listUserProfil) {
				for (AdmProfil admProfil : listProfile) {
					if (admProfil.getId().equals(admUserProfil.getIdAdmProfil())) {
						admProfil.setIsChecked(true);
						break;
					}

				}
			}
			VAdmUser admUser = this.getVAdmUserById(id);
			if (admUser.getIdAdmUser() == null)
				return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_ALIAS_PARAM, new JSONObject());

			JSONObject js = new JSONObject();
			js.put("admUser", new JSONObject(admUser));
			js.put("listProfile", listProfile);

			return utilsWs.resultWs(ConstanteWs._CODE_WS_SUCCESS, js);
		} catch (Exception e) {
			logger.error("Error VAdmUserService in method getVAdmUserByIdWs :: " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	@Override
	public VAdmUser getVAdmUserById(Long id) {
		try {
			if (id == null)
				return new VAdmUser();
			VAdmUser user = vAdmUserRepository.findVAdmUserById(id);
			return user == null ? new VAdmUser() : user;
		} catch (Exception e) {
			logger.error("Error VAdmUserService in method getVAdmUserById :: " + e.toString());
			return new VAdmUser();
		}
	}

	@Override
	public SendObject getVAdmUserByLoginWs(String login) {
		try {
			if (login == null)
				return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_ALIAS_PARAM, new JSONObject());
			VAdmUser admUser = this.getVAdmUserByLogin(login);
			if (admUser.getIdAdmUser() == null)
				return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_ALIAS_PARAM, new JSONObject());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_SUCCESS, admUser);
		} catch (Exception e) {
			logger.error("Error VAdmUserService in method getVAdmUserByLoginWs :: " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	@Override
	public VAdmUser getVAdmUserByLogin(String login) {
		try {
			if (login == null)
				return new VAdmUser();
			VAdmUser user = vAdmUserRepository.findVAdmUserByLogin(login);
			return user == null ? new VAdmUser() : user;
		} catch (Exception e) {
			logger.error("Error VAdmUserService in method getVAdmUserByLogin :: " + e.toString());
			return new VAdmUser();
		}
	}

	public List<VAdmUser> getList() {
		try {
			return vAdmUserRepository.findAll();
		} catch (Exception e) {
			logger.error("Error AdmUtilisateurService in method getList :: " + e.toString());
			return null;
		}
	}

	@Override
	public SendObject getListAdmUtilisateurWs() {
		try {
			return utilsWs.resultWs(ConstanteWs._CODE_WS_SUCCESS, new JSONArray(this.getList()));
		} catch (Exception e) {
			logger.error("Error AdmUtilisateurService in method getListAdmUtilisateurWs() :: " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

}
