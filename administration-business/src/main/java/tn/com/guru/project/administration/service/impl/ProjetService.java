package tn.com.guru.project.administration.service.impl;

import java.io.InputStreamReader;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.supercsv.io.CsvListReader;
import org.supercsv.io.ICsvListReader;
import org.supercsv.prefs.CsvPreference;
import org.supercsv.util.Util;

import tn.com.guru.project.administration.model.MultipartInputStreamFileResource;
import tn.com.guru.project.administration.model.Project;
import tn.com.guru.project.administration.model.ProjectViewer;
import tn.com.guru.project.administration.repository.IProjectViewerRepository;
import tn.com.guru.project.administration.repository.IProjetRepository;
import tn.com.guru.project.administration.service.IProjetService;
import tn.com.guru.project.common.service.ICommonService;
import tn.com.guru.project.tools.ConstanteService;
import tn.com.guru.project.tools.ConstanteWs;
import tn.com.guru.project.tools.UtilsWs;
import tn.com.guru.project.tools.model.SendObject;

@Service
public class ProjetService implements IProjetService {

	private static final Logger logger = LogManager.getLogger(ProjetService.class);

	@Autowired
	private IProjetRepository projetRepository;

	@Autowired
	private ICommonService commonService;

	@Autowired
	private UtilsWs utilsWs;

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private IProjectViewerRepository projectViewerRepository;

	@Override
	public List<Project> getList() {
		try {
			return projetRepository.findAll();
		} catch (Exception e) {
			logger.error("Error ProjectService in method getList :: " + e.toString());
			return null;
		}
	}

	@Override
	public Project findById(Long id) {
		try {
			SendObject sendObject = commonService.getObjectById(new Project(), id.toString(), false);
			if (sendObject.getCode().equals(ConstanteService._CODE_SERVICE_SUCCESS))
				return (Project) sendObject.getPayload();
			return null;
		} catch (Exception e) {
			logger.error("Error ProjectService in method findById :: " + e.toString());
			return null;
		}
	}

	@Override
	public Project saveOrUpdate(Project entity) {
		try {
			entity.setCreatedAt(new Timestamp(System.currentTimeMillis()));
			entity.setUpdatedAt(new Timestamp(System.currentTimeMillis()));
			return projetRepository.save(entity);
		} catch (Exception e) {
			logger.error("Error ProjectService in method saveOrUpdate :: " + e.toString());
			return null;
		}
	}

	@Override
	public Boolean deleteById(Long id) {
		try {
			if (id == null)
				return false;
			projetRepository.delete(this.findById(id));
			return true;
		} catch (Exception e) {
			logger.error("Error ProjectService in method deleteById :: " + e.toString());
			return false;
		}
	}

	@Override
	public SendObject findProjectByIdWs(Long id) {
		try {
			if (id == null)
				return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_ALIAS_PARAM, new JSONObject());
			Project entity = this.findById(id);
			if (entity == null)
				return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_NOT_EXISTS_ROW_DATA_BASE, new JSONObject());

			if (entity.getNodeRef() != null) {
				String mediaData = restTemplate.getForObject("lb://ged-service/intern/document/media?nodeRef={nodeRef}",
						String.class, entity.getNodeRef());
				JSONArray jsonArray = new JSONArray(mediaData);

				entity.setDataMedia(jsonArray);
			}
			List<ProjectViewer> listProjectViewer = projectViewerRepository.getListByIdProject(entity.getId());
			List<String> urn = new ArrayList<>();
			if (listProjectViewer != null)
				for (ProjectViewer projectViewer : listProjectViewer) {
					urn.add(projectViewer.getUrn());
				}
			entity.setUrn(urn);
			return utilsWs.resultWs(ConstanteWs._CODE_WS_SUCCESS, new JSONObject(entity));
		} catch (Exception e) {
			logger.error("Error ProjectService in method findProjectByIdWs :: " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	@Override
	public SendObject getListProjectWs() {
		try {
			return utilsWs.resultWs(ConstanteWs._CODE_WS_SUCCESS, new JSONArray(this.getList()));
		} catch (Exception e) {
			logger.error("Error ProjectService in method getListProjectWs() :: " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	@Override
	public SendObject saveOrUpdateProjectWs(Project entity, List<MultipartFile> filedata) {
		try {
			if (filedata != null) {
				LinkedMultiValueMap<String, Object> map = new LinkedMultiValueMap<>();

				for (MultipartFile file : filedata) {
					if (!file.isEmpty()) {
						map.add("filedata", new MultipartInputStreamFileResource(file.getInputStream(),
								file.getOriginalFilename()));
					}
				}
				HttpHeaders headers = new HttpHeaders();
				headers.setContentType(MediaType.MULTIPART_FORM_DATA);
				String url = "http://ged-service/intern/document/saveMedia";

				HttpEntity<LinkedMultiValueMap<String, Object>> requestEntity = new HttpEntity<>(map, headers);
				String response = restTemplate.postForObject(url, requestEntity, String.class);
				JSONObject j = new JSONObject(response);
				if (j.get("code").toString().equals("200"))
					entity.setNodeRef(j.get("payload").toString());
			}
			if (this.saveOrUpdate(entity) == null)
				return utilsWs.resultWs(ConstanteService._CODE_SERVICE_ERROR_SAVE_OR_UPDATE, new JSONObject());

			ProjectViewer pv;
			projectViewerRepository.deleteAllByIdPojet(entity.getId());
			if (entity.getUrn() != null)
				for (String urn : entity.getUrn()) {
					pv = new ProjectViewer();
					pv.setIdProject(entity.getId());
					pv.setUrn(urn);
					projectViewerRepository.save(pv);
				}
			return utilsWs.resultWs(ConstanteService._CODE_SERVICE_SUCCESS, new JSONObject(entity));
		} catch (Exception e) {
			logger.error("Error ProjectService in method saveOrUpdateProjectWs :: " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	@Override
	public SendObject deleteProjectByIdWs(Long id) {
		try {
			Boolean resultDelete = this.deleteById(id);
			if (resultDelete == false)
				return utilsWs.resultWs(ConstanteService._CODE_SERVICE_ERROR_DELETE_ROW, new JSONObject());
			return utilsWs.resultWs(ConstanteService._CODE_SERVICE_SUCCESS, new JSONObject());
		} catch (Exception e) {
			logger.error("Error ProjectService in method getListProject " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	@Override
	public SendObject uploadFileWs(MultipartFile uploadedFile, Long idProject) {
		try {

			InputStreamReader inReader = new InputStreamReader(uploadedFile.getInputStream(), "ISO-8859-1");
			try (ICsvListReader listReader = new CsvListReader(inReader, CsvPreference.STANDARD_PREFERENCE)) {
				String[] headers = listReader.getHeader(true);
				String[] column = new String[headers.length];

				for (int i = 0; i < headers.length; i++) {
					String st = headers[i];

					st = st.replaceAll("\\s+", "");
					st = st.replaceAll("%", "pourcentage");
					if (st.contains("."))
						st = st.replace(".", "");
					st = st.toLowerCase();
					column[i] = st;
				}
				List<String> row = null;
				JSONArray ja = new JSONArray();
				while ((row = listReader.read()) != null) {

					if (listReader.length() != headers.length) {
						return utilsWs.resultWs("skipping invalid row: ", row);

					}
					Map<String, String> rowMap = new HashMap<String, String>();
					Util.filterListToMap(rowMap, column, row);
					ja.put(rowMap);
				}
				listReader.close();

				return utilsWs.resultWs(ConstanteService._CODE_SERVICE_SUCCESS, ja);
			}
		} catch (Exception e) {
			logger.error("Error ProjectService in method uploadFileWs " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}
}
