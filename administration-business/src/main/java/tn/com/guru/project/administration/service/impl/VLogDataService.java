package tn.com.guru.project.administration.service.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tn.com.guru.project.administration.repository.IVLogDataRepository;
import tn.com.guru.project.administration.service.IVLogDataService;
import tn.com.guru.project.common.service.ICommonService;
import tn.com.guru.project.tools.ConstanteService;
import tn.com.guru.project.tools.ConstanteWs;
import tn.com.guru.project.tools.UtilsWs;
import tn.com.guru.project.tools.model.SendObject;
import tn.com.guru.project.view.model.VLogData;

@Service
public class VLogDataService implements IVLogDataService {

	private static final Logger logger = LogManager.getLogger(VLogDataService.class);

	@Autowired
	private IVLogDataRepository vLogDataRepository;

	@Autowired
	private ICommonService commonService;

	@Autowired
	private UtilsWs utilsWs;

	@Override
	public List<VLogData> getList() {
		try {
			return vLogDataRepository.findAll();
		} catch (Exception e) {
			logger.error("Error VLogDataService in method getList :: " + e.toString());
			return null;
		}
	}

	@Override
	public VLogData findById(Long id) {
		try {

			return vLogDataRepository.findById(id) != null ? vLogDataRepository.findById(id).get() : null;
		} catch (Exception e) {
			logger.error("Error VLogDataService in method findById :: " + e.toString());
			return null;
		}
	}

	@Override
	public VLogData saveOrUpdate(VLogData entity) {
		try {
			return vLogDataRepository.save(entity);
		} catch (Exception e) {
			logger.error("Error VLogDataService in method saveOrUpdate :: " + e.toString());
			return null;
		}
	}

	@Override
	public Boolean deleteById(Long id) {
		try {
			if (id == null)
				return false;
			vLogDataRepository.delete(this.findById(id));
			return true;
		} catch (Exception e) {
			logger.error("Error VLogDataService in method deleteById :: " + e.toString());
			return false;
		}
	}

	@Override
	public SendObject findVLogDataByIdWs(Long id) {
		try {
			if (id == null)
				return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_ALIAS_PARAM, new JSONObject());
			VLogData entity = this.findById(id);
			if (entity == null)
				return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_NOT_EXISTS_ROW_DATA_BASE, new JSONObject());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_SUCCESS, new JSONObject(entity));
		} catch (Exception e) {
			logger.error("Error VLogDataService in method findVLogDataByIdWs :: " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	@Override
	public SendObject getListVLogDataWs() {
		try {
			return utilsWs.resultWs(ConstanteWs._CODE_WS_SUCCESS, new JSONArray(this.getList()));
		} catch (Exception e) {
			logger.error("Error VLogDataService in method getListVLogDataWs() :: " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	@Override
	public SendObject saveOrUpdateVLogDataWs(VLogData entity) {
		try {
			entity = this.saveOrUpdate(entity);
			if (entity == null)
				return new SendObject(ConstanteService._CODE_SERVICE_ERROR_SAVE_OR_UPDATE);
			return new SendObject(ConstanteService._CODE_SERVICE_SUCCESS, entity);
		} catch (Exception e) {
			logger.error("Error VLogDataService in method saveOrUpdateVLogDataWs :: " + e.toString());
			return new SendObject(ConstanteService._CODE_SERVICE_ERROR);
		}
	}

	@Override
	public SendObject deleteVLogDataByIdWs(Long id) {
		try {
			Boolean resultDelete = this.deleteById(id);
			if (resultDelete == false)
				return new SendObject(ConstanteService._CODE_SERVICE_ERROR_DELETE_ROW);
			return new SendObject(ConstanteService._CODE_SERVICE_SUCCESS);
		} catch (Exception e) {
			logger.error("Error VLogDataService in method getListVLogData " + e.toString());
			return new SendObject(ConstanteService._CODE_SERVICE_ERROR);
		}
	}

}
