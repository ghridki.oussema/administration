package tn.com.guru.project.administration.view.service;

import tn.com.guru.project.tools.model.SendObject;
import tn.com.guru.project.view.model.VAdmUser;

public interface IVAdmUserService {

	public SendObject getVAdmUserByIdWs(Long id);
	
	public SendObject getVAdmUserByLoginWs(String login);
	
	public VAdmUser getVAdmUserById(Long id);
	
	public VAdmUser getVAdmUserByLogin(String login);
	
	public SendObject getListAdmUtilisateurWs() ;
		
}
