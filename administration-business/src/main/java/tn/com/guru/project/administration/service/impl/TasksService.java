package tn.com.guru.project.administration.service.impl;

import java.sql.Timestamp;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import tn.com.guru.project.administration.model.Contact;
import tn.com.guru.project.administration.model.MultipartInputStreamFileResource;
import tn.com.guru.project.administration.repository.IContactRepository;
import tn.com.guru.project.administration.service.IContactService;
import tn.com.guru.project.common.service.ICommonService;
import tn.com.guru.project.tools.ConstanteService;
import tn.com.guru.project.tools.ConstanteWs;
import tn.com.guru.project.tools.UtilsWs;
import tn.com.guru.project.tools.model.SendObject;

@Service
public class TasksService implements IContactService {

	private static final Logger logger = LogManager.getLogger(TasksService.class);

	@Autowired
	private IContactRepository tasksRepository;

	@Autowired
	private ICommonService commonService;

	@Autowired
	private UtilsWs utilsWs;

	@Autowired
	private RestTemplate restTemplate;

	@Override
	public List<Contact> getList() {
		try {
			return tasksRepository.findAll();
		} catch (Exception e) {
			logger.error("Error TasksService in method getList :: " + e.toString());
			return null;
		}
	}

	@Override
	public Contact findById(Long id) {
		try {
			SendObject sendObject = commonService.getObjectById(new Contact(), id.toString(), false);
			if (sendObject.getCode().equals(ConstanteService._CODE_SERVICE_SUCCESS))
				return (Contact) sendObject.getPayload();
			return null;
		} catch (Exception e) {
			logger.error("Error TasksService in method findById :: " + e.toString());
			return null;
		}
	}

	@Override
	public Contact saveOrUpdate(Contact entity) {
		try {
			entity.setDtMaj(new Timestamp(System.currentTimeMillis()));
			entity.setNodeRef("aa");
			return tasksRepository.save(entity);
		} catch (Exception e) {
			logger.error("Error TasksService in method saveOrUpdate :: " + e.toString());
			return null;
		}
	}

	@Override
	public Boolean deleteById(Long id) {
		try {
			if (id == null)
				return false;
			tasksRepository.delete(this.findById(id));
			return true;
		} catch (Exception e) {
			logger.error("Error TasksService in method deleteById :: " + e.toString());
			return false;
		}
	}

	@Override
	public SendObject findContactByIdWs(Long id) {
		try {
			if (id == null)
				return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_ALIAS_PARAM, new JSONObject());
			Contact entity = this.findById(id);
			if (entity == null)
				return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_NOT_EXISTS_ROW_DATA_BASE, new JSONObject());

			if (entity.getNodeRef() != null) {
				String mediaData = restTemplate.getForObject("lb://ged-service/intern/document/media?nodeRef={nodeRef}",
						String.class, entity.getNodeRef());
				JSONArray jsonArray = new JSONArray(mediaData);
				entity.setDataMedia(jsonArray);
			}

			return utilsWs.resultWs(ConstanteWs._CODE_WS_SUCCESS, new JSONObject(entity));
		} catch (Exception e) {
			logger.error("Error ContactService in method findContactByIdWs :: " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	@Override
	public SendObject getListContactWs() {
		try {
			return utilsWs.resultWs(ConstanteWs._CODE_WS_SUCCESS, new JSONArray(this.getList()));
		} catch (Exception e) {
			logger.error("Error ContactService in method getListContactWs() :: " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	@Override
	public SendObject saveOrUpdateContactWs(Contact entity, List<MultipartFile> filedata) {
		try {

			if (filedata != null) {
				LinkedMultiValueMap<String, Object> map = new LinkedMultiValueMap<>();

				for (MultipartFile file : filedata) {
					if (!file.isEmpty()) {
						map.add("filedata", new MultipartInputStreamFileResource(file.getInputStream(),
								file.getOriginalFilename()));
					}
				}
				HttpHeaders headers = new HttpHeaders();
				headers.setContentType(MediaType.MULTIPART_FORM_DATA);
				String url = "http://ged-service/intern/document/saveMedia";

				HttpEntity<LinkedMultiValueMap<String, Object>> requestEntity = new HttpEntity<>(map, headers);
				String response = restTemplate.postForObject(url, requestEntity, String.class);
				JSONObject j = new JSONObject(response);
				if (j.get("code").toString().equals("200"))
					entity.setNodeRef(j.get("payload").toString());
			}
			entity = this.saveOrUpdate(entity);
			if (entity == null)
				return utilsWs.resultWs(ConstanteService._CODE_SERVICE_ERROR_SAVE_OR_UPDATE, new JSONObject());
			return utilsWs.resultWs(ConstanteService._CODE_SERVICE_SUCCESS, new JSONObject(entity));
		} catch (Exception e) {
			logger.error("Error ContactService in method saveOrUpdateContactWs :: " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	@Override
	public SendObject deleteContactByIdWs(Long id) {
		try {
			Boolean resultDelete = this.deleteById(id);
			if (resultDelete == false)
				return utilsWs.resultWs(ConstanteService._CODE_SERVICE_ERROR_DELETE_ROW, new JSONObject());
			return utilsWs.resultWs(ConstanteService._CODE_SERVICE_SUCCESS, new JSONObject());
		} catch (Exception e) {
			logger.error("Error ContactService in method getListContact " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	@Override
	public SendObject getListContactByIdProjectWs(Long idProject) {
		try {
			if (idProject == null)
				return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_ALIAS_PARAM, new JSONObject());
			List<Contact> contacts = tasksRepository.getListContacts(idProject);
				for (Contact entity : contacts) {
					if (entity.getNodeRef() != null) {
						String mediaData = restTemplate.getForObject(
								"lb://ged-service/intern/document/media?nodeRef={nodeRef}", String.class,
								entity.getNodeRef());
						JSONArray jsonArray = new JSONArray(mediaData);
						entity.setDataMedia(jsonArray);
					}
				}

			return utilsWs.resultWs(ConstanteWs._CODE_WS_SUCCESS, new JSONArray(contacts));
		} catch (Exception e) {
			logger.error("Error ContactService in method getListContactByIdProjectWs() :: " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

}
