package tn.com.guru.project.administration.service;

import java.util.List;

import tn.com.guru.project.administration.model.AdmPersonnel;
import tn.com.guru.project.tools.model.SendObject;

public interface IAdmPersonnelService {

	public List<AdmPersonnel> getList();

	public AdmPersonnel findById(Long id);

	public AdmPersonnel saveOrUpdate(AdmPersonnel entity);

	public Boolean deleteById(Long id);

	public SendObject findAdmPersonnelByIdWs(Long id);

	public SendObject getListAdmPersonnelWs();

	public SendObject saveOrUpdateAdmPersonnelWs(AdmPersonnel entity);

	public SendObject deleteAdmPersonnelByIdWs(Long id);

}
