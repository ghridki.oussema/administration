package tn.com.guru.project.administration.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import tn.com.guru.project.administration.model.Contact;
import tn.com.guru.project.tools.model.SendObject;

public interface IContactService {

	public List<Contact> getList();

	public Contact findById(Long id);

	public Contact saveOrUpdate(Contact entity);

	public Boolean deleteById(Long id);

	public SendObject findContactByIdWs(Long id);

	public SendObject getListContactWs();

	public SendObject saveOrUpdateContactWs(Contact entity,List<MultipartFile> filedata);

	public SendObject deleteContactByIdWs(Long id);
	
	public SendObject getListContactByIdProjectWs(Long idProject);

}
