package tn.com.guru.project.administration.service;

import java.util.List;

import tn.com.guru.project.administration.model.ContractorPaymentStatut;
import tn.com.guru.project.tools.model.SendObject;

public interface IContractorPaymentStatutService {

	public List<ContractorPaymentStatut> getList();

	public ContractorPaymentStatut findById(Long id);

	public ContractorPaymentStatut saveOrUpdate(ContractorPaymentStatut entity);

	public Boolean deleteById(Long id);

	public SendObject findContractorPaymentStatutByIdWs(Long id);

	public SendObject getListContractorPaymentStatutWs();

	public SendObject saveOrUpdateContractorPaymentStatutWs(List<ContractorPaymentStatut> entity, Long idProject);

	public SendObject deleteContractorPaymentStatutByIdWs(Long id);

}
