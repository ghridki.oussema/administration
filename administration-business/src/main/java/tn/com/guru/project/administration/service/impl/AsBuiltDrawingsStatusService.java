package tn.com.guru.project.administration.service.impl;

import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tn.com.guru.project.administration.model.AsBuiltDrawingsStatus;
import tn.com.guru.project.administration.repository.IAsBuiltDrawingsStatusRepository;
import tn.com.guru.project.administration.service.IAsBuiltDrawingsStatusService;
import tn.com.guru.project.common.service.ICommonService;
import tn.com.guru.project.tools.ConstanteService;
import tn.com.guru.project.tools.ConstanteWs;
import tn.com.guru.project.tools.UtilsWs;
import tn.com.guru.project.tools.model.SendObject;

@Service
public class AsBuiltDrawingsStatusService implements IAsBuiltDrawingsStatusService {

	private static final Logger logger = LogManager.getLogger(AsBuiltDrawingsStatusService.class);

	@Autowired
	private IAsBuiltDrawingsStatusRepository asBuiltDrawingsStatusRepository;

	@Autowired
	private ICommonService commonService;

	@Autowired
	private UtilsWs utilsWs;

	@Override
	public List<AsBuiltDrawingsStatus> getList() {
		try {
			return asBuiltDrawingsStatusRepository.findAll();
		} catch (Exception e) {
			logger.error("Error AsBuiltDrawingsStatusService in method getList :: " + e.toString());
			return null;
		}
	}

	@Override
	public AsBuiltDrawingsStatus findById(Long id) {
		try {
			SendObject sendObject = commonService.getObjectById(new AsBuiltDrawingsStatus(), id.toString(), false);
			if (sendObject.getCode().equals(ConstanteService._CODE_SERVICE_SUCCESS))
				return (AsBuiltDrawingsStatus) sendObject.getPayload();
			return null;
		} catch (Exception e) {
			logger.error("Error AsBuiltDrawingsStatusService in method findById :: " + e.toString());
			return null;
		}
	}

	@Override
	public AsBuiltDrawingsStatus saveOrUpdate(AsBuiltDrawingsStatus entity) {
		try {
			return asBuiltDrawingsStatusRepository.save(entity);
		} catch (Exception e) {
			logger.error("Error AsBuiltDrawingsStatusService in method saveOrUpdate :: " + e.toString());
			return null;
		}
	}

	@Override
	public Boolean deleteById(Long id) {
		try {
			if (id == null)
				return false;
			asBuiltDrawingsStatusRepository.delete(this.findById(id));
			return true;
		} catch (Exception e) {
			logger.error("Error AsBuiltDrawingsStatusService in method deleteById :: " + e.toString());
			return false;
		}
	}

	@Override
	public SendObject findAsBuiltDrawingsStatusByIdWs(Long id) {
		try {
			if (id == null)
				return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_ALIAS_PARAM, new JSONObject());
			AsBuiltDrawingsStatus entity = this.findById(id);
			if (entity == null)
				return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_NOT_EXISTS_ROW_DATA_BASE, new JSONObject());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_SUCCESS, new JSONObject(entity));
		} catch (Exception e) {
			logger.error(
					"Error AsBuiltDrawingsStatusService in method findAsBuiltDrawingsStatusByIdWs :: " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	@Override
	public SendObject getListAsBuiltDrawingsStatusWs() {
		try {
			return utilsWs.resultWs(ConstanteWs._CODE_WS_SUCCESS, new JSONArray(this.getList()));
		} catch (Exception e) {
			logger.error(
					"Error AsBuiltDrawingsStatusService in method getListAsBuiltDrawingsStatusWs() :: " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	@Override
	public SendObject saveOrUpdateAsBuiltDrawingsStatusWs(List<AsBuiltDrawingsStatus> entityList, Long idProject) {
		try {

			asBuiltDrawingsStatusRepository.deleteByIdProjet(idProject);

			for (AsBuiltDrawingsStatus entity : entityList) {
				entity.setCreatedAt(new Date());
				entity.setIdProjet(idProject);
				entity = this.saveOrUpdate(entity);
				if (entity == null)
					return utilsWs.resultWs(ConstanteService._CODE_SERVICE_ERROR_SAVE_OR_UPDATE, new JSONObject());
			}
			return utilsWs.resultWs(ConstanteService._CODE_SERVICE_SUCCESS, new JSONArray(entityList));
		} catch (Exception e) {
			logger.error("Error AsBuiltDrawingsStatusService in method saveOrUpdateAsBuiltDrawingsStatusWs :: "
					+ e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	@Override
	public SendObject deleteAsBuiltDrawingsStatusByIdWs(Long id) {
		try {
			Boolean resultDelete = this.deleteById(id);
			if (resultDelete == false)
				return utilsWs.resultWs(ConstanteService._CODE_SERVICE_ERROR_DELETE_ROW, new JSONObject());
			return utilsWs.resultWs(ConstanteService._CODE_SERVICE_SUCCESS, new JSONObject());
		} catch (Exception e) {
			logger.error("Error AsBuiltDrawingsStatusService in method getListAsBuiltDrawingsStatus " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	@Override
	public SendObject getListAsBuiltDrawingsStatusByIdWs(Long idProject) {
		try {
			List<AsBuiltDrawingsStatus> list = asBuiltDrawingsStatusRepository.getListByIdProjet(idProject);
			JSONObject jo;
			JSONObject joo;
			JSONArray ja = new JSONArray();

			for (AsBuiltDrawingsStatus e : list) {
				jo = new JSONObject();
				joo = new JSONObject();
				jo.put("name", e.getSummary());
				jo.put("value", e.getTotal());
				joo.put("ar", e.getAr());
				joo.put("st", e.getSt());
				joo.put("gn", e.getGn());
				joo.put("el", e.getEl());
				joo.put("la", e.getLa());
				joo.put("me", e.getMe());
				joo.put("pourcentage", e.getPourcentage());
				jo.put("extra", joo);
				ja.put(jo);
			}

			return utilsWs.resultWs(ConstanteWs._CODE_WS_SUCCESS, ja);
		} catch (Exception e) {
			logger.error("Error AsBuiltDrawingsStatusService in method getListAsBuiltDrawingsStatusByIdWs() :: "
					+ e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

}
