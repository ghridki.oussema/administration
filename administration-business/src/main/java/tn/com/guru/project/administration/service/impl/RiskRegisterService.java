package tn.com.guru.project.administration.service.impl;

import java.io.InputStreamReader;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.supercsv.io.CsvListReader;
import org.supercsv.io.ICsvListReader;
import org.supercsv.prefs.CsvPreference;
import org.supercsv.util.Util;

import tn.com.guru.project.administration.model.RiskRegister;
import tn.com.guru.project.administration.repository.IRiskRegisterRepository;
import tn.com.guru.project.administration.service.IRiskRegisterService;
import tn.com.guru.project.common.service.ICommonService;
import tn.com.guru.project.tools.ConstanteService;
import tn.com.guru.project.tools.ConstanteWs;
import tn.com.guru.project.tools.UtilsWs;
import tn.com.guru.project.tools.model.SendObject;

@Service
public class RiskRegisterService implements IRiskRegisterService {

	private static final Logger logger = LogManager.getLogger(RiskRegisterService.class);

	@Autowired
	private IRiskRegisterRepository riskRegisterRepository;

	@Autowired
	private ICommonService commonService;

	@Autowired
	private UtilsWs utilsWs;

	@Override
	public List<RiskRegister> getList() {
		try {
			return riskRegisterRepository.findAll();
		} catch (Exception e) {
			logger.error("Error RiskRegisterService in method getList :: " + e.toString());
			return null;
		}
	}

	@Override
	public RiskRegister findById(Long id) {
		try {
			SendObject sendObject = commonService.getObjectById(new RiskRegister(), id.toString(), false);
			if (sendObject.getCode().equals(ConstanteService._CODE_SERVICE_SUCCESS))
				return (RiskRegister) sendObject.getPayload();
			return null;
		} catch (Exception e) {
			logger.error("Error RiskRegisterService in method findById :: " + e.toString());
			return null;
		}
	}

	@Override
	public RiskRegister saveOrUpdate(RiskRegister entity) {
		try {
			return riskRegisterRepository.save(entity);
		} catch (Exception e) {
			logger.error("Error RiskRegisterService in method saveOrUpdate :: " + e.toString());
			return null;
		}
	}

	@Override
	public Boolean deleteById(Long id) {
		try {
			if (id == null)
				return false;
			riskRegisterRepository.delete(this.findById(id));
			return true;
		} catch (Exception e) {
			logger.error("Error RiskRegisterService in method deleteById :: " + e.toString());
			return false;
		}
	}

	@Override
	public SendObject findRiskRegisterByIdWs(Long id) {
		try {
			if (id == null)
				return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_ALIAS_PARAM, new JSONObject());
			RiskRegister entity = this.findById(id);
			if (entity == null)
				return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_NOT_EXISTS_ROW_DATA_BASE, new JSONObject());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_SUCCESS, new JSONObject(entity));
		} catch (Exception e) {
			logger.error("Error RiskRegisterService in method findRiskRegisterByIdWs :: " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	@Override
	public SendObject getListRiskRegisterWs() {
		try {
			return utilsWs.resultWs(ConstanteWs._CODE_WS_SUCCESS, new JSONArray(this.getList()));
		} catch (Exception e) {
			logger.error("Error RiskRegisterService in method getListRiskRegisterWs() :: " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	@Override
	public SendObject saveOrUpdateRiskRegisterWs(List<RiskRegister> entityList, Long idProject) {
		try {

			if (!entityList.isEmpty())
				riskRegisterRepository.deleteByIdProjet(idProject);
			for (RiskRegister entity : entityList) {
				entity.setCreatedAt(new Date());
				entity.setIdProjet(idProject);
				entity = this.saveOrUpdate(entity);
				if (entity == null)
					return utilsWs.resultWs(ConstanteService._CODE_SERVICE_ERROR_SAVE_OR_UPDATE, new JSONObject());
			}

			return utilsWs.resultWs(ConstanteService._CODE_SERVICE_SUCCESS, new JSONArray(entityList));
		} catch (Exception e) {
			logger.error("Error RiskRegisterService in method saveOrUpdateRiskRegisterWs :: " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	@Override
	public SendObject saveOrUpdateListOFRiskRegisterWs(List<RiskRegister> listOfentity) {
		try {

			if (!listOfentity.isEmpty()) {
				riskRegisterRepository.deleteByIdProjet(listOfentity.get(0).getIdProjet());

				for (RiskRegister aa : listOfentity) {

					this.saveOrUpdate(aa);

				}
			}
			if (listOfentity == null)
				return utilsWs.resultWs(ConstanteService._CODE_SERVICE_ERROR_SAVE_OR_UPDATE,
						new JSONArray(listOfentity));
			return utilsWs.resultWs(ConstanteService._CODE_SERVICE_SUCCESS, new JSONObject());
		} catch (Exception e) {
			logger.error("Error RiskRegisterService in method saveOrUpdateRiskRegisterWs :: " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	@Override
	public SendObject deleteRiskRegisterByIdWs(Long id) {
		try {
			Boolean resultDelete = this.deleteById(id);
			if (resultDelete == false)
				return utilsWs.resultWs(ConstanteService._CODE_SERVICE_ERROR_DELETE_ROW, new JSONObject());
			return utilsWs.resultWs(ConstanteService._CODE_SERVICE_SUCCESS, new JSONObject());
		} catch (Exception e) {
			logger.error("Error RiskRegisterService in method getListRiskRegister " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	@Override
	public SendObject uploadFileWs(MultipartFile uploadedFile, Long idProject) {
		try {

			InputStreamReader inReader = new InputStreamReader(uploadedFile.getInputStream(), "ISO-8859-1");
			try (ICsvListReader listReader = new CsvListReader(inReader, CsvPreference.STANDARD_PREFERENCE)) {
				final String[] headers = listReader.getHeader(true);
				List<String> row = null;
				JSONArray ja = new JSONArray();
				while ((row = listReader.read()) != null) {

					if (listReader.length() != headers.length) {
						return utilsWs.resultWs("skipping invalid row: ", row);

					}
					Map<String, String> rowMap = new HashMap<String, String>();
					Util.filterListToMap(rowMap, headers, row);
					ja.put(rowMap);
				}
				listReader.close();
				return utilsWs.resultWs(ConstanteService._CODE_SERVICE_SUCCESS, ja);
			}
		} catch (Exception e) {
			logger.error("Error ProjectService in method uploadFileWs " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

}
