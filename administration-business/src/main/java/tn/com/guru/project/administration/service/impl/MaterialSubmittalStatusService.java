package tn.com.guru.project.administration.service.impl;

import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tn.com.guru.project.administration.model.MaterialSubmittalStatus;
import tn.com.guru.project.administration.repository.IMaterialSubmittalStatusRepository;
import tn.com.guru.project.administration.service.IMaterialSubmittalStatusService;
import tn.com.guru.project.common.service.ICommonService;
import tn.com.guru.project.tools.ConstanteService;
import tn.com.guru.project.tools.ConstanteWs;
import tn.com.guru.project.tools.UtilsWs;
import tn.com.guru.project.tools.model.SendObject;

@Service
public class MaterialSubmittalStatusService implements IMaterialSubmittalStatusService {

	private static final Logger logger = LogManager.getLogger(MaterialSubmittalStatusService.class);

	@Autowired
	private IMaterialSubmittalStatusRepository materialSubmittalStatusRepository;

	@Autowired
	private ICommonService commonService;

	@Autowired
	private UtilsWs utilsWs;

	@Override
	public List<MaterialSubmittalStatus> getList() {
		try {
			return materialSubmittalStatusRepository.findAll();
		} catch (Exception e) {
			logger.error("Error MaterialSubmittalStatusService in method getList :: " + e.toString());
			return null;
		}
	}

	@Override
	public MaterialSubmittalStatus findById(Long id) {
		try {
			SendObject sendObject = commonService.getObjectById(new MaterialSubmittalStatus(), id.toString(), false);
			if (sendObject.getCode().equals(ConstanteService._CODE_SERVICE_SUCCESS))
				return (MaterialSubmittalStatus) sendObject.getPayload();
			return null;
		} catch (Exception e) {
			logger.error("Error MaterialSubmittalStatusService in method findById :: " + e.toString());
			return null;
		}
	}

	@Override
	public MaterialSubmittalStatus saveOrUpdate(MaterialSubmittalStatus entity) {
		try {
			return materialSubmittalStatusRepository.save(entity);
		} catch (Exception e) {
			logger.error("Error MaterialSubmittalStatusService in method saveOrUpdate :: " + e.toString());
			return null;
		}
	}

	@Override
	public Boolean deleteById(Long id) {
		try {
			if (id == null)
				return false;
			materialSubmittalStatusRepository.delete(this.findById(id));
			return true;
		} catch (Exception e) {
			logger.error("Error MaterialSubmittalStatusService in method deleteById :: " + e.toString());
			return false;
		}
	}

	@Override
	public SendObject findMaterialSubmittalStatusByIdWs(Long id) {
		try {
			if (id == null)
				return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_ALIAS_PARAM, new JSONObject());
			MaterialSubmittalStatus entity = this.findById(id);
			if (entity == null)
				return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_NOT_EXISTS_ROW_DATA_BASE, new JSONObject());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_SUCCESS, new JSONObject(entity));
		} catch (Exception e) {
			logger.error("Error MaterialSubmittalStatusService in method findMaterialSubmittalStatusByIdWs :: "
					+ e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	@Override
	public SendObject getListMaterialSubmittalStatusWs() {
		try {
			return utilsWs.resultWs(ConstanteWs._CODE_WS_SUCCESS, new JSONArray(this.getList()));
		} catch (Exception e) {
			logger.error("Error MaterialSubmittalStatusService in method getListMaterialSubmittalStatusWs() :: "
					+ e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	@Override
	public SendObject saveOrUpdateMaterialSubmittalStatusWs(List<MaterialSubmittalStatus> entityList, Long idProject) {
		try {

			materialSubmittalStatusRepository.deleteByIdProjet(idProject);
			for (MaterialSubmittalStatus entity : entityList) {
				entity.setCreatedAt(new Date());
				entity.setIdProjet(idProject);
				entity = this.saveOrUpdate(entity);
				if (entity == null)
					return utilsWs.resultWs(ConstanteService._CODE_SERVICE_ERROR_SAVE_OR_UPDATE, new JSONObject());
			}

			return utilsWs.resultWs(ConstanteService._CODE_SERVICE_SUCCESS, new JSONArray(entityList));
		} catch (Exception e) {
			logger.error("Error MaterialSubmittalStatusService in method saveOrUpdateMaterialSubmittalStatusWs :: "
					+ e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	@Override
	public SendObject deleteMaterialSubmittalStatusByIdWs(Long id) {
		try {
			Boolean resultDelete = this.deleteById(id);
			if (resultDelete == false)
				return utilsWs.resultWs(ConstanteService._CODE_SERVICE_ERROR_DELETE_ROW, new JSONObject());
			return utilsWs.resultWs(ConstanteService._CODE_SERVICE_SUCCESS, new JSONObject());
		} catch (Exception e) {
			logger.error(
					"Error MaterialSubmittalStatusService in method getListMaterialSubmittalStatus " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	@Override
	public SendObject getMaterialSubmittalStatusByIdWs(Long idProject) {
		try {
			List<MaterialSubmittalStatus> list = materialSubmittalStatusRepository.getListByIdProjet(idProject);
			JSONObject jo;
			JSONObject joo;
			JSONArray ja = new JSONArray();

			for (MaterialSubmittalStatus e : list) {
				jo = new JSONObject();
				joo = new JSONObject();
				jo.put("name", e.getSummary());
				jo.put("value", e.getTotal());
				joo.put("ar", e.getAr());
				joo.put("st", e.getSt());
				joo.put("cv", e.getCv());
				joo.put("dr", e.getDr());
				joo.put("el", e.getEl());
				joo.put("la", e.getLa());
				joo.put("me", e.getMe());
				joo.put("pourcentage", e.getPourcentage());
				jo.put("extra", joo);
				ja.put(jo);
			}

			return utilsWs.resultWs(ConstanteWs._CODE_WS_SUCCESS, ja);
		} catch (Exception e) {
			logger.error("Error MaterialSubmittalStatusService in method getMaterialSubmittalStatusByIdWs() :: "
					+ e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

}
