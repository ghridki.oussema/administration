package tn.com.guru.project.administration.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import tn.com.guru.project.administration.repository.IProjectViewerRepository;
import tn.com.guru.project.administration.service.IBim360Service;
import tn.com.guru.project.tools.Constante;
import tn.com.guru.project.tools.ConstanteService;
import tn.com.guru.project.tools.ConstanteWs;
import tn.com.guru.project.tools.UtilsWs;
import tn.com.guru.project.tools.model.SendObject;

import java.util.*;

@Service
public class Bim360Service implements IBim360Service {

    private static final Logger logger = LogManager.getLogger(Bim360Service.class);

    private final IProjectViewerRepository projectViewerRepository;

    private final UtilsWs utilsWs;

    ObjectMapper om = new ObjectMapper();

    public Bim360Service(IProjectViewerRepository projectViewerRepository, UtilsWs utilsWs) {
        this.projectViewerRepository = projectViewerRepository;
        this.utilsWs = utilsWs;
    }


    @Override
    public SendObject getBim360DocumentManagementWs(String clientId, String clientSecret, String projectId) {
        try {

            JSONObject token = this.getForgeToken(clientId, clientSecret);
            if (token == null) {
                logger.error("Error Bim360Service in method getBim360DocumentManagementWs ");
                return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
            }

            List<String> listViewerUrn = new ArrayList<>();
            if (!projectId.equals("")) {
                listViewerUrn = projectViewerRepository.findListDocUrnByIdProject( Long.parseLong(projectId));
            }

            List<JSONObject> list = this.getHubsList(token, listViewerUrn);
            if (list == null) {
                logger.error("Error Bim360Service in method getBim360DocumentManagementWs ");
                return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
            }

            return utilsWs.resultWs(ConstanteService._CODE_SERVICE_SUCCESS, new JSONArray(list));
        } catch (Exception e) {
            logger.error("Error Bim360Service in method getBim360DocumentManagementWs " + e);
            return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
        }
    }

    JSONObject getForgeToken(String clientId, String clientSecret) {
        try {
            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(Constante.GET_FORGE_TOKEN);
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
            MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
            map.add("client_id", clientId);
            map.add("client_secret", clientSecret);
            map.add("grant_type", "client_credentials");
            map.add("scope", "data:read data:write data:create data:search bucket:create bucket:read bucket:update bucket:delete code:all account:read viewables:read");
            HttpEntity<?> send = new HttpEntity<>(map, headers);
            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity<String> response = restTemplate.exchange(builder.toUriString(), HttpMethod.POST, send,
                    String.class);

            if (response.getStatusCode() != HttpStatus.OK) {
                logger.error("Error Bim360Service in method getForgeToken " + response.getBody());
                return null;
            }


            return this.om.convertValue(response.getBody(), JSONObject.class);
        } catch (Exception e) {
            logger.error("Error Bim360Service in method getForgeToken " + e.toString());
            return null;
        }
    }

    List<JSONObject> getHubsList(JSONObject token, List<String> listViewerUrn) {
        try {
            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(Constante.GET_HUBS_LIST);
            HttpHeaders headers = new HttpHeaders();
            headers.set("Authorization", token.getString("token_type") + " " + token.getString("access_token"));
            headers.set(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
            HttpEntity<?> send = new HttpEntity<>(null, headers);
            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity<String> response = restTemplate.exchange(builder.toUriString(), HttpMethod.GET, send,
                    String.class);

            if (response.getStatusCode() != HttpStatus.OK) {
                logger.error("Error Bim360Service in method getHubsList " + response.getBody());
                return null;
            }

            List<JSONObject> listHubs = new ArrayList<>();
            JSONArray data = this.om.convertValue(response.getBody(), JSONObject.class).getJSONArray("data");
            for (Object datum : data) {
                JSONObject m = new JSONObject();
                m.put("type", "hub");
                m.put("id", ((JSONObject) datum).getString("id"));
                m.put("name", ((JSONObject) datum).getJSONObject("attributes").getString("name"));
                m.put("displayName", ((JSONObject) datum).getJSONObject("attributes").getString("name"));
                m.put("href", ((JSONObject) datum).getJSONObject("links").getJSONObject("self").getString("href"));

                List<JSONObject> lisProjects = this.getProjectsList(token, m.getString("href"), listViewerUrn);
                if (lisProjects == null) {
                    logger.error("Error Bim360Service in method getHubsList ");
                    return null;
                }
                m.put("children", lisProjects);
                listHubs.add(m);
            }

            return listHubs;
        } catch (Exception e) {
            logger.error("Error Bim360Service in method getHubsList " + e.toString());
            return null;
        }
    }

    List<JSONObject> getProjectsList(JSONObject token, String hubHref, List<String> listViewerUrn) {
        try {
            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(hubHref);
            builder.path("/projects");
            HttpHeaders headers = new HttpHeaders();
            headers.set("Authorization", token.getString("token_type") + " " + token.getString("access_token"));
            headers.set(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
            HttpEntity<?> send = new HttpEntity<>(null, headers);
            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity<String> response = restTemplate.exchange(builder.toUriString(), HttpMethod.GET, send,
                    String.class);

            if (response.getStatusCode() != HttpStatus.OK) {
                logger.error("Error Bim360Service in method getProjectsList " + response.getBody());
                return null;
            }

            List<JSONObject> listProjects = new ArrayList<>();
            JSONArray data = this.om.convertValue(response.getBody(), JSONObject.class).getJSONArray("data");
            for (Object datum : data) {
                JSONObject m = new JSONObject();
                m.put("id", ((JSONObject) datum).getString("id"));
                m.put("type", "project");
                m.put("name", ((JSONObject) datum).getJSONObject("attributes").getString("name"));
                m.put("displayName", ((JSONObject) datum).getJSONObject("attributes").getString("name"));
                m.put("href", ((JSONObject) datum).getJSONObject("links").getJSONObject("self").getString("href"));

                List<JSONObject> foldersList = this.getTopFoldersList(token, m.getString("href"), listViewerUrn);
                if (foldersList == null) {
                    logger.error("Error Bim360Service in method getProjectsList ");
                    return null;
                }

                m.put("children", foldersList);
                listProjects.add(m);
            }

            return listProjects;
        } catch (Exception e) {
            logger.error("Error Bim360Service in method getProjectsList " + e.toString());
            return null;
        }
    }

    List<JSONObject> getTopFoldersList(JSONObject token, String projectHref, List<String> listViewerUrn) {
        try {
            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(projectHref);
            builder.path("/topFolders");
            HttpHeaders headers = new HttpHeaders();
            headers.set("Authorization", token.getString("token_type") + " " + token.getString("access_token"));
            headers.set(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
            HttpEntity<?> send = new HttpEntity<>(null, headers);
            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity<String> response = restTemplate.exchange(builder.toUriString(), HttpMethod.GET, send,
                    String.class);

            if (response.getStatusCode() != HttpStatus.OK) {
                logger.error("Error Bim360Service in method getTopFoldersList " + response.getBody());
                return null;
            }


            List<JSONObject> listTopFolders = new ArrayList<>();
            JSONArray data = this.om.convertValue(response.getBody(), JSONObject.class).getJSONArray("data");
            for (Object datum : data) {
                JSONObject attributes = ((JSONObject) datum).getJSONObject("attributes");
                if (attributes.getInt("objectCount") != 0 && !attributes.getBoolean("hidden")) {
                    JSONObject m = new JSONObject();
                    m.put("id", ((JSONObject) datum).getString("id"));
                    m.put("type", "folder");
                    m.put("name", attributes.getString("name"));
                    m.put("displayName", attributes.getString("displayName"));
                    m.put("href", ((JSONObject) datum).getJSONObject("links").getJSONObject("self").getString("href"));

                    List<JSONObject> topFolderContent = this.getFolderContent(token, m.getString("href"), listViewerUrn);
                    if (topFolderContent == null) {
                        logger.error("Error Bim360Service in method getTopFoldersList ");
                        return null;
                    }
                    m.put("children", topFolderContent);

                    listTopFolders.add(m);
                }

            }

            return listTopFolders;
        } catch (Exception e) {
            logger.error("Error Bim360Service in method getTopFoldersList " + e.toString());
            return null;
        }
    }

    List<JSONObject> getFolderContent(JSONObject token, String topFolderHref, List<String>  listViewerUrn) {
        try {
            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(topFolderHref);
            builder.path("/contents");
            HttpHeaders headers = new HttpHeaders();
            headers.set("Authorization", token.getString("token_type") + " " + token.getString("access_token"));
            headers.set(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
            HttpEntity<?> send = new HttpEntity<>(null, headers);
            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity<String> response = restTemplate.exchange(builder.toUriString(), HttpMethod.GET, send,
                    String.class);

            if (response.getStatusCode() != HttpStatus.OK) {
                logger.error("Error Bim360Service in method getTopFoldersContent " + response.getBody());
                return null;
            }

            JSONObject responseBody = this.om.convertValue(response.getBody(), JSONObject.class);
            List<JSONObject> listFolderContent = new ArrayList<>();

            // add the documents
            if (responseBody.has("included") && !responseBody.getJSONArray("included").isEmpty()) {
                JSONArray included = responseBody.getJSONArray("included");
                for (Object datum : included) {
                    if (((JSONObject) datum).getJSONObject("relationships").has("derivatives")) {
                        JSONObject d = new JSONObject();
                        d.put("id", ((JSONObject) datum).getString("id"));
                        d.put("type", "document");
                        d.put("name", ((JSONObject) datum).getJSONObject("attributes").getString("name"));
                        d.put("displayName", ((JSONObject) datum).getJSONObject("attributes").getString("displayName"));
                        d.put("webViewLink", ((JSONObject) datum).getJSONObject("links").getJSONObject("webView").getString("href"));
                        d.put("urn", ((JSONObject) datum).getJSONObject("relationships").getJSONObject("derivatives").getJSONObject("data").getString("id"));
                        d.put("isChecked", listViewerUrn.contains(d.getString("urn")));
                        listFolderContent.add(d);
                    }
                }
            }


            // add the folders
            if (responseBody.has("data") && !responseBody.getJSONArray("data").isEmpty()) {
                JSONArray data = responseBody.getJSONArray("data");
                for (Object datum : data) {
                    if (((JSONObject) datum).getString("type").equals("folders")) {
                        JSONObject f = new JSONObject();
                        f.put("id", ((JSONObject) datum).getString("id"));
                        f.put("type", "folder");
                        f.put("name", ((JSONObject) datum).getJSONObject("attributes").getString("name"));
                        f.put("displayName", ((JSONObject) datum).getJSONObject("attributes").getString("displayName"));
                        f.put("href", ((JSONObject) datum).getJSONObject("links").getJSONObject("self").getString("href"));

                        List<JSONObject> folderContent = this.getFolderContent(token, f.getString("href"), listViewerUrn);
                        if (folderContent == null) {
                            logger.error("Error Bim360Service in method getTopFolderContent ");
                            return null;
                        }
                        f.put("children", folderContent);

                        listFolderContent.add(f);
                    }
                }
            }


            return listFolderContent;
        } catch (Exception e) {
            logger.error("Error Bim360Service in method getTopFoldersContent " + e.toString());
            return null;
        }
    }


}
