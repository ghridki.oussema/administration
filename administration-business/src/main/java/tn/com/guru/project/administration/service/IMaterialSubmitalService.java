package tn.com.guru.project.administration.service;

import java.util.List;

import tn.com.guru.project.administration.model.MaterialSubmital;
import tn.com.guru.project.tools.model.SendObject;

public interface IMaterialSubmitalService {

	public List<MaterialSubmital> getList();

	public MaterialSubmital findById(Long id);

	public MaterialSubmital saveOrUpdate(MaterialSubmital entity);

	public Boolean deleteById(Long id);

	public SendObject findMaterialSubmitalByIdWs(Long id);

	public SendObject getListMaterialSubmitalWs();

	public SendObject saveOrUpdateMaterialSubmitalWs(List<MaterialSubmital> entity, Long idProject);

	public SendObject deleteMaterialSubmitalByIdWs(Long id);

}
