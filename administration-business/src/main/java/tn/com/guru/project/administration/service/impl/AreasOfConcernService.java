package tn.com.guru.project.administration.service.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tn.com.guru.project.administration.model.AreasOfConcern;
import tn.com.guru.project.administration.repository.IAreasOfConcernDao;
import tn.com.guru.project.administration.service.IAreasOfConcernService;
import tn.com.guru.project.common.service.ICommonService;
import tn.com.guru.project.tools.ConstanteService;
import tn.com.guru.project.tools.ConstanteWs;
import tn.com.guru.project.tools.UtilsWs;
import tn.com.guru.project.tools.model.SendObject;

@Service
public class AreasOfConcernService implements IAreasOfConcernService {

	private static final Logger logger = LogManager.getLogger(AreasOfConcernService.class);

	@Autowired
	private IAreasOfConcernDao areasOfConcernRepository;

	@Autowired
	private ICommonService commonService;

	@Autowired
	private UtilsWs utilsWs;

	@Override
	public List<AreasOfConcern> getList() {
		// TODO Auto-generated method stub
		return areasOfConcernRepository.findAll();
	}

	@Override
	public AreasOfConcern findById(Long id) {
		try {
			SendObject sendObject = commonService.getObjectById(new AreasOfConcern(), id.toString(), false);
			if (sendObject.getCode().equals(ConstanteService._CODE_SERVICE_SUCCESS))
				return (AreasOfConcern) sendObject.getPayload();
			return null;
		} catch (Exception e) {
			logger.error("Error AreasOfConcernService in method findById :: " + e.toString());
			return null;
		}
	}

	@Override
	public AreasOfConcern saveOrUpdate(AreasOfConcern entity) {
		try {
			if (entity.getId() == null) {
				entity.setSeq(areasOfConcernRepository.getSeq(entity.getIdProjet()));
			}
			return areasOfConcernRepository.save(entity);
		} catch (Exception e) {
			logger.error("Error AreasOfConcernService in method saveOrUpdate :: " + e.toString());
			return null;
		}
	}

	@Override
	public Boolean deleteById(Long id) {
		try {
			if (id == null)
				return false;
			areasOfConcernRepository.delete(this.findById(id));
			return true;
		} catch (Exception e) {
			logger.error("Error AreasOfConcernService in method deleteById :: " + e.toString());
			return false;
		}
	}

	@Override
	public SendObject findAreasOfConcernByIdWs(Long id) {
		try {
			if (id == null)
				return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_ALIAS_PARAM, new JSONObject());
			AreasOfConcern entity = this.findById(id);
			if (entity == null)
				return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_NOT_EXISTS_ROW_DATA_BASE, new JSONObject());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_SUCCESS, new JSONObject(entity));
		} catch (Exception e) {
			logger.error("Error AreasOfConcernService in method findAreasOfConcernByIdWs :: " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	@Override
	public SendObject getListAreasOfConcernWs() {
		try {
			return utilsWs.resultWs(ConstanteWs._CODE_WS_SUCCESS, new JSONArray(this.getList()));
		} catch (Exception e) {
			logger.error("Error AreasOfConcernService in method getListAreasOfConcernWs() :: " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	@Override
	public SendObject saveOrUpdateAreasOfConcernWs(AreasOfConcern entity) {
		try {
			entity = this.saveOrUpdate(entity);
			if (entity == null)
				return utilsWs.resultWs(ConstanteService._CODE_SERVICE_ERROR_SAVE_OR_UPDATE, new JSONObject());
			return utilsWs.resultWs(ConstanteService._CODE_SERVICE_SUCCESS, new JSONObject(entity));
		} catch (Exception e) {
			logger.error("Error AreasOfConcernService in method saveOrUpdateAreasOfConcernWs :: " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	@Override
	public SendObject deleteAreasOfConcernByIdWs(Long id) {
		try {
			Boolean resultDelete = this.deleteById(id);
			if (resultDelete == false)
				return utilsWs.resultWs(ConstanteService._CODE_SERVICE_ERROR_DELETE_ROW, new JSONObject());
			return utilsWs.resultWs(ConstanteService._CODE_SERVICE_SUCCESS, new JSONObject());
		} catch (Exception e) {
			logger.error("Error AreasOfConcernService in method getListAreasOfConcern " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}
}
