package tn.com.guru.project.administration.service.impl;

import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tn.com.guru.project.administration.model.MaterialSubmital;
import tn.com.guru.project.administration.repository.IMaterialSubmitalRepository;
import tn.com.guru.project.administration.service.IMaterialSubmitalService;
import tn.com.guru.project.common.service.ICommonService;
import tn.com.guru.project.tools.ConstanteService;
import tn.com.guru.project.tools.ConstanteWs;
import tn.com.guru.project.tools.UtilsWs;
import tn.com.guru.project.tools.model.SendObject;

@Service
public class MaterialSubmitalService implements IMaterialSubmitalService {

	private static final Logger logger = LogManager.getLogger(MaterialSubmitalService.class);

	@Autowired
	private IMaterialSubmitalRepository materialSubmitalRepository;

	@Autowired
	private ICommonService commonService;

	@Autowired
	private UtilsWs utilsWs;

	@Override
	public List<MaterialSubmital> getList() {
		try {
			return materialSubmitalRepository.findAll();
		} catch (Exception e) {
			logger.error("Error MaterialSubmitalService in method getList :: " + e.toString());
			return null;
		}
	}

	@Override
	public MaterialSubmital findById(Long id) {
		try {
			SendObject sendObject = commonService.getObjectById(new MaterialSubmital(), id.toString(), false);
			if (sendObject.getCode().equals(ConstanteService._CODE_SERVICE_SUCCESS))
				return (MaterialSubmital) sendObject.getPayload();
			return null;
		} catch (Exception e) {
			logger.error("Error MaterialSubmitalService in method findById :: " + e.toString());
			return null;
		}
	}

	@Override
	public MaterialSubmital saveOrUpdate(MaterialSubmital entity) {
		try {
			return materialSubmitalRepository.save(entity);
		} catch (Exception e) {
			logger.error("Error MaterialSubmitalService in method saveOrUpdate :: " + e.toString());
			return null;
		}
	}

	@Override
	public Boolean deleteById(Long id) {
		try {
			if (id == null)
				return false;
			materialSubmitalRepository.delete(this.findById(id));
			return true;
		} catch (Exception e) {
			logger.error("Error MaterialSubmitalService in method deleteById :: " + e.toString());
			return false;
		}
	}

	@Override
	public SendObject findMaterialSubmitalByIdWs(Long id) {
		try {
			if (id == null)
				return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_ALIAS_PARAM, new JSONObject());
			MaterialSubmital entity = this.findById(id);
			if (entity == null)
				return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_NOT_EXISTS_ROW_DATA_BASE, new JSONObject());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_SUCCESS, new JSONObject(entity));
		} catch (Exception e) {
			logger.error("Error MaterialSubmitalService in method findMaterialSubmitalByIdWs :: " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	@Override
	public SendObject getListMaterialSubmitalWs() {
		try {
			return utilsWs.resultWs(ConstanteWs._CODE_WS_SUCCESS, new JSONArray(this.getList()));
		} catch (Exception e) {
			logger.error("Error MaterialSubmitalService in method getListMaterialSubmitalWs() :: " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	@Override
	public SendObject saveOrUpdateMaterialSubmitalWs(List<MaterialSubmital> entityList, Long idProject) {
		try {
			if (!entityList.isEmpty())
				materialSubmitalRepository.deleteByIdProjet(idProject);
			for (MaterialSubmital entity : entityList) {
				entity.setCreatedAt(new Date());
				entity.setIdProjet(idProject);
				entity = this.saveOrUpdate(entity);
				if (entity == null)
					return utilsWs.resultWs(ConstanteService._CODE_SERVICE_ERROR_SAVE_OR_UPDATE, new JSONObject());
			}

			return utilsWs.resultWs(ConstanteService._CODE_SERVICE_SUCCESS, new JSONArray(entityList));
		} catch (Exception e) {
			logger.error("Error MaterialSubmitalService in method saveOrUpdateMaterialSubmitalWs :: " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	@Override
	public SendObject deleteMaterialSubmitalByIdWs(Long id) {
		try {
			Boolean resultDelete = this.deleteById(id);
			if (resultDelete == false)
				return utilsWs.resultWs(ConstanteService._CODE_SERVICE_ERROR_DELETE_ROW, new JSONObject());
			return utilsWs.resultWs(ConstanteService._CODE_SERVICE_SUCCESS, new JSONObject());
		} catch (Exception e) {
			logger.error("Error MaterialSubmitalService in method getListMaterialSubmital " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

}
