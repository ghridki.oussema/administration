package tn.com.guru.project.administration.service;

import java.util.List;

import tn.com.guru.project.administration.model.LaborHistogram;
import tn.com.guru.project.tools.model.SendObject;

public interface ILaborHistogramService {

	public List<LaborHistogram> getList();

	public LaborHistogram findById(Long id);

	public LaborHistogram saveOrUpdate(LaborHistogram entity);

	public Boolean deleteById(Long id);

	public SendObject findLaborHistogramByIdWs(Long id);

	public SendObject getListLaborHistogramWs();

	public SendObject saveOrUpdateLaborHistogramWs(LaborHistogram entity);

	public SendObject deleteLaborHistogramByIdWs(Long id);

	SendObject saveOrUpdateListOFLaborHistogramws(List<LaborHistogram> listOfentity);

}
