package tn.com.guru.project.administration.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import tn.com.guru.project.administration.model.Project;
import tn.com.guru.project.tools.model.SendObject;

public interface IProjetService {

	public List<Project> getList();

	public Project findById(Long id);

	public Project saveOrUpdate(Project entity);

	public Boolean deleteById(Long id);

	public SendObject findProjectByIdWs(Long id);

	public SendObject getListProjectWs();

	public SendObject saveOrUpdateProjectWs(Project entity, List<MultipartFile> filedata);

	public SendObject deleteProjectByIdWs(Long id);

	public SendObject uploadFileWs(MultipartFile filedata,Long idProject);
}
