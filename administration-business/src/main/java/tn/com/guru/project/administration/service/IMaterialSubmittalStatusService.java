package tn.com.guru.project.administration.service;

import java.util.List;

import tn.com.guru.project.administration.model.MaterialSubmittalStatus;
import tn.com.guru.project.tools.model.SendObject;

public interface IMaterialSubmittalStatusService {

	public List<MaterialSubmittalStatus> getList();

	public MaterialSubmittalStatus findById(Long id);

	public MaterialSubmittalStatus saveOrUpdate(MaterialSubmittalStatus entity);

	public Boolean deleteById(Long id);

	public SendObject findMaterialSubmittalStatusByIdWs(Long id);

	public SendObject getListMaterialSubmittalStatusWs();

	public SendObject saveOrUpdateMaterialSubmittalStatusWs(List<MaterialSubmittalStatus> entity, Long idProject);

	public SendObject deleteMaterialSubmittalStatusByIdWs(Long id);

	public SendObject getMaterialSubmittalStatusByIdWs(Long idProject);

}
