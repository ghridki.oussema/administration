package tn.com.guru.project.administration.service.impl;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tn.com.guru.project.administration.model.AdmPersonnel;
import tn.com.guru.project.administration.repository.IAdmPersonnelRepository;
import tn.com.guru.project.administration.service.IAdmPersonnelService;
import tn.com.guru.project.common.service.ICommonService;
import tn.com.guru.project.tools.ConstanteService;
import tn.com.guru.project.tools.ConstanteWs;
import tn.com.guru.project.tools.UtilsWs;
import tn.com.guru.project.tools.model.SendObject;

@Service
public class AdmPersonnelService implements IAdmPersonnelService {

	private static final Logger logger = LogManager.getLogger(AdmPersonnelService.class);

	@Autowired
	private IAdmPersonnelRepository admPersonnelRepository;

	@Autowired
	private ICommonService commonService;

	@Autowired
	private UtilsWs utilsWs;

	@Override
	public List<AdmPersonnel> getList() {
		try {
			return admPersonnelRepository.findAll();
		} catch (Exception e) {
			logger.error("Error AdmPersonnelService in method getList :: " + e.toString());
			return null;
		}
	}

	@Override
	public AdmPersonnel findById(Long id) {
		try {
			SendObject sendObject = commonService.getObjectById(new AdmPersonnel(), id.toString(), false);
			if (sendObject.getCode().equals(ConstanteService._CODE_SERVICE_SUCCESS))
				return (AdmPersonnel) sendObject.getPayload();
			return null;
		} catch (Exception e) {
			logger.error("Error AdmPersonnelService in method findById :: " + e.toString());
			return null;
		}
	}

	@Override
	public AdmPersonnel saveOrUpdate(AdmPersonnel entity) {
		try {
			if (entity.getDtMaj() == null)
				entity.setDtMaj(new Timestamp(new Date().getTime()));
			return admPersonnelRepository.save(entity);
		} catch (Exception e) {
			logger.error("Error AdmPersonnelService in method saveOrUpdate :: " + e.toString());
			return null;
		}
	}

	@Override
	public Boolean deleteById(Long id) {
		try {
			if (id == null)
				return false;
			admPersonnelRepository.delete(this.findById(id));
			return true;
		} catch (Exception e) {
			logger.error("Error AdmPersonnelService in method deleteById :: " + e.toString());
			return false;
		}
	}

	@Override
	public SendObject findAdmPersonnelByIdWs(Long id) {
		try {
			if (id == null)
				return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_ALIAS_PARAM, new JSONObject());
			AdmPersonnel entity = this.findById(id);
			if (entity == null)
				return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_NOT_EXISTS_ROW_DATA_BASE, new JSONObject());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_SUCCESS, new JSONObject(entity));
		} catch (Exception e) {
			logger.error("Error AdmPersonnelService in method findAdmPersonnelByIdWs :: " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	@Override
	public SendObject getListAdmPersonnelWs() {
		try {
			return utilsWs.resultWs(ConstanteWs._CODE_WS_SUCCESS, new JSONArray(this.getList()));
		} catch (Exception e) {
			logger.error("Error AdmPersonnelService in method getListAdmPersonnelWs() :: " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	@Override
	public SendObject saveOrUpdateAdmPersonnelWs(AdmPersonnel entity) {
		try {
			entity = this.saveOrUpdate(entity);
			if (entity == null)
				return utilsWs.resultWs(ConstanteService._CODE_SERVICE_ERROR_SAVE_OR_UPDATE, new JSONObject());
			return utilsWs.resultWs(ConstanteService._CODE_SERVICE_SUCCESS, new JSONObject(entity));
		} catch (Exception e) {
			logger.error("Error AdmPersonnelService in method saveOrUpdateAdmPersonnelWs :: " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	@Override
	public SendObject deleteAdmPersonnelByIdWs(Long id) {
		try {
			Boolean resultDelete = this.deleteById(id);
			if (resultDelete == false)
				return utilsWs.resultWs(ConstanteService._CODE_SERVICE_ERROR_DELETE_ROW, new JSONObject());
			return utilsWs.resultWs(ConstanteService._CODE_SERVICE_SUCCESS, new JSONObject());
		} catch (Exception e) {
			logger.error("Error AdmPersonnelService in method getListAdmPersonnel " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

}
