package tn.com.guru.project.administration.service.impl;

import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tn.com.guru.project.administration.model.ShopDrawingsStatus;
import tn.com.guru.project.administration.repository.IShopDrawingsStatusRepository;
import tn.com.guru.project.administration.service.IShopDrawingsStatusService;
import tn.com.guru.project.common.service.ICommonService;
import tn.com.guru.project.tools.ConstanteService;
import tn.com.guru.project.tools.ConstanteWs;
import tn.com.guru.project.tools.UtilsWs;
import tn.com.guru.project.tools.model.SendObject;

@Service
public class ShopDrawingsStatusService implements IShopDrawingsStatusService {

	private static final Logger logger = LogManager.getLogger(ShopDrawingsStatusService.class);

	@Autowired
	private IShopDrawingsStatusRepository shopDrawingsStatusRepository;

	@Autowired
	private ICommonService commonService;

	@Autowired
	private UtilsWs utilsWs;

	@Override
	public List<ShopDrawingsStatus> getList() {
		try {
			return shopDrawingsStatusRepository.findAll();
		} catch (Exception e) {
			logger.error("Error ShopDrawingsStatusService in method getList :: " + e.toString());
			return null;
		}
	}

	@Override
	public ShopDrawingsStatus findById(Long id) {
		try {
			SendObject sendObject = commonService.getObjectById(new ShopDrawingsStatus(), id.toString(), false);
			if (sendObject.getCode().equals(ConstanteService._CODE_SERVICE_SUCCESS))
				return (ShopDrawingsStatus) sendObject.getPayload();
			return null;
		} catch (Exception e) {
			logger.error("Error ShopDrawingsStatusService in method findById :: " + e.toString());
			return null;
		}
	}

	@Override
	public ShopDrawingsStatus saveOrUpdate(ShopDrawingsStatus entity) {
		try {
			return shopDrawingsStatusRepository.save(entity);
		} catch (Exception e) {
			logger.error("Error ShopDrawingsStatusService in method saveOrUpdate :: " + e.toString());
			return null;
		}
	}

	@Override
	public Boolean deleteById(Long id) {
		try {
			if (id == null)
				return false;
			shopDrawingsStatusRepository.delete(this.findById(id));
			return true;
		} catch (Exception e) {
			logger.error("Error ShopDrawingsStatusService in method deleteById :: " + e.toString());
			return false;
		}
	}

	@Override
	public SendObject findShopDrawingsStatusByIdWs(Long id) {
		try {
			if (id == null)
				return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_ALIAS_PARAM, new JSONObject());
			ShopDrawingsStatus entity = this.findById(id);
			if (entity == null)
				return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_NOT_EXISTS_ROW_DATA_BASE, new JSONObject());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_SUCCESS, new JSONObject(entity));
		} catch (Exception e) {
			logger.error("Error ShopDrawingsStatusService in method findShopDrawingsStatusByIdWs :: " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	@Override
	public SendObject getListShopDrawingsStatusWs() {
		try {
			return utilsWs.resultWs(ConstanteWs._CODE_WS_SUCCESS, new JSONArray(this.getList()));
		} catch (Exception e) {
			logger.error("Error ShopDrawingsStatusService in method getListShopDrawingsStatusWs() :: " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	@Override
	public SendObject saveOrUpdateShopDrawingsStatusWs(List<ShopDrawingsStatus> entityList, Long idProject) {
		try {
			if (!entityList.isEmpty())
				shopDrawingsStatusRepository.deleteByIdProjet(idProject);

			for (ShopDrawingsStatus entity : entityList) {
				entity.setIdProjet(idProject);
				entity.setCreatedAt(new Date());;
				entity = this.saveOrUpdate(entity);
				if (entity == null)
					return utilsWs.resultWs(ConstanteService._CODE_SERVICE_ERROR_SAVE_OR_UPDATE, new JSONObject());
			}

			return utilsWs.resultWs(ConstanteService._CODE_SERVICE_SUCCESS, new JSONArray(entityList));
		} catch (Exception e) {
			logger.error(
					"Error ShopDrawingsStatusService in method saveOrUpdateShopDrawingsStatusWs :: " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	@Override
	public SendObject deleteShopDrawingsStatusByIdWs(Long id) {
		try {
			Boolean resultDelete = this.deleteById(id);
			if (resultDelete == false)
				return utilsWs.resultWs(ConstanteService._CODE_SERVICE_ERROR_DELETE_ROW, new JSONObject());
			return utilsWs.resultWs(ConstanteService._CODE_SERVICE_SUCCESS, new JSONObject());
		} catch (Exception e) {
			logger.error("Error ShopDrawingsStatusService in method getListShopDrawingsStatus " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	@Override
	public SendObject getListShopDrawingsStatusByIdWs(Long idProject) {
		try {

			List<ShopDrawingsStatus> list = shopDrawingsStatusRepository.getListByIdProjet(idProject);
			JSONObject jo;
			JSONObject joo;
			JSONArray ja = new JSONArray();

			for (ShopDrawingsStatus e : list) {
				jo = new JSONObject();
				joo = new JSONObject();
				jo.put("name", e.getSummary());
				jo.put("value", e.getTotal());
				joo.put("ar", e.getAr());
				joo.put("st", e.getSt());
				joo.put("el", e.getEl());
				joo.put("la", e.getLa());
				joo.put("me", e.getMe());
				joo.put("gn", e.getGn());
				joo.put("pourcentage", e.getPourcentage());
				jo.put("extra", joo);
				ja.put(jo);
			}

			return utilsWs.resultWs(ConstanteWs._CODE_WS_SUCCESS, ja);
		} catch (Exception e) {
			logger.error(
					"Error ShopDrawingsStatusService in method getListShopDrawingsStatusByIdWs() :: " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

}
