package tn.com.guru.project.administration.service;

import java.util.List;

import tn.com.guru.project.tools.model.SendObject;
import tn.com.guru.project.view.model.VLogData;

public interface IVLogDataService {

	public List<VLogData> getList();

	public VLogData findById(Long id);

	public VLogData saveOrUpdate(VLogData entity);

	public Boolean deleteById(Long id);

	public SendObject findVLogDataByIdWs(Long id);

	public SendObject getListVLogDataWs();

	public SendObject saveOrUpdateVLogDataWs(VLogData entity);

	public SendObject deleteVLogDataByIdWs(Long id);

}
