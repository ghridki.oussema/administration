package tn.com.guru.project.administration.service;

import java.util.List;

import tn.com.guru.project.administration.model.DirectCosts;
import tn.com.guru.project.tools.model.SendObject;

public interface IDirectCostsService {

	public List<DirectCosts> getList();

	public DirectCosts findById(Long id);

	public DirectCosts saveOrUpdate(DirectCosts entity);

	public Boolean deleteById(Long id);

	public SendObject findDirectCostsByIdWs(Long id);

	public SendObject getListDirectCostsWs();

	public SendObject saveOrUpdateDirectCostsWs(DirectCosts entity);

	public SendObject deleteDirectCostsByIdWs(Long id);
	
	public SendObject findDirectCostsByIdProjetWs(Long idProjet);

}
