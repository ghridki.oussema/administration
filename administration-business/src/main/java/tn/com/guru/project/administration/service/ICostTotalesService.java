package tn.com.guru.project.administration.service;

import java.util.List;

import tn.com.guru.project.administration.model.CostTotales;
import tn.com.guru.project.tools.model.SendObject;

public interface ICostTotalesService {

	public List<CostTotales> getList();

	public CostTotales findById(Long id);

	public CostTotales saveOrUpdate(CostTotales entity);

	public Boolean deleteById(Long id);

	public SendObject findCostTotalesByIdWs(Long id);

	public SendObject getListCostTotalesWs();

	public SendObject deleteCostTotalesByIdWs(Long id);

	public SendObject saveOrUpdateCostTotalesWs(CostTotales entity);
	
	public SendObject findCostTotalesByIdProjetWs(Long idProjet);

}
