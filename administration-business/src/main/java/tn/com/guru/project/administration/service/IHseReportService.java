package tn.com.guru.project.administration.service;

import java.util.List;

import tn.com.guru.project.administration.model.HseReport;
import tn.com.guru.project.tools.model.SendObject;

public interface IHseReportService {

	public List<HseReport> getList();

	public HseReport findById(Long id);

	public HseReport saveOrUpdate(HseReport entity);

	public Boolean deleteById(Long id);

	public SendObject findHseReportByIdWs(Long id);

	public SendObject getListHseReportWs();

	public SendObject saveOrUpdateHseReportWs(List<HseReport> entity,Long idProject);

	public SendObject deleteHseReportByIdWs(Long id);
	
	public SendObject getListHseReporByIdtWs(Long idProject);

}
