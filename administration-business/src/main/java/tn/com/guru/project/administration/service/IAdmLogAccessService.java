package tn.com.guru.project.administration.service;

import java.util.List;

import tn.com.guru.project.administration.model.AdmLogAccess;
import tn.com.guru.project.tools.model.SendObject;

public interface IAdmLogAccessService {

	public List<AdmLogAccess> getList();

	public AdmLogAccess findById(Long id);

	public AdmLogAccess saveOrUpdate(AdmLogAccess entity);

	public Boolean deleteById(Long id);

	public SendObject findAdmLogAccessByIdWs(Long id);

	public SendObject getListAdmLogAccessWs();

	public SendObject saveOrUpdateAdmLogAccessWs(AdmLogAccess entity);

	public SendObject deleteAdmLogAccessByIdWs(Long id);

}
