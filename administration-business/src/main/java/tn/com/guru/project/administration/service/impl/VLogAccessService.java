package tn.com.guru.project.administration.service.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tn.com.guru.project.administration.repository.IVLogAccessRepository;
import tn.com.guru.project.administration.service.IVLogAccessService;
import tn.com.guru.project.common.service.ICommonService;
import tn.com.guru.project.tools.ConstanteService;
import tn.com.guru.project.tools.ConstanteWs;
import tn.com.guru.project.tools.UtilsWs;
import tn.com.guru.project.tools.model.SendObject;
import tn.com.guru.project.view.model.VLogAccess;


@Service
public class VLogAccessService implements IVLogAccessService {

	private static final Logger logger = LogManager.getLogger(VLogAccessService.class);

	@Autowired
	private IVLogAccessRepository vLogAccessRepository;

	@Autowired
	private ICommonService commonService;

	@Autowired
	private UtilsWs utilsWs;

	@Override
	public List<VLogAccess> getList() {
		try {
			return vLogAccessRepository.findAll();
		} catch (Exception e) {
			logger.error("Error VLogAccessService in method getList :: " + e.toString());
			return null;
		}
	}

	@Override
	public VLogAccess findById(Long id) {
		try {
			SendObject sendObject = commonService.getObjectById(new VLogAccess(), id.toString(), false);
			if (sendObject.getCode().equals(ConstanteService._CODE_SERVICE_SUCCESS))
				return (VLogAccess) sendObject.getPayload();
			return null;
		} catch (Exception e) {
			logger.error("Error VLogAccessService in method findById :: " + e.toString());
			return null;
		}
	}

	@Override
	public VLogAccess saveOrUpdate(VLogAccess entity) {
		try {
			return vLogAccessRepository.save(entity);
		} catch (Exception e) {
			logger.error("Error VLogAccessService in method saveOrUpdate :: " + e.toString());
			return null;
		}
	}

	@Override
	public Boolean deleteById(Long id) {
		try {
			if (id == null)
				return false;
			vLogAccessRepository.delete(this.findById(id));
			return true;
		} catch (Exception e) {
			logger.error("Error VLogAccessService in method deleteById :: " + e.toString());
			return false;
		}
	}

	@Override
	public SendObject findVLogAccessByIdWs(Long id) {
		try {
			if (id == null)
				return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_ALIAS_PARAM, new JSONObject());
			VLogAccess entity = this.findById(id);
			if (entity == null)
				return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_NOT_EXISTS_ROW_DATA_BASE, new JSONObject());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_SUCCESS, new JSONObject(entity));
		} catch (Exception e) {
			logger.error("Error VLogAccessService in method findVLogAccessByIdWs :: " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	@Override
	public SendObject getListVLogAccessWs() {
		try {
			return utilsWs.resultWs(ConstanteWs._CODE_WS_SUCCESS, new JSONArray(this.getList()));
		} catch (Exception e) {
			logger.error("Error VLogAccessService in method getListVLogAccessWs() :: " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	@Override
	public SendObject saveOrUpdateVLogAccessWs(VLogAccess entity) {
		try {
			entity = this.saveOrUpdate(entity);
			if (entity == null)
				return new SendObject(ConstanteService._CODE_SERVICE_ERROR_SAVE_OR_UPDATE);
			return new SendObject(ConstanteService._CODE_SERVICE_SUCCESS, entity);
		} catch (Exception e) {
			logger.error("Error VLogAccessService in method saveOrUpdateVLogAccessWs :: " + e.toString());
			return new SendObject(ConstanteService._CODE_SERVICE_ERROR);
		}
	}

	@Override
	public SendObject deleteVLogAccessByIdWs(Long id) {
		try {
			Boolean resultDelete = this.deleteById(id);
			if (resultDelete == false)
				return new SendObject(ConstanteService._CODE_SERVICE_ERROR_DELETE_ROW);
			return new SendObject(ConstanteService._CODE_SERVICE_SUCCESS);
		} catch (Exception e) {
			logger.error("Error VLogAccessService in method getListVLogAccess " + e.toString());
			return new SendObject(ConstanteService._CODE_SERVICE_ERROR);
		}
	}

}
