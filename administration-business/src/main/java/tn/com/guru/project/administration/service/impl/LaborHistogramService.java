package tn.com.guru.project.administration.service.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tn.com.guru.project.administration.model.LaborHistogram;
import tn.com.guru.project.administration.model.RiskRegister;
import tn.com.guru.project.administration.repository.ILaborHistogramRepository;
import tn.com.guru.project.administration.service.ILaborHistogramService;
import tn.com.guru.project.common.service.ICommonService;
import tn.com.guru.project.tools.ConstanteService;
import tn.com.guru.project.tools.ConstanteWs;
import tn.com.guru.project.tools.UtilsWs;
import tn.com.guru.project.tools.model.SendObject;

@Service
public class LaborHistogramService implements ILaborHistogramService {

	private static final Logger logger = LogManager.getLogger(LaborHistogramService.class);

	@Autowired
	private ILaborHistogramRepository laborHistogramRepository;

	@Autowired
	private ICommonService commonService;

	@Autowired
	private UtilsWs utilsWs;
	
	@Override
	public SendObject saveOrUpdateListOFLaborHistogramws(List<LaborHistogram> listOfentity) {
		try {

			if (!listOfentity.isEmpty()) {
				laborHistogramRepository.deleteByIdProjet(listOfentity.get(0).getIdProjet());

				for (LaborHistogram aa : listOfentity) {

					this.saveOrUpdate(aa);

				}
			}
			if (listOfentity == null)
				return utilsWs.resultWs(ConstanteService._CODE_SERVICE_ERROR_SAVE_OR_UPDATE, new JSONArray(listOfentity));
			return utilsWs.resultWs(ConstanteService._CODE_SERVICE_SUCCESS, new JSONObject());
		} catch (Exception e) {
			logger.error("Error RiskRegisterService in method ssaveOrUpdateListOFLaborHistogramws :: " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	@Override
	public List<LaborHistogram> getList() {
		try {
			return laborHistogramRepository.findAll();
		} catch (Exception e) {
			logger.error("Error LaborHistogramService in method getList :: " + e.toString());
			return null;
		}
	}

	@Override
	public LaborHistogram findById(Long id) {
		try {
			SendObject sendObject = commonService.getObjectById(new LaborHistogram(), id.toString(), false);
			if (sendObject.getCode().equals(ConstanteService._CODE_SERVICE_SUCCESS))
				return (LaborHistogram) sendObject.getPayload();
			return null;
		} catch (Exception e) {
			logger.error("Error LaborHistogramService in method findById :: " + e.toString());
			return null;
		}
	}

	@Override
	public LaborHistogram saveOrUpdate(LaborHistogram entity) {
		try {
			return laborHistogramRepository.save(entity);
		} catch (Exception e) {
			logger.error("Error LaborHistogramService in method saveOrUpdate :: " + e.toString());
			return null;
		}
	}

	@Override
	public Boolean deleteById(Long id) {
		try {
			if (id == null)
				return false;
			laborHistogramRepository.delete(this.findById(id));
			return true;
		} catch (Exception e) {
			logger.error("Error LaborHistogramService in method deleteById :: " + e.toString());
			return false;
		}
	}

	@Override
	public SendObject findLaborHistogramByIdWs(Long id) {
		try {
			if (id == null)
				return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_ALIAS_PARAM, new JSONObject());
			LaborHistogram entity = this.findById(id);
			if (entity == null)
				return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_NOT_EXISTS_ROW_DATA_BASE, new JSONObject());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_SUCCESS, new JSONObject(entity));
		} catch (Exception e) {
			logger.error("Error LaborHistogramService in method findLaborHistogramByIdWs :: " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	@Override
	public SendObject getListLaborHistogramWs() {
		try {
			return utilsWs.resultWs(ConstanteWs._CODE_WS_SUCCESS, new JSONArray(this.getList()));
		} catch (Exception e) {
			logger.error("Error LaborHistogramService in method getListLaborHistogramWs() :: " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	@Override
	public SendObject saveOrUpdateLaborHistogramWs(LaborHistogram entity) {
		try {
			entity = this.saveOrUpdate(entity);
			if (entity == null)
				return utilsWs.resultWs(ConstanteService._CODE_SERVICE_ERROR_SAVE_OR_UPDATE, new JSONObject());
			return utilsWs.resultWs(ConstanteService._CODE_SERVICE_SUCCESS, new JSONObject(entity));
		} catch (Exception e) {
			logger.error("Error LaborHistogramService in method saveOrUpdateLaborHistogramWs :: " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

	@Override
	public SendObject deleteLaborHistogramByIdWs(Long id) {
		try {
			Boolean resultDelete = this.deleteById(id);
			if (resultDelete == false)
				return utilsWs.resultWs(ConstanteService._CODE_SERVICE_ERROR_DELETE_ROW, new JSONObject());
			return utilsWs.resultWs(ConstanteService._CODE_SERVICE_SUCCESS, new JSONObject());
		} catch (Exception e) {
			logger.error("Error LaborHistogramService in method getListLaborHistogram " + e.toString());
			return utilsWs.resultWs(ConstanteWs._CODE_WS_ERROR_IN_METHOD, new JSONObject());
		}
	}

}
