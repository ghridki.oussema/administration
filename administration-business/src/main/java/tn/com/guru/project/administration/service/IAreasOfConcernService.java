package tn.com.guru.project.administration.service;

import java.util.List;

import tn.com.guru.project.administration.model.AreasOfConcern;
import tn.com.guru.project.tools.model.SendObject;

public interface IAreasOfConcernService {

	public List<AreasOfConcern> getList();

	public AreasOfConcern findById(Long id);

	public AreasOfConcern saveOrUpdate(AreasOfConcern entity);

	public Boolean deleteById(Long id);

	public SendObject findAreasOfConcernByIdWs(Long id);

	public SendObject getListAreasOfConcernWs();

	public SendObject saveOrUpdateAreasOfConcernWs(AreasOfConcern entity);

	public SendObject deleteAreasOfConcernByIdWs(Long id);
}
