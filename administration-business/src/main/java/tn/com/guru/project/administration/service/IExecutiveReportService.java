package tn.com.guru.project.administration.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import tn.com.guru.project.administration.model.ExecutiveReport;
import tn.com.guru.project.tools.model.SendObject;

public interface IExecutiveReportService {

	public List<ExecutiveReport> getList();

	public ExecutiveReport findById(Long id);

	public ExecutiveReport saveOrUpdate(ExecutiveReport entity);

	public Boolean deleteById(Long id);

	public SendObject findExecutiveReportByIdWs(Long id);

	public SendObject getListExecutiveReportWs();

	public SendObject saveOrUpdateExecutiveReportWs(ExecutiveReport entity, List<MultipartFile> filedata);

	public SendObject deleteExecutiveReportByIdWs(Long id);
	
	public SendObject getListExecutiveReporListtWs(Long idProject);
}
