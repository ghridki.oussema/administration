package tn.com.guru.project.administration.service;

import java.util.List;

import tn.com.guru.project.administration.model.AdmUtilisateurProfil;
import tn.com.guru.project.tools.model.SendObject;

public interface IAdmUtilisateurProfilService {

	public List<AdmUtilisateurProfil> getList();

	public AdmUtilisateurProfil findById(Long id);

	public AdmUtilisateurProfil saveOrUpdate(AdmUtilisateurProfil entity);

	public Boolean deleteById(Long id);

	public SendObject findAdmUtilisateurProfilByIdWs(Long id);

	public SendObject getListAdmUtilisateurProfilWs();

	public SendObject saveOrUpdateAdmUtilisateurProfilWs(AdmUtilisateurProfil entity);

	public SendObject deleteAdmUtilisateurProfilByIdWs(Long id);

	public List<AdmUtilisateurProfil> getListUserProfilesByIdUser(Long iduser);

}
