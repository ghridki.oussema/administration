package tn.com.guru.project.administration.service;

import java.util.List;

import tn.com.guru.project.administration.model.AdmProfil;
import tn.com.guru.project.tools.model.SendObject;

public interface IAdmProfilService {

	public List<AdmProfil> getList();

	public AdmProfil findById(Long id);

	public AdmProfil saveOrUpdate(AdmProfil entity);

	public Boolean deleteById(Long id);

	public SendObject findAdmProfilByIdWs(Long id);

	public SendObject getListAdmProfilWs();

	public SendObject saveOrUpdateAdmProfilWs(AdmProfil entity);

	public SendObject deleteAdmProfilByIdWs(Long id);
	
	public List<AdmProfil> getListProfilesByIdUser(Long idUser);
	
	public SendObject updateAdmProfilWs(AdmProfil entity);

	public SendObject getAdmProfilPaginated(Long lim, Long page);
}
