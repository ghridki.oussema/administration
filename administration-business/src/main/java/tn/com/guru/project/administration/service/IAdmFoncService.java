package tn.com.guru.project.administration.service;

import java.util.List;

import tn.com.guru.project.administration.model.AdmFonc;
import tn.com.guru.project.tools.model.SendObject;

public interface IAdmFoncService {

	public List<AdmFonc> getList();

	public AdmFonc findById(Long id);

	public AdmFonc saveOrUpdate(AdmFonc entity);

	public Boolean deleteById(Long id);

	public SendObject findAdmFoncByIdWs(Long id);

	public SendObject getListAdmFoncWs();

	public SendObject saveOrUpdateAdmFoncWs(AdmFonc entity);

	public SendObject deleteAdmFoncByIdWs(Long id);
	
	public SendObject getMenuByIdUser(Long idUser, String lang);
	
	public List<AdmFonc> getMenuByIdProfil(Long id);
	
	public List<AdmFonc> getChildMenu(Long idParent);
	
	public SendObject getMenu();

}
