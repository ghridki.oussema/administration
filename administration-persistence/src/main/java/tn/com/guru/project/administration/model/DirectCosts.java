package tn.com.guru.project.administration.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "direct_costs", schema = "application")

public class DirectCosts implements java.io.Serializable, Cloneable {
	private transient static final long serialVersionUID = 1L;

	@SequenceGenerator(allocationSize = 1, initialValue = 1, sequenceName = "application.seq_direct_costs", name = "application.seq_direct_costs")
	@GeneratedValue(generator = "application.seq_direct_costs", strategy = GenerationType.SEQUENCE)
	@Id
	@Column(name = "id", unique = true, nullable = false, precision = 22, scale = 0)
	private Long id;

	@Column(name = "id_projet")
	private Long idProjet;

	@Column(name = "Task_id")
	private String taskID;

	@Column(name = "cost_description")
	private String costDescription;

	@Column(name = "type")
	private String type;

	@Column(name = "Amount")
	private String amount;

	@Column(name = "cost_account")
	private String costAccount;

	@Column(name = "delete")
	private String delete;

}