package tn.com.guru.project.administration.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

@Entity
@Table(name = "risk_register", schema = "application")

public class RiskRegister implements java.io.Serializable, Cloneable {
	private transient static final long serialVersionUID = 1L;

	@SequenceGenerator(allocationSize = 1, initialValue = 1, sequenceName = "application.risk_register_seq", name = "application.risk_register_seq")
	@GeneratedValue(generator = "application.risk_register_seq", strategy = GenerationType.SEQUENCE)
	@Id

	@Column(name = "id_risk_register", unique = true, nullable = false)
	private Long idRiskRegister;

	@Column(name = "risk_id")
	private String riskid;

	@Column(name = "risk_title")
	private String risktitle;

	@Column(name = "project_name")
	private String projectname;

	@Column(name = "risk_owner")
	private String riskowner;

	@Column(name = "risk_statut")
	private String riskstatus;

	@Column(name = "causes_description")
	private String causesdescription;

	@Column(name = "risk_event_description")
	private String riskeventdescription;

	@Column(name = "impact_description")
	private String impactdescription;

	@Column(name = "current_risk_scoring")
	private String currentriskscoring;

	@Column(name = "target_risk_scoring")
	private String targetriskscoring;

	@Column(name = "response_title")
	private String responsetitle;

	@Column(name = "response_owner")
	private String responseowner;

	@Column(name = "response_status")
	private String responsestatus;

	@Column(name = "start_date", length = 13)
	private Date startdate;

	@Column(name = "due_date", length = 13)
	private Date duedate;

	@Column(name = "completion_date", length = 13)
	private Date completiondate;

	@Column(name = "id_projet")
	private Long idProjet;

	@Column(name = "created_at", length = 29)
	private Date createdAt;

	public RiskRegister clone() throws CloneNotSupportedException {
		return (RiskRegister) super.clone();
	}
}
