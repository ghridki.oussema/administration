package tn.com.guru.project.administration.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RefDocument {

	private String nodeRef;
	private String fileName;
	private String base;
}
