package tn.com.guru.project.administration.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

@Entity
@Table(name = "adm_personnel", schema = "administration")

public class AdmPersonnel implements java.io.Serializable, Cloneable {
	private transient static final long serialVersionUID = 1L;

	@SequenceGenerator(allocationSize = 1, initialValue = 1, sequenceName = "administration.seq_adm_personnel", name = "administration.seq_adm_personnel")
	@GeneratedValue(generator = "administration.seq_adm_personnel", strategy = GenerationType.SEQUENCE)
	@Id

	@Column(name = "id", unique = true, nullable = false, precision = 22, scale = 0)
	private Long id;

	@Column(name = "sexe", nullable = false)
	private Integer sexe;

	@Column(name = "prenom", nullable = false, length = 50)
	private String prenom;

	@Column(name = "nom", nullable = false, length = 50)
	private String nom;

	@Column(name = "dt_naissance", nullable = false, length = 13)
	private Date dtNaissance;

	@Column(name = "matricule", length = 50)
	private String matricule;

	@Column(name = "mail", length = 50)
	private String mail;

	@Column(name = "num_tel", precision = 22, scale = 0)
	private Long numTel;
	
	@Column(name = "cin", precision = 8, scale = 0)
	private String cin; 

	@Column(name = "dt_maj", nullable = false, length = 13)
	private Date dtMaj;

	public AdmPersonnel clone() throws CloneNotSupportedException {
		return (AdmPersonnel) super.clone();
	}
}
