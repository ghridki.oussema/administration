package tn.com.guru.project.administration.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import tn.com.guru.project.administration.model.MaterialSubmittalStatus;

@Repository
public interface IMaterialSubmittalStatusRepository extends JpaRepository<MaterialSubmittalStatus, Long> {

	@Transactional
	@Modifying
	@Query(value = "delete from  application.material_submittal_status where id_projet=:idProjet", nativeQuery = true)
	void deleteByIdProjet(@Param(value = "idProjet") Long idProjet);

	@Query(value = "select a from  MaterialSubmittalStatus a where a.idProjet=:idProjet and a.code!=null ")
	List<MaterialSubmittalStatus> getListByIdProjet(@Param(value = "idProjet") Long idProjet);

}
