package tn.com.guru.project.administration.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import tn.com.guru.project.administration.model.AdmFoncProfil;

@Repository
public interface IAdmFoncProfilRepository extends JpaRepository<AdmFoncProfil, Long> {

	@Query("select a from AdmFoncProfil a where a.idAdmProfil=:idProfil")

	List<AdmFoncProfil> getListAdmFoncProfilByIdProfil(@Param(value = "idProfil") Long idProfil);
}
