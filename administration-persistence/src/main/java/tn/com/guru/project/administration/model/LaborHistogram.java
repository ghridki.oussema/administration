package tn.com.guru.project.administration.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

@Entity
@Table(name = "labor_histogram", schema = "application")

public class LaborHistogram implements java.io.Serializable, Cloneable {
	private transient static final long serialVersionUID = 1L;

	@SequenceGenerator(allocationSize = 1, initialValue = 1, sequenceName = "application.seq_labor_histogram", name = "application.seq_labor_histogram")
	@GeneratedValue(generator = "application.seq_labor_histogram", strategy = GenerationType.SEQUENCE)
	@Id

	@Column(name = "id_labor_histogram", unique = true, nullable = false)
	private Long idLaborHistogram;

	@Column(name = "days")
	private String days;

	@Column(name = "budgeted_human")
	private String budgetedhuman;

	@Column(name = "budgeted_equipment")
	private String budgetedequipment;

	@Column(name = "no_of_labors_planned")
	private String nooflaborsplanned;

	@Column(name = "created_at", length = 29)
	private Date createdAt;
	
	@Column(name = "id_projet")
	private Long idProjet;

	public LaborHistogram clone() throws CloneNotSupportedException {
		return (LaborHistogram) super.clone();
	}
}
