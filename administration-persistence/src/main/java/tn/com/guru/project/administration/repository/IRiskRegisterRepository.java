package tn.com.guru.project.administration.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import tn.com.guru.project.administration.model.RiskRegister;

@Repository
public interface IRiskRegisterRepository extends JpaRepository<RiskRegister, Long> {

	@Transactional
	@Modifying
	@Query(value = "delete from  application.risk_register where id_projet=:idProjet", nativeQuery = true)
	void deleteByIdProjet(@Param(value = "idProjet") Long idProjet);
}
