package tn.com.guru.project.administration.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

@Entity
@Table(name = "adm_utilisateur", schema = "administration")

public class AdmUtilisateur implements java.io.Serializable, Cloneable {
	private transient static final long serialVersionUID = 1L;

	@SequenceGenerator(allocationSize = 1, initialValue = 1, sequenceName = "administration.seq_adm_utilisateur", name = "administration.seq_adm_utilisateur")
	@GeneratedValue(generator = "administration.seq_adm_utilisateur", strategy = GenerationType.SEQUENCE)
	@Id

	@Column(name = "id", unique = true, nullable = false, precision = 22, scale = 0)
	private Long id;
	
	@Column(name = "id_adm_personnel")
	private Long idAdmPersonnel;

	@Column(name = "login", length = 150)
	private String login;

	@Column(name = "password", length = 150)
	private String password;

	@Column(name = "dt_ajout", nullable = false, length = 13)
	private Date dtAjout;

	@Column(name = "dt_maj", length = 13)
	private Date dtMaj;

	@Column(name = "is_actif", nullable = false)
	private Boolean isActif;
	
	@Column(name = "is_modif", nullable = false)
	private Boolean isModif;

	@Transient
	private String confirmPassword;
	
	@Transient
	private AdmPersonnel admPersonnel ;
	
	@Transient
	private List<AdmUtilisateurProfil> listProfiles ;

	public AdmUtilisateur clone() throws CloneNotSupportedException {
		return (AdmUtilisateur) super.clone();
	}
}
