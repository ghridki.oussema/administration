package tn.com.guru.project.administration.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "cost_totales", schema = "application")
public class CostTotales implements java.io.Serializable, Cloneable {
	private transient static final long serialVersionUID = 1L;

	@SequenceGenerator(allocationSize = 1, initialValue = 1, sequenceName = "application.seq_cost_totales", name = "application.seq_cost_totales")
	@GeneratedValue(generator = "application.seq_cost_totales", strategy = GenerationType.SEQUENCE)
	@Id
	@Column(name = "id", unique = true, nullable = false, precision = 22, scale = 0)
	private Long id;

	@Column(name = "id_projet")
	private Long idProjet;

	@Column(name = "task")
	private String task;

	@Column(name = "budgeted_human")
	private String budgetedHuman;

	@Column(name = "budgeted_equipment")
	private String budgetedEquipment;

	@Column(name = "budgeted_material")
	private String budgetedMaterial;

	@Column(name = "budgeted_location", unique = true, nullable = false)
	private String budgetedLocation;

	@Column(name = "budgeted_mitigated_risks")
	private String budgetedMitigatedRisks;

	@Column(name = "budgeted_unmitigated_risks")
	private String budgetedUnmitigatedRisks;

	@Column(name = "budgeted_direct")
	private String budgetedDirect;

	@Column(name = "budgeted_total", unique = true, nullable = false)
	private String budgetedTotal;

	@Column(name = "actual_human")
	private String actualHuman;

	@Column(name = "actual_equipment")
	private String actualEquipment;

	@Column(name = "actual_material")
	private String actualMaterial;

	@Column(name = "actual_location", unique = true, nullable = false)
	private String actualLocation;

	@Column(name = "actual_reports")
	private String actualReports;

	@Column(name = "actual_total")
	private String actualTotal;

}
