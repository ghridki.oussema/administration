package tn.com.guru.project.administration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import tn.com.guru.project.view.model.VLogAccess;

@Repository
public interface IVLogAccessRepository extends JpaRepository<VLogAccess, Long> {

}
