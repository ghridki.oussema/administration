package tn.com.guru.project.administration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import tn.com.guru.project.administration.model.AreasOfConcern;

@Repository
public interface IAreasOfConcernDao extends JpaRepository<AreasOfConcern, Long> {

    @Query(value = "select coalesce(max(a.seq), 0)  + 1 from application.areas_of_concern a where a.id_project = :idProjet", nativeQuery = true)
    Integer getSeq(Long idProjet);
}

