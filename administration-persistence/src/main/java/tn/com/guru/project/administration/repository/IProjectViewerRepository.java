package tn.com.guru.project.administration.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import tn.com.guru.project.administration.model.ProjectViewer;

import javax.transaction.Transactional;

@Repository
public interface IProjectViewerRepository extends JpaRepository<ProjectViewer, Long> {

	@Query("select a from ProjectViewer a where a.idProject=:idProject")
	List<ProjectViewer> getListByIdProject(@Param(value = "idProject") Long idProject);

	@Query("select a.urn from ProjectViewer a where a.idProject=:projectId")
    List<String> findListDocUrnByIdProject(@Param(value = "projectId") Long projectId);


	@Transactional
	@Modifying
	@Query("delete from ProjectViewer where idProject=:projectId")
    void deleteAllByIdPojet(@Param(value = "projectId") Long projectId);
}
