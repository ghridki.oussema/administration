package tn.com.guru.project.administration.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import tn.com.guru.project.administration.model.CostTotales;

@Repository
public interface ICostTotalesRepository extends JpaRepository<CostTotales, Long> {

	@Procedure(value = "application.do_copy_cost_totales")
	public String copyCostTotales(@Param("id_projet") Long idProjet);

	public List<CostTotales> findByIdProjet(Long idProjet);
}
