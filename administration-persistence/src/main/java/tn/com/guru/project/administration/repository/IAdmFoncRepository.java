package tn.com.guru.project.administration.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import tn.com.guru.project.administration.model.AdmFonc;

@Repository
public interface IAdmFoncRepository extends JpaRepository<AdmFonc, Long> {

	@Query(value = "select * from administration.adm_fonc a order by a.code", nativeQuery = true)
	List<AdmFonc> getMenuParentOrderByCde();

	@Query(value = "select * from administration.adm_fonc a where a.id_parent isnull and exists (select 1 from administration.adm_fonc_profil b where a.id = b.id_adm_fonc and b.id_adm_profil in(:idRoles)) order by a.code", nativeQuery = true)
	public List<AdmFonc> getMenuByRoles(@Param(value = "idRoles") List<Long> idRoles);

	@Query(value = "select a.id_adm_profil  from administration.adm_utilisateur_profil a where a.id_adm_utilisateur =:idUser", nativeQuery = true)
	public List<Long> getRolesByIduser(@Param(value = "idUser") Long idUser);

	@Query(value = "select * from administration.adm_fonc a where a.id_parent=:idParent order by a.code ", nativeQuery = true)
	public List<AdmFonc> getChildMenu(@Param(value = "idParent") Long idParent);

	@Query(value = "select * from administration.adm_fonc af where exists (select 1 from administration.adm_fonc_profil afp where af.id =afp.id_adm_fonc and afp.id_adm_profil=:idProfil) order by af.code ", nativeQuery = true)
	public List<AdmFonc> getMenuByIdProfil(@Param(value = "idProfil") Long idProfil);

	@Query(value = "select\r\n" + "	*\r\n" + "from\r\n" + "	administration.adm_fonc a\r\n" + "where\r\n"
			+ "	a.id_parent =:idParent\r\n" + "	and exists (\r\n" + "	select\r\n" + "		1\r\n" + "	from\r\n"
			+ "		administration.adm_fonc_profil b\r\n" + "	where\r\n" + "		a.id = b.id_adm_fonc\r\n"
			+ "		and b.id_adm_profil in(:idRoles))\r\n" + "order by\r\n" + "	a.code", nativeQuery = true)
	public List<AdmFonc> getChildMenuByRole(@Param(value = "idParent") Long idParent,
			@Param(value = "idRoles") List<Long> idRoles);

}
