package tn.com.guru.project.view.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "v_log_access", schema = "administration")
public class VLogAccess implements java.io.Serializable, Cloneable {
	private transient static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id_log_access", precision = 22, scale = 0)
	private Long idLogAccess;

	@Column(name = "code_access")
	private String codeAccess;

	@Column(name = "date_auth", length = 29)
	private Date dateAuth;

	@Column(name = "id_adm_user", precision = 22, scale = 0)
	private Long idAdmUser;

	@Column(name = "nom_user")
	private String nomUser;

	@Column(name = "ip_address")
	private String ipAddress;

	@Column(name = "login")
	private String login;

	public VLogAccess clone() throws CloneNotSupportedException {
		return (VLogAccess) super.clone();
	}
}
