package tn.com.guru.project.administration.model;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

@Entity
@Table(name = "adm_profil", schema = "administration")

public class AdmProfil implements java.io.Serializable, Cloneable {
	private transient static final long serialVersionUID = 1L;

	@SequenceGenerator(allocationSize = 1, initialValue = 1, sequenceName = "administration.seq_adm_profil", name = "administration.seq_adm_profil")
	@GeneratedValue(generator = "administration.seq_adm_profil", strategy = GenerationType.SEQUENCE)
	@Id

	@Column(name = "id", unique = true, nullable = false, precision = 22, scale = 0)
	private Long id;

	@Column(name = "code", nullable = false, length = 50)
	private String code;

	@Column(name = "libelle_ar", nullable = true, length = 150)
	private String libelleAr;

	@Column(name = "libelle_fr", nullable = true, length = 150)
	private String libelleFr;

	@Column(name = "libelle_en", nullable = true, length = 150)
	private String libelleEn;

	@Column(name = "role", length = 150)
	private String role;

	@Column(name = "flg_actif", nullable = false)
	private boolean flgActif;

	@Column(name = "dt_ajout", length = 13)
	private Timestamp dtAjout;

	@Column(name = "dt_maj", length = 13)
	private Date dtMaj;

	@Transient
	private Boolean isChecked = false;

	@Transient
	private List<AdmFonc> listAdmFonc;

	public AdmProfil clone() throws CloneNotSupportedException {
		return (AdmProfil) super.clone();
	}
}
