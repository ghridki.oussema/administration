package tn.com.guru.project.administration.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import tn.com.guru.project.administration.model.ShopDrawingsStatus;

@Repository
public interface IShopDrawingsStatusRepository extends JpaRepository<ShopDrawingsStatus, Long> {
	@Transactional
	@Modifying
	@Query(value = "delete from  application.shop_drawings_status where id_projet=:idProjet", nativeQuery = true)
	void deleteByIdProjet(@Param(value = "idProjet") Long idProjet);

	@Query(value = "select a from  ShopDrawingsStatus a where a.idProjet=:idProjet and a.code!=null")
	List<ShopDrawingsStatus> getListByIdProjet(@Param(value = "idProjet") Long idProjet);

}
