package tn.com.guru.project.administration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import tn.com.guru.project.administration.model.AdmUtilisateur;

@Repository
public interface IAdmUtilisateurRepository extends JpaRepository<AdmUtilisateur, Long> {
	
	
	
	@Query("select case when count(u) > 0 then true else false end from AdmUtilisateur u where u.login=:login ")
	public Boolean uniqueAdmUserByLogin(@Param("login") String login);

}
