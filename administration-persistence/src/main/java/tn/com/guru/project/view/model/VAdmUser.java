package tn.com.guru.project.view.model;

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "v_adm_utilisateur", schema = "administration")

public class VAdmUser {

	@Id
	@Column(name = "ID", unique = true, nullable = false, length = 255)
	private Long idAdmUser;

	@Column(name = "LOGIN", unique = true, nullable = false, length = 255)
	private String login;

	@Column(name = "PASSWORD")
	private String password;

	@Column(name = "prenom", nullable = false, length = 50)
	private String prenom;

	@Column(name = "nom", nullable = false, length = 50)
	private String nom;

	@Column(name = "dt_naissance", nullable = false, length = 13)
	private Date dtNaissance;

	@Column(name = "sexe", nullable = false)
	private Integer sexe;

	@Column(name = "sexe_str_ar", nullable = false)
	private String sexeStrAr;

	@Column(name = "sexe_str_en", nullable = false)
	private String sexeStrEn;

	@Column(name = "is_actif")
	private Boolean isActif;

	@Column(name = "is_actif_ar")
	private String isActifAr;

	@Column(name = "is_actif_en")
	private String isActifEn;

	@Column(name = "id_adm_personnel")
	private Long idAdmPersonnel;

	@Column(name = "dt_ajout")
	private Timestamp dt_ajout;

	@Column(name = "dt_maj")
	private Timestamp dt_maj;

	@Column(name = "matricule")
	private String matricule;

	@Column(name = "mail")
	private String mail;

	@Column(name = "num_tel")
	private String num_tel;

	@Column(name = "cin")
	private String cin;
}
