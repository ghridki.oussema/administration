package tn.com.guru.project.administration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import tn.com.guru.project.view.model.VLogData;

@Repository
public interface IVLogDataRepository extends JpaRepository<VLogData, Long> {

}
