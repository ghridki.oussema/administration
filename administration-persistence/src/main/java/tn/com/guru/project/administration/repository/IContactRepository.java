package tn.com.guru.project.administration.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import tn.com.guru.project.administration.model.Contact;

@Repository
public interface IContactRepository extends JpaRepository<Contact, Long> {

	@Query("select a from Contact a where a.idProject=:idProject")

	List<Contact> getListContacts(@Param(value = "idProject") Long idProfil);
}
