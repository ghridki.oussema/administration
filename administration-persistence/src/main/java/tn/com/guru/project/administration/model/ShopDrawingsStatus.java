package tn.com.guru.project.administration.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

@Entity
@Table(name = "shop_drawings_status", schema = "application")

public class ShopDrawingsStatus implements java.io.Serializable, Cloneable {
	private transient static final long serialVersionUID = 1L;

	@SequenceGenerator(allocationSize = 1, initialValue = 1, sequenceName = "application.seq_shop_drawings_status", name = "application.seq_shop_drawings_status")
	@GeneratedValue(generator = "application.seq_shop_drawings_status", strategy = GenerationType.SEQUENCE)
	@Id

	@Column(name = "id_shop_drawings_status", unique = true, nullable = false)
	private Long idShopDrawingsStatus;

	@Column(name = "code")
	private String code;

	@Column(name = "id_projet")
	private Long idProjet;

	@Column(name = "summary")
	private String summary;

	@Column(name = "st")
	private String st;

	@Column(name = "ar")
	private String ar;

	@Column(name = "me")
	private String me;

	@Column(name = "el")
	private String el;

	@Column(name = "gn")
	private String gn;

	@Column(name = "la")
	private String la;

	@Column(name = "total")
	private String total;

	@Column(name = "pourcentage")
	private String pourcentage;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	@Column(name = "created_at", length = 29)
	private Date createdAt;

	public ShopDrawingsStatus clone() throws CloneNotSupportedException {
		return (ShopDrawingsStatus) super.clone();
	}
}
