package tn.com.guru.project.view.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "v_log_data", schema = "administration")
public class VLogData implements java.io.Serializable, Cloneable {
	private transient static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "id_log_data", precision = 22, scale = 0)
	private Long idLogData;

	@Column(name = "date_log", length = 29)
	private Date dateLog;

	@Column(name = "http_method", length = 50)
	private String httpMethod;

	@Column(name = "id_adm_user", precision = 22, scale = 0)
	private Long idAdmUser;

	@Column(name = "nom_user")
	private String nomUser;

	@Column(name = "ip_address")
	private String ipAddress;

	@Column(name = "name_service")
	private String nameService;

	@Column(name = "case")
	private String case_;

	@Column(name = "uri")
	private String uri;

	public VLogData clone() throws CloneNotSupportedException {
		return (VLogData) super.clone();
	}
}
