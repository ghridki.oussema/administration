package tn.com.guru.project.administration.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

@Entity
@Table(name = "contractor_payment_statut", schema = "application")

public class ContractorPaymentStatut implements java.io.Serializable, Cloneable {
	private transient static final long serialVersionUID = 1L;

	@SequenceGenerator(allocationSize = 1, initialValue = 1, sequenceName = "application.seq_contractor_payment_statut", name = "application.seq_contractor_payment_statut")
	@GeneratedValue(generator = "application.seq_contractor_payment_statut", strategy = GenerationType.SEQUENCE)
	@Id

	@Column(name = "id", unique = true, nullable = false)
	private Long id;

	@Column(name = "id_project", unique = true, nullable = false)
	private Long idProject;

	@Column(name = "code")
	private String code;

	@Column(name = "status")
	private String status;

	@Column(name = "sn")
	private String sn;

	@Column(name = "curency")
	private String curency;

	
	@Column(name = "submitted_to_consultant")
	private String submittedtoconsultant;

	@Column(name = "amount_submitted_to_consultant")
	private String amountsubmittedtoconsultant;

	
	@Column(name = "submitted_to_client")
	private String submittedtoclient;

	@Column(name = "amount_submitted_to_client")
	private String amountsubmittedtoclient;

	@Column(name = "amount_approved_by_client")
	private String amountapprovedbyclient;

	@Column(name = "amount_processed_by_client")
	private String amountprocessedbyclient;

	
	@Column(name = "approved_by_client")
	private String approvedbyclient;

	
	@Column(name = "processed_by_client")
	private String processedbyclient;

	public ContractorPaymentStatut clone() throws CloneNotSupportedException {
		return (ContractorPaymentStatut) super.clone();
	}
}
