package tn.com.guru.project.administration.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

@Entity
@Table(name = "adm_utilisateur_profil", schema = "administration")

public class AdmUtilisateurProfil implements java.io.Serializable, Cloneable {
	private transient static final long serialVersionUID = 1L;

	@SequenceGenerator(allocationSize = 1, initialValue = 1, sequenceName = "administration.seq_adm_utilisateur_profil", name = "administration.seq_adm_utilisateur_profil")
	@GeneratedValue(generator = "administration.seq_adm_utilisateur_profil", strategy = GenerationType.SEQUENCE)
	@Id

	@Column(name = "id", unique = true, nullable = false, precision = 22, scale = 0)
	private Long id;
	@Column(name = "id_adm_profil", nullable = false)
	private Long idAdmProfil;
	@Column(name = "id_adm_utilisateur", nullable = false)
	private Long idAdmUtilisateur;

	@Column(name = "dt_maj", nullable = false, length = 13)
	private Date dtMaj;

	public AdmUtilisateurProfil clone() throws CloneNotSupportedException {
		return (AdmUtilisateurProfil) super.clone();
	}
}
