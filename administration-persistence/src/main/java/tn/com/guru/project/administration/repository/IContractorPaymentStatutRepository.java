package tn.com.guru.project.administration.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import tn.com.guru.project.administration.model.ContractorPaymentStatut;

@Repository
public interface IContractorPaymentStatutRepository extends JpaRepository<ContractorPaymentStatut, Long> {

	@Transactional
	@Modifying
	@Query(value = "delete from  application.contractor_payment_statut where id_project=:idProjet and code=:code", nativeQuery = true)
	void deleteByIdProjet(@Param(value = "idProjet") Long idProjet, @Param(value = "code") String code);

	@Query(value = "select a from  ContractorPaymentStatut a where a.idProject=:idProjet")
	List<ContractorPaymentStatut> getListByIdProjet(@Param(value = "idProjet") Long idProjet);
}
