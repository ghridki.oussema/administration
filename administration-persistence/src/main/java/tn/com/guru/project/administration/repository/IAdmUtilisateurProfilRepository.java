package tn.com.guru.project.administration.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import tn.com.guru.project.administration.model.AdmUtilisateurProfil;

@Repository
public interface IAdmUtilisateurProfilRepository extends JpaRepository<AdmUtilisateurProfil, Long> {

	@Query(value = "select * from administration.adm_utilisateur_profil a where a.id_adm_utilisateur =:idUser", nativeQuery = true)
	List<AdmUtilisateurProfil> getListUserProfilesByIdUser(@Param("idUser") Long idUser);

}
