package tn.com.guru.project.administration.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.json.JSONArray;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

@Entity
@Table(name = "executive_report", schema = "application")

public class ExecutiveReport implements java.io.Serializable, Cloneable {

	private transient static final long serialVersionUID = 1L;

	@SequenceGenerator(allocationSize = 1, initialValue = 1, sequenceName = "application.seq_executive_report", name = "application.seq_executive_report")
	@GeneratedValue(generator = "application.seq_executive_report", strategy = GenerationType.SEQUENCE)
	@Id

	@Column(name = "id", unique = true, nullable = false)
	private Long id;

	@Column(name = "report")
	private String report;

	@Column(name = "observation", length = 150)
	private String observation;

	@Column(name = "node_ref")
	private String nodeRef;

	@Column(name = "id_project", nullable = false)
	private Long idProjet;

	@Transient
	private JSONArray dataMedia;

	public ExecutiveReport clone() throws CloneNotSupportedException {
		return (ExecutiveReport) super.clone();
	}
}