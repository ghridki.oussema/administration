package tn.com.guru.project.administration.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

@Entity
@Table(name = "areas_of_concern", schema = "application")

public class AreasOfConcern implements java.io.Serializable, Cloneable {

	private transient static final long serialVersionUID = 1L;

	@SequenceGenerator(allocationSize = 1, initialValue = 1, sequenceName = "application.seq_areas_of_concern", name = "application.seq_areas_of_concern")
	@GeneratedValue(generator = "application.seq_areas_of_concern", strategy = GenerationType.SEQUENCE)
	@Id

	@Column(name = "id", unique = true, nullable = false)
	private Long id;

	@Column(name = "seq")
	private Integer seq;

	@Column(name = "description")
	private String description;

	@Column(name = "id_project", nullable = false)
	private Long idProjet;

	@Column(name = "action")
	private String action;

	@Column(name = "sevirite")
	private String sevirite;

	@Column(name = "status")
	private String status;

	public AreasOfConcern clone() throws CloneNotSupportedException {
		return (AreasOfConcern) super.clone();
	}
}
