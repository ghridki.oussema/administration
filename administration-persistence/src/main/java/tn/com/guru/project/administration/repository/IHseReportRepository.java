package tn.com.guru.project.administration.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import tn.com.guru.project.administration.model.HseReport;

@Repository
public interface IHseReportRepository extends JpaRepository<HseReport, Long> {

	@Transactional
	@Modifying
	@Query(value = "delete from  application.hse_report where id_projet=:idProjet", nativeQuery = true)
	void deleteByIdProjet(@Param(value = "idProjet") Long idProjet);

	@Query(value = "select a from  HseReport a where a.idProjet=:idProjet order by sn")
	List<HseReport> getListByIdProjet(@Param(value = "idProjet") Long idProjet);

}
