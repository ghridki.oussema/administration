package tn.com.guru.project.administration.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import tn.com.guru.project.administration.model.ExecutiveReport;

@Repository
public interface IExecutiveReportRepository extends JpaRepository<ExecutiveReport, Long> {

	@Query(value = "select a from ExecutiveReport a where a.idProjet=:idProjet")
	List<ExecutiveReport> getExecutiveReportByIdProject(@Param(value = "idProjet") Long idProjet);

}
