package tn.com.guru.project.administration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import tn.com.guru.project.administration.model.AdmLogAccess;

@Repository
public interface IAdmLogAccessRepository extends JpaRepository<AdmLogAccess, Long> {

}
