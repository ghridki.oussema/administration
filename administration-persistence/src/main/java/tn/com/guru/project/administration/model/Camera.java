package tn.com.guru.project.administration.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

@Entity
@Table(name = "camera", schema = "application")

public class Camera implements java.io.Serializable, Cloneable {

	private transient static final long serialVersionUID = 1L;

	@SequenceGenerator(allocationSize = 1, initialValue = 1, sequenceName = "application.seq_camera", name = "application.seq_camera")
	@GeneratedValue(generator = "application.seq_camera", strategy = GenerationType.SEQUENCE)
	@Id

	@Column(name = "id", unique = true, nullable = false)
	private Long id;

	@Column(name = "code")
	private String code;

	@Column(name = "name", nullable = false, length = 150)
	private String name;

	@Column(name = "id_project", nullable = false)
	private Long idProjet;

	@Column(name = "url", nullable = false, length = 150)
	private String url;

	public Camera clone() throws CloneNotSupportedException {
		return (Camera) super.clone();
	}
}
