package tn.com.guru.project.administration.model;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

@Entity
@Table(name = "adm_fonc", schema = "administration")

public class AdmFonc implements java.io.Serializable, Cloneable {
	private transient static final long serialVersionUID = 1L;

	@SequenceGenerator(allocationSize = 1, initialValue = 1, sequenceName = "administration.seq_adm_fonc", name = "administration.seq_adm_fonc")
	@GeneratedValue(generator = "administration.seq_adm_fonc", strategy = GenerationType.SEQUENCE)
	@Id

	@Column(name = "id", unique = true, nullable = false, precision = 22, scale = 0)
	private Long menuId;

	@Column(name = "id_parent")
	private Long idAdmFonc;

	@Column(name = "code", nullable = false, length = 50)
	private String code;

	@Column(name = "libelle_ar", length = 150)
	private String libelleAr;

	@Column(name = "libelle_fr", length = 150)
	private String libelleFr;

	@Column(name = "libelle_en", length = 150)
	private String libelleEn;

	@Column(name = "is_actif", nullable = false)
	private boolean isActif;

	@Column(name = "is_admin", nullable = false)
	private boolean isAdmin;

	@Column(name = "router")
	private String path;

	@Column(name = "icon")
	private String icon;

	@Column(name = "dt_maj", nullable = false, length = 13)
	private Timestamp dtMaj;

	@Transient
	private List<AdmFonc> submenus;

	@Transient
	private String title;
	
	@Transient
	private String tooltip;
	
	@Transient
	private Boolean isChecked = false;

	public AdmFonc clone() throws CloneNotSupportedException {
		return (AdmFonc) super.clone();
	}
}
