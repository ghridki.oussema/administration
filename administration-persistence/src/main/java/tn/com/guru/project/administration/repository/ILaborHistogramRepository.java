package tn.com.guru.project.administration.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import tn.com.guru.project.administration.model.LaborHistogram;

@Repository
public interface ILaborHistogramRepository extends JpaRepository<LaborHistogram, Long> {

	
	@Transactional
	@Modifying
	@Query(value = "delete from  application.labor_histogram where id_projet=:idProjet", nativeQuery = true)

	void deleteByIdProjet(@Param(value = "idProjet") Long idProjet);
}
