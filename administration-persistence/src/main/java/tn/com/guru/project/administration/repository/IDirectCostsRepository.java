package tn.com.guru.project.administration.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import tn.com.guru.project.administration.model.DirectCosts;

@Repository
public interface IDirectCostsRepository extends JpaRepository<DirectCosts, Long> {

	
	@Procedure(value = "application.do_copy_direct_costs")
	public String copyDirectCosts(@Param("id_projet") Long idProjet);

	public List<DirectCosts> findByIdProjet(Long idProjet);
}
