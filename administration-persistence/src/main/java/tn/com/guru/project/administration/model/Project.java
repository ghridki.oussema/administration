package tn.com.guru.project.administration.model;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.json.JSONArray;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "projet", schema = "application")
public class Project implements java.io.Serializable, Cloneable {
	private transient static final long serialVersionUID = 1L;

	@SequenceGenerator(allocationSize = 1, initialValue = 1, sequenceName = "application.seq_project", name = "application.seq_project")
	@GeneratedValue(generator = "application.seq_project", strategy = GenerationType.SEQUENCE)
	@Id
	@Column(name = "id", unique = true, nullable = false, precision = 22, scale = 0)
	private Long id;

	@Column(name = "code", unique = true, nullable = false)
	private String code;

	@Column(name = "name")
	private String name;

	@Column(name = "location")
	private String location;

	@Column(name = "description")
	private String description;

	@Column(name = "id_project_primavera")
	private Long idProjectPrimavera;


	@Column(name = "created_at")
	private Timestamp createdAt;

	@Column(name = "updated_at")
	private Timestamp updatedAt;

	@Column(name = "employer")
	private String employer;

	@Column(name = "consultant")
	private String consultant;

	@Column(name = "contractor")
	private String contractor;

	@Column(name = "contract_type")
	private String contractType;

	@Column(name = "contract_value")
	private String contractValue;

	@Column(name = "built_up_area")
	private String builtUpArea;

	@Column(name = "site_instructions_no")
	private Integer siteInstructionsNo;

	@Column(name = "variation_orders_no")
	private Integer variationOrdersNo;

	@Column(name = "revised_contract_value")
	private String revisedContractValue;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	@Column(name = "commencement_date")
	private Date commencementDate;

	@Column(name = "project_duration")
	private Integer projectDuration;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	@Column(name = "project_completion")
	private Date projectCompletion;

	@Column(name = "executive_report")
	private String executiveReport;

	@Column(name = "node_ref")
	private String nodeRef;

	@Transient
	private JSONArray dataMedia;

	@Transient
	private List<String> urn;
}
