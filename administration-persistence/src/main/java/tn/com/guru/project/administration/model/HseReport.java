package tn.com.guru.project.administration.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

@Entity
@Table(name = "hse_report", schema = "application")

public class HseReport implements java.io.Serializable, Cloneable {
	private transient static final long serialVersionUID = 1L;

	@SequenceGenerator(allocationSize = 1, initialValue = 1, sequenceName = "application.seq_hse_report", name = "application.seq_hse_report")
	@GeneratedValue(generator = "application.seq_hse_report", strategy = GenerationType.SEQUENCE)
	@Id

	@Column(name = "id_hse_reports", unique = true, nullable = false)
	private Long idHseReports;

	@Column(name = "sn")
	private Integer sn;

	@Column(name = "id_projet")
	private Long idProjet;

	@Column(name = "discription")
	private String discription;

	@Column(name = "contractor_this_week")
	private String contractorthisweek;

	@Column(name = "subcontractor_this_week")
	private String subcontractorthisweek;

	@Column(name = "total_this_week")
	private String totalthisweek;

	@Column(name = "total")
	private String total;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	@Column(name = "created_at", length = 29)
	private Date createdAt;

	public HseReport clone() throws CloneNotSupportedException {
		return (HseReport) super.clone();
	}
}
