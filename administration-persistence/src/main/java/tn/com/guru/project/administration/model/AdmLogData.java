package tn.com.guru.project.administration.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

@Entity
@Table(name = "adm_log_data", schema = "administration")

public class AdmLogData implements java.io.Serializable, Cloneable {
	private transient static final long serialVersionUID = 1L;

	@SequenceGenerator(allocationSize = 1, initialValue = 1, sequenceName = "administration.seq_adm_log_data", name = "administration.seq_adm_log_data")
	@GeneratedValue(generator = "administration.seq_adm_log_data", strategy = GenerationType.SEQUENCE)
	@Id

	@Column(name = "id", unique = true, nullable = false, precision = 22, scale = 0)
	private Long id;

	@Column(name = "date_log", nullable = false, length = 29)
	private Date dateLog;

	@Column(name = "http_method", nullable = false, length = 50)
	private String httpMethod;

	@Column(name = "id_adm_user", nullable = false, precision = 22, scale = 0)
	private Long idAdmUser;

	@Column(name = "ip_address", nullable = false)
	private String ipAddress;

	@Column(name = "name_service", nullable = false)
	private String nameService;

	@Column(name = "result_ws", nullable = false)
	private String resultWs;

	@Column(name = "uri", nullable = false)
	private String uri;

	public AdmLogData clone() throws CloneNotSupportedException {
		return (AdmLogData) super.clone();
	}
}
