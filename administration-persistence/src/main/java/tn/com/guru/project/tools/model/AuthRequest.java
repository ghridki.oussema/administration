package tn.com.guru.project.tools.model;

import lombok.Data;

@Data
public class AuthRequest {

	private String username;
	private String password;
	
}
