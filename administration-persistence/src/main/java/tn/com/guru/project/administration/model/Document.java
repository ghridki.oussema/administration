package tn.com.guru.project.administration.model;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.web.multipart.MultipartFile;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

@Entity
@Table(name = "document", schema = "application")

public class Document implements java.io.Serializable, Cloneable {

	private transient static final long serialVersionUID = 1L;

	@SequenceGenerator(allocationSize = 1, initialValue = 1, sequenceName = "application.seq_document", name = "application.seq_document")
	@GeneratedValue(generator = "application.seq_document", strategy = GenerationType.SEQUENCE)
	@Id

	@Column(name = "id", unique = true, nullable = false, precision = 22, scale = 0)
	private Long id;

	@Column(name = "code")
	private String code;

	@Column(name = "id_project", nullable = false, precision = 22, scale = 0)
	private Long idProjet;

	@Column(name = "id_node_ref", length = 150)
	private String idNodeRef;

	@Column(name = "nom_fichier", nullable = false, length = 150)
	private String nomFichier;

	@Column(name = "created_at", nullable = false, length = 13)
	private Timestamp createdAt;

	@Column(name = "num_doc", length = 50)
	private String numDoc;

	@Transient
	private List<MultipartFile> listFile;

	public Document clone() throws CloneNotSupportedException {
		return (Document) super.clone();
	}
}
