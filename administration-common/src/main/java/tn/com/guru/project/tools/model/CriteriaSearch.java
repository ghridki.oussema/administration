package tn.com.guru.project.tools.model;

import lombok.Data;

@Data
public class CriteriaSearch {

	private String key;
	private Object value;
	private String specificSearch;

}
