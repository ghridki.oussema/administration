package tn.com.guru.project.tools;

public class Constante {

    // ################################################
    // # Actif Account User
    // ################################################

    public static final Long CODE_DISABLE_ACCOUNT = 0L;
    public static final Long CODE_ACTIF_ACCOUNT = 1L;
    public static final Long CODE_DEAVTIVE_ACCOUNT = 2L;


    // ################################################
    // # FORGE URI
    // ################################################
    public static final String GET_FORGE_TOKEN = "https://developer.api.autodesk.com/authentication/v1/authenticate";
    public static final String GET_HUBS_LIST = "https://developer.api.autodesk.com/project/v1/hubs";
    public static final String GET_PROJECTS_LIST = "https://developer.api.autodesk.com/project/v1/hubs/:hub_id/projects";

}
