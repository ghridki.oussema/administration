package tn.com.guru.project.tools.model;

import java.util.List;

import lombok.Data;

@Data
public class SearchObject {

	private Pagination pagination;
	private Sort sort;
	private List<Sort> listSort;
	private List<CriteriaSearch> dataSearch;
	private List<String> listCol;

	//For Export
	private String language;
	private String title;
	private String typeExport;
	private String colTitle;

}
