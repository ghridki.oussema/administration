package tn.com.guru.project.tools.model;

import lombok.Data;

@Data
public class Sort {

	private String nameCol = null;
	private String direction = null;
	
}
